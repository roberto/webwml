#use wml::debian::template title="LTS informações de segurança" GEN_TIME="yes"
#use wml::debian::toc
#use wml::debian::recent_list_security
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="28712bce73c493fd692778a4aadfe4c172ebaad9"

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

<toc-display/>

<toc-add-entry name="keeping-secure">Mantendo o seu sistema Debian LTS system seguro</toc-add-entry>

<p>
O pacote
<a href="https://packages.debian.org/stable/admin/unattended-upgrades">unattended-upgrades</a>
mantêm o computador atualizado com as últimas atualizações de segurança
(e outras) automaticamente.
A <a href="https://wiki.debian.org/UnattendedUpgrades">wiki</a> tem
informações mais detalhadas sobre como configurar o pacote.</p>

<p>Para mais informações sobre questões de segurança no Debian,
por favor acesse a página
the <a href="../../security">informações sobre segurança</a>.</p>

<a class="rss_logo" href="dla">RSS</a>
<toc-add-entry name="DLAS">Alertas recentes</toc-add-entry>

<p>Essas páginas web contém um histórico condensado de alertas de segurança
enviados para a lista de discussão
<a href="https://lists.debian.org/debian-lts-announce/">debian-lts-announce</a>.</p>

<p>Confira o <a href="new.html#DLAS">novo formato da lista</a>.</p>

<p>
<:= get_recent_security_list( '1m', '6', '.', '$(ENGLISHDIR)/lts/security' ) :>
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian LTS Security Advisories (titles only)" href="dla">
<link rel="alternate" type="application/rss+xml"
 title="Debian LTS Security Advisories (summaries)" href="dla-long">
:#rss#}
<p>Os últimos alertas de segurança do Debian LTS também estão disponíveis em
<a href="dla">formato RDF</a>. Também ofertamos um
<a href="dla-long">segundo arquivo</a> que inclui o primeiro parágrafo
do alerta correspondente para que você possa ver sobre o que ele trata.
</p>

#include "$(ENGLISHDIR)/lts/security/index.include"
<p>Alertas de segurança mais antigos também estão disponíveis:
<:= get_past_sec_list(); :>

<!-- This section a copy from /security/dsa.wml. TODO: create and include file -->
<toc-add-entry name="infos">Fontes de informações de segurança</toc-add-entry>

<ul>
<li><a href="https://security-tracker.debian.org/">Rastreador de segurança Debian</a>
fonte primária para todas as informações relacionadas à segurança, opções de pesquisa</li>

<li><a href="https://security-tracker.debian.org/tracker/data/json">Lista JSON</a>
   contém a descrição do CVE, o nome do pacote, o número do bug do Debian, as
   versões dos pacotes com correções, nenhum DSA incluído
</li>

<li><a href="https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/DSA/list">Lista de DSA</a>
   contém o DSA incluindo data, os números de CVE relacionados, as versões de pacotes com correções
</li>

<li><a href="https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/DLA/list">Lista DLA</a>
   contém o DLA incluindo data, os números de CVE relacionados, as versões de pacotes com correções
</li>

<li><a href="https://lists.debian.org/debian-security-announce/">
Anúncios de DSA</a></li>
<li><a href="https://lists.debian.org/debian-lts-announce/">
Anúncios de DLA</a></li>

<li><a href="oval">Arquivos Oval</a></li>

<li>Pesquise um DSA (maiúsculas são importantes)<br>
por exemplo: <tt>https://security-tracker.debian.org/tracker/DSA-3814</tt>
</li>

<li>Pesquise um DLA (-1 é importante)<br>
por exemplo: <tt>https://security-tracker.debian.org/tracker/DLA-867-1</tt>
</li>

<li>Pesquise um CVE<br>
por exemplo: <tt>https://security-tracker.debian.org/tracker/CVE-2017-6827</tt>
</li>
</ul>

<ul>
<li> <a href="https://lts-team.pages.debian.net/wiki/FAQ">Perguntas frequentes sobre o Debian LTS</a>.
Sua pergunta já pode estar respondida lá!
<li>
<a href="https://lts-team.pages.debian.net/wiki/Contact">Informações de contato da equipe Debian LTS</a>
</ul>
