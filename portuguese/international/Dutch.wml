#use wml::debian::template title="Páginas holandesas do Debian"
#use wml::debian::translation-check translation="e1086a4582af6d8098ee177d56337deffe7a389d"

# This contents of this page is completely the responsibility of
# the translation team


<h1>Debian nos países e territórios de língua holandesa</h1>

<p>O Debian visa tornar-se um sistema operacional universal.
Isto torna o Debian não apenas funcional em uma variedade de arquiteturas de 
computadores, mas também procura estar disponível em tantos idiomas e acessível 
para o maior número possível de pessoas. Portanto, não é surpreendente que o 
Debian, originado nos EUA e usando o inglês como seu idioma principal, 
evoluiu para um rede global de voluntários(as) com uma base mundial de
usuários(as).</p>
<p>Nos países e territórios de língua holandesa ao redor do mundo há vários(as) 
desenvolvedores(as) do Debian, assim como muitos(as) outros(as) que contribuem
para o Debian oferecendo suporte ao(a) usuário(a), traduzindo o sistema
operacional para o holandês ou convertendo páginas web do Debian para que os(as)
holandeses(as) possam lê-las em sua língua nativa.</p>

<h2>O Debian para sua base de usuários(as) falantes de holandês</h2>

<ul>
  <li><a href="$(HOME)/social_contract">O <q>contrato social</q> do Debian com a 
  comunidade de Software Livre</a></li>
  <li><a href="$(HOME)/intro/cn">Leia as páginas web do Debian no seu próprio 
  idioma</a></li>
  <li>Listas de discussão holandesa para usuários(as) do Debian:
    <a href="https://lists.debian.org/debian-user-dutch/">arquivos da lista de 
    discussão</a>;
    <a href="https://lists.debian.org/debian-user-dutch/">inscreva-se na lista</a>
  </li>
</ul>


<h2>Ajudando na tradução do Debian</h2>

<p>Temos a ambição de fazer que sistema operacional Debian e sua documentação 
também estejam disponíveis em holandês.
Isso pode ser de grande ajuda para as pessoas que não têm um bom conhecimento do
idioma inglês. Principalmente para as crianças, a falta de uma tradução 
holandesa pode ser uma barreira importante.</p>
<p>Como o desenvolvimento do próprio sistema operacional Debian, sua tradução 
também depende de voluntários(as). Se você tem algum tempo livre e um domínio 
razoável do idioma inglês, então pode considerar contribuir também. Se a ideia
de contribuir parecer atraente, por favor, encontre informações adicionais nos 
seguintes locais:</p>
  
  
<ul>
  <li>No <a href="dutch/index.html">site web do projeto</a> de tradução do 
  Debian você poderá encontrar informações adicionais da tradução do Debian para
  o holandês</li>
  <li>A tradução para o holandês é coordenada nessa lista de discussão:
  <a href="mailto:debian-l10n-dutch@lists.debian.org">
  debian-l10n-dutch@lists.debian.org</a>
  (<a href="https://lists.debian.org/debian-l10n-dutch/">páginas de arquivos e 
  inscrição</a>).</li>
</ul>
