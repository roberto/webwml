#use wml::debian::template title="Instalador do Debian" NOHEADER="true"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="5d9ed11b1ac37b0f0417096ff186a5fd178b7fa6"

<h1>Notícias</h1>

<p><:= get_recent_list('News/$(CUR_YEAR)', '2',
'$(ENGLISHDIR)/devel/debian-installer', '', '\d+\w*' ) :>
<a href="News">Notícias mais antigas</a>
</p>

<h1>Instalando com o Instalador do Debian (Debian-Installer)</h1>

<p>
<if-stable-release release="bookworm">
<strong>Para mídias oficiais de instalação e informações sobre o Debian
<current_release_bookworm></strong>, veja
<a href="$(HOME)/releases/bookworm/debian-installer">a página do bookworm</a>.
</if-stable-release>
<if-stable-release release="trixie">
<strong>Para mídias oficiais de instalação e informações sobre o Debian
<current_release_trixie></strong>, veja
<a href="$(HOME)/releases/trixie/debian-installer">a página do trixie</a>.
</if-stable-release>

</p>

<div class="tip">
<p>
Todos os links das imagens abaixo são para a versão em desenvolvimento
do Instalador do Debian para o próximo lançamento do Debian e irão instalar,
por padrão, o Debian test (testing) (<q><current_testing_name></q>).
</p>
</div>

<!-- Mostrar no início do ciclo de lançamento: nenhum Alpha/Beta/RC lançado
ainda. -->
<if-testing-installer released="no">

<p>

<strong>Para instalar o Debian teste (testing)</strong>, recomendamos que você
use as <strong>construções diárias</strong> do instalador. As seguintes imagens
estão disponíveis para compilações diárias:

</p>

</if-testing-installer>

<!-- Exibir posteriormente no ciclo de lançamento: Alfa/Beta/RC disponível,
aponte para o mais recente. -->
<if-testing-installer released="yes">
<p>

<strong>Para instalar o Debian teste (testing)</strong>, recomendamos que
use a versão <strong><humanversion /></strong> do instalador, após verificar sua
<a href="errata">errata</a>. As imagens a seguir estão disponíveis para a
versão <humanversion />:

</p>

<h2>Versão oficial</h2>

<div class="line">
<div class="item col50">
<strong>imagens de CD netinst</strong>
<netinst-images />
</div>

<div class="item col50 lastcol">
<strong>imagens de CD netinst (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<netinst-images-jigdo />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>CD</strong>
<full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>DVD</strong>
<full-dvd-images />
</div>

</div>


<div class="line">
<div class="item col50">
<strong>CD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>DVD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-dvd-jigdo />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>Blu-ray (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-bd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>outras imagens (netboot, pendrive USB, etc)</strong>
<other-images />
</div>
</div>

<p>
Alternativamente, você pode usar um <b>snapshot</b> da versão teste (testing) do
Debian.
As construções semanais geram conjuntos completos de imagens, enquanto as
construções diárias geram apenas algumas imagens.
</p>

<div class="warning">

<p>
Esses snapshots instalam o Debian teste (testing), mas o instalador é baseado no
Debian instável (unstable).
</p>

</div>

<h2>Snapshots semanais atuais</h2>

<div class="line">
<div class="item col50">
<strong>CD</strong>
<devel-full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>DVD</strong>
<devel-full-dvd-images />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>CD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>DVD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-dvd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>Blu-ray (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-bd-jigdo />
</div>
</div>

<p>
Se você preferir utilizar o último e maior, tanto para nos ajudar a testar o
futuro lançamento do instalador como por causa de problemas de hardware ou
outras questões, tente uma destas <strong>imagens construídas diariamente</strong>
que contêm a última versão disponível dos componentes do instalador.
</p>
</if-testing-installer>

<h2>Snapshots diários atuais</h2>

<div class="line">
<div class="item col50">
<strong>imagens de CD netinst</strong>
<devel-small-cd-images />
</div>

<div class="item col50 lastcol">
<strong>imagens de CD netinst (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-small-cd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>outras imagens (netboot, pendrive USB, etc)</strong>
<devel-other-images />
</div>
</div>

<hr />

<p>
<strong>Notas</strong>
</p>
<ul>
#	<li>Antes de baixar as imagens que são construídas diariamente, sugerimos
#	que verifique os <a href="https://wiki.debian.org/DebianInstaller/Today">
#	problemas conhecidos</a>.</li>
	<li>Uma arquitetura pode ser (temporariamente) omitida do resumo de
	imagens diárias se a mesma não estiver (confiavelmente) disponível.</li>
	<li>Para imagens de instalação, arquivos de verificação (<tt>SHA512SUMS</tt> e
  <tt>SHA256SUMS</tt>) estão disponíveis no mesmo diretório das imagens.</li>
  <li>Para baixar imagens completas de CD e DVD a utilização do jigdo é
  recomendada.</li>
  <li>Apenas um número limitado do conjunto completo de imagens de DVD estão
  disponíveis como arquivo ISO para baixar diretamente. A maioria dos usuários
  não precisa de todo software disponível em todos os discos, então para
  economizar espaço nos servidores e espelhos os conjunto completos estão
  disponíveis apenas via jidgo.</li>
</ul>

<p>
<strong>Depois de usar o Instalador do Debian</strong>, por favor, envie-nos um
<a href="https://d-i.debian.org/manual/pt.amd64/ch05s04.html#submit-bug">
relatório de instalação</a>, mesmo que não hajam problemas.
</p>

<h1>Documentação</h1>

<p>
<strong>Se você lê somente um documento</strong> antes de instalar, leia
nosso <a href="https://d-i.debian.org/manual/pt.amd64/apa.html">Howto de
Instalação</a>, uma passagem rápida pelo processo de instalação. Outras
documentações úteis incluem:
</p>

<ul>
<li>Guia de Instalação:
#    <a href="$(HOME)/releases/stable/installmanual">versão para o lançamento
#    atual</a>
#    &mdash;
    <a href="$(HOME)/releases/testing/installmanual">versão em desenvolvimento
    (testing)</a>
    &mdash;
    <a href="https://d-i.debian.org/manual/">última versão (Git)</a>
<br />
instruções detalhadas de instalação</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">FAQ do Instalador
do Debian</a> e <a href="$(HOME)/CD/faq/">FAQ do Debian-CD</a><br />
perguntas mais frequentes e suas respostas</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Wiki do Instalador do
Debian</a><br /> documentação mantida pela comunidade</li>
</ul>

<h1>Entrando em contato conosco</h1>

<p>
A <a href="https://lists.debian.org/debian-boot/">lista de discussão debian-boot</a>
é o fórum principal para discussão e trabalho no Instalador do Debian.
</p>

<p>
Também temos um canal no IRC, #debian-boot em <tt>irc.debian.org</tt>. Esse
canal é utilizado principalmente para desenvolvimento, mas ocasionalmente
para suporte. Se você não receber uma resposta, tente a lista de discussão.
</p>
