#use wml::debian::template title="Etapa 2: identificação" NOHEADER="true"
#include "$(ENGLISHDIR)/devel/join/nm-steps.inc"
#use wml::debian::translation-check translation="b59849318da01faabdbc1d5bb5e05f99c4f7a61a"

<p>As informações nesta página, embora públicas, são de interesse
principalmente dos(as) futuros(as) desenvolvedores(as) Debian.</p>

<h2>Etapa 2: identificação</h2>

<h3>Por que OpenPGP?</h3>

<p>O Debian faz uso extensivo do OpenPGP porque
<a href="newmaint#Member">membros(as) do Debian</a> estão localizados(as) em
todo o mundo (veja
<a href="$(DEVEL)/developers.loc">localizações dos(as) desenvolvedor(as)</a>)
e raramente se encontram pessoalmente. Isso significa que a confiança não pode
ser construída por contato pessoal e outros meios são necessários. Todos os(as)
desenvolvedores(as) Debian são identificados(as) por sua chave
<a href="http://www.gnupg.org/">OpenPGP</a>. Essas chaves permitem autenticar
mensagens e outros dados, assinando-os. Para mais informações sobre as chaves
OpenPGP, consulte o arquivo README do pacote <code>debian-keyring</code>.</p>

<h3>Fornecendo uma chave</h3>

<p>Cada <a href="newmaint#Applicant">candidato(a)</a> deve fornecer uma chave
pública OpenPGP versão 4 com recursos de criptografia. A maneira preferida de
fazer isso é exportá-la para um dos servidores de chave pública, como
<tt>subkeys.pgp.net</tt>.
Chaves públicas podem ser exportadas fazendo:</p>
<pre>
gpg --send-key --keyserver &lt;endereço-do-servidor&gt; &lt;id-da-sua-chave&gt;
</pre>

<p>Se sua chave não possui capacidade de encriptação, você pode simplesmente
adicionar uma subchave de encriptação.</p>

<p>Veja <a href="https://keyring.debian.org/">keyring.debian.org</a>
para mais informações sobre formatos de chave e propriedades.</p>

<h3>Verificação</h3>

<p>Como qualquer pessoa pode fazer upload de uma chave pública para os
servidores, é necessário verificar se a chave pertence ao(a) candidato(a). </p>

<p>Para conseguir isso, a chave pública deve ser assinada por dois(duas)
outros(as) <a href="newmaint#Member">membros(as) do Debian</a>. Portanto, o(a)
candidato(a) deve conhecer esse(a) membro(a) do Debian pessoalmente e deve se
identificar (fornecendo um passaporte, uma carteira de motorista ou alguma
outra identidade).</p>

<h4><a name="key_signature">Como conseguir sua chave OpenPGP assinada</a></h4>

<p>Existem várias maneiras de encontrar um(a) membro(a) do Debian para uma
troca de chaves. Você deve tentar na ordem listada abaixo: </p>

<ol>

<li>Os anúncios das festas de assinatura de chaves geralmente são postados na
lista de discussão <code>debian-devel</code>; portanto, verifique lá
primeiro.</li>

<li><p>Você pode procurar desenvolvedores(as) em áreas específicas através
da <a href="https://wiki.debian.org/Keysigning">página de coordenação de
assinatura de chaves</a>:</p>

<ul>
      <li>Primeiro você deve verificar a lista de ofertas de assinatura de
        chave para um(a) membro(a) do Debian perto de você.</li>
      <li>Se você não conseguir encontrar um(a) membro(a) do Debian entre as
        ofertas de assinatura de chave, você pode registrar sua solicitação de
        assinatura de chave.</li>
</ul>
</li>

<li>Se ninguém respondeu à sua solicitação por várias semanas, envie um e-mail
para <email debian-private@lists.debian.org> informando exatamente onde você
mora (além de citar algumas grandes cidades próximas a você), então alguém
podem verificar no banco de dados do(a) desenvolvedor(as) aqueles(as)
desenvolvedores(as) que estão perto de você.</li>

</ol>

<p>Depois de encontrar alguém para assinar sua chave, siga os passos em
 <a href="$(HOME)/events/keysigning">MiniHOWTO de assinatura de chaves</a>.</p>

<p>É recomendável que você assine também a chave do(a) desenvolvedor(a) Debian.
Isso não é necessário para a sua verificação de identidade, mas fortalece a
rede de confiança.</p>

<h4>Quando você não conseguir ter sua chave assinada</h4>

<p>Se todas as etapas acima falharem, entre em contato com a
<a href="newmaint#FrontDesk">secretaria</a> e peça ajuda.
Eles(as) podem oferecer uma maneira alternativa de identificação.</p>

<hr>
#include "$(ENGLISHDIR)/devel/join/nm-steps.inc"
