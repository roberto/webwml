#use wml::debian::template title="Debian 9 -- Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="9f9207921c4ea799d5d3daa8a0f4b0faedf2b073"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Problemas conhecidos</toc-add-entry>
<toc-add-entry name="security">Problemas de segurança</toc-add-entry>

<p>A equipe de segurança Debian emite atualizações para os pacotes na versão
estável (stable) nos quais eles identificaram problemas relacionados à segurança.
Por favor, consulte as <a href="$(HOME)/security/">páginas de segurança</a> para
informações sobre quaisquer problemas de segurança identificados no
<q>Stretch</q>.</p>



<p>Se você usa o APT, adicione a seguinte linha ao <tt>/etc/apt/sources.list</tt>
para poder acessar as atualizações de segurança mais recentes:</p>

<pre>
  deb http://security.debian.org/ stretch/updates main contrib non-free
</pre>

<p>Depois disso, execute <kbd>apt-get update</kbd> seguido de
<kbd>apt-get upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Lançamentos pontuais</toc-add-entry>

<p>Às vezes, no caso de vários problemas críticos ou atualizações de segurança,
a versão lançada é atualizada. Geralmente, estas são indicadas como
lançamentos pontuais.</p>

<ul>
  <li>O primeiro lançamento pontual, 9.1, foi lançado em
      <a href="$(HOME)/News/2017/20170722">22 de julho de 2017</a>.</li>
  <li>O segundo lançamento pontual, 9.2, foi lançado em
      <a href="$(HOME)/News/2017/20171007">7 de outubro de 2017</a>.</li>
  <li>O terceiro lançamento pontual, 9.3, foi lançado em
      <a href="$(HOME)/News/2017/2017120902">9 dezembro de 2017</a>.</li>
  <li>O quarto lançamento pontual, 9.4, foi lançado em
      <a href="$(HOME)/News/2018/20180310">10 de março de 2018</a>.</li>
  <li>O quinto lançamento pontual, 9.5, foi lançado em
      <a href="$(HOME)/News/2018/20180714">14 de julho de 2018</a>.</li>
  <li>O sexto lançamento pontual, 9.6, foi lançado em
      <a href="$(HOME)/News/2018/20181110">10 de novembro de 2018</a>.</li>
  <li>O sétimo lançamento pontual, 9.7, foi lançado em
      <a href="$(HOME)/News/2019/20190123">23 de janeiro de 2019</a>.</li>
  <li>O oitavo lançamento pontual, 9.8, foi lançado em
      <a href="$(HOME)/News/2019/20190216">16 de fevereiro de 2019</a>.</li>
  <li>O nono lançamento pontual, 9.9, foi lançado em
      <a href="$(HOME)/News/2019/20190427">27 de abril de 2019</a>.</li>
  <li>O décimo lançamento pontual, 9.10, foi lançado em
      <a href="$(HOME)/News/2019/2019090702">7 de setembro de 2019</a>.</li>
  <li>O décimo primeiro lançamento pontual, 9.11, foi lançado em
      <a href="$(HOME)/News/2019/20190908">8 de setembro de 2019</a>.</li>
 <li>O décimo segundo lançamento pontual, 9.12, foi lançado em
      <a href="$(HOME)/News/2020/2020020802">8 de fevereiro de 2020</a>.</li>
 <li>O décimo terceiro lançamento pontual, 9.13, foi lançado em
      <a href="$(HOME)/News/2020/20200718">18 de julho de 2020</a>.</li>
  </ul>

<ifeq <current_release_stretch> 9.0 "

<p>Ainda não há lançamentos pontuais para o Debian 9.</p>" "

<p>Veja o <a
href="http://http.us.debian.org/debian/dists/stretch/ChangeLog">\
ChangeLog</a>
para detalhes sobre mudanças entre o 9.0 e o <current_release_stretch/>.</p>"/>


<p>As correções lançadas na versão estável (stable) geralmente passam por um
período de teste prolongado antes de serem aceitas no repositório.
No entanto, essas correções estão disponíveis no repositório
<a href="http://ftp.debian.org/debian/dists/stretch-proposed-updates/">\
dists/stretch-proposed-updates</a> de qualquer espelho de arquivos do Debian.</p>

<p>Se você usa o APT para atualizar seus pacotes, poderá instalar
as atualizações propostas adicionando a seguinte linha ao
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# Atualizações propostas para a versão 9
  deb http://ftp.us.debian.org/debian stretch-proposed-updates main contrib non-free
</pre>

<p>Depois disso, execute <kbd>apt-get update</kbd> seguido de
<kbd>apt-get upgrade</kbd>.</p>


<toc-add-entry name="installer">Sistema de instalação</toc-add-entry>

<p>
Para obter informações sobre erratas e atualizações para o sistema de
instalação, consulte a página de informações sobre instalação.
</p>
