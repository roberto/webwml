#use wml::debian::translation-check translation="b51002b9cd5b7dbca0e39ddb2d17bcc495ead21a"
<define-tag pagetitle>Atualização Debian 9: 9.13 lançado</define-tag>
<define-tag release_date>2020-07-18</define-tag>
#use wml::debian::news

<define-tag release>9</define-tag>
<define-tag codename>stretch</define-tag>
<define-tag revision>9.13</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>O projeto Debian está feliz em anunciar a décima terceira (e última)
atualização de sua versão antiga (oldstable) Debian <release> (codinome
<q><codename></q>).
Esta versão pontual adiciona principalmente correções para problemas de
segurança, além de pequenos ajustes para problemas sérios. Avisos de
segurança já foram publicados em separado e são referenciados quando
disponíveis.</p>

<p>Após esta atualização pontual, as equipes de segurança e lançamento não irão
mais produzir atualizações para Debian 9. Usuários(as) que desejarem continuar
recebendo suporte de segurança deverão atualizar para o Debian 10 ou acessar
<url "https://wiki.debian.org/LTS"> para detalhes sobre o subconjunto de
arquiteturas e pacotes cobertos pela equipe do projeto de suporte de longo
prazo.</p>

<p>Por favor note que a versão pontual não constitui uma nova versão do Debian
<release>, mas apenas atualiza alguns dos pacotes já incluídos. Não há
necessidade de jogar fora as antigas mídias do <q><codename></q>. Após a
instalação, os pacotes podem ser atualizados para as versões atuais usando um
espelho atualizado do Debian.</p>

<p>Aquelas pessoas que frequentemente instalam atualizações a partir do
security.debian.org não terão que atualizar muitos pacotes, e a maioria de tais
atualizações estão incluídas na versão pontual.</p>

<p>Novas imagens de instalação logo estarão disponíveis nos locais habituais.</p>

<p>A atualização de uma instalação existente para esta revisão pode ser feita
apontando o sistema de gerenciamento de pacotes para um dos muitos espelhos
HTTP do Debian. Uma lista abrangente de espelhos está disponível em:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Correções de bugs gerais</h2>

<p>Esta atualização da versão antiga (oldstable) adiciona algumas correções
importantes para os seguintes pacotes:</p>

<table border=0>
<tr><th>Pacote</th>               <th>Justificativa</th></tr>
<correction acmetool "Reconstruído contra golang mais recente para
atualizações de segurança">
<correction atril "dvi: Mitiga injeção de comandos de ataque através da citação
de nome de arquivo [CVE-2017-1000159]; Corrige sobrecarga de checagem no
mecanismo tiff [CVE-2019-1010006]; tiff: Manipula falha de TIFFReadRGBAImageOriented [CVE-2019-11459]">
<correction bacula "Adiciona pacote de transição bacula-director-common,
evitando a perda de /etc/bacula/bacula-dir.conf quando expurgado; torna usuário
root proprietário dos arquivos PID">
<correction base-files "Atualiza /etc/debian_version para a versão pontual">
<correction batik "Corrige a falsificação de requisições pelo lado servidor
através de atributos xlink:href [CVE-2019-17566]">
<correction c-icap-modules "Suporte a ClamAV 0.102">
<correction ca-certificates "Atualiza pacote Mozilla CA para a versão 2.40,
bloqueia raízes não confiáveis da Symantec e <q>AddTrust External Root</q>
expirados; remove certificados exclusivos de e-mail">
<correction chasquid "Reconstruído contra golang mais recente para atualizações
 de segurança">
<correction checkstyle "Corrige problema de injeções XML External Entity [CVE-2019-9658 CVE-2019-10782]">
<correction clamav "Nova versão upstream [CVE-2020-3123]; correções de
segurança [CVE-2020-3327 CVE-2020-3341]">
<correction compactheader "Nova versão upstream, compatível com versões mais
novas do Thunderbird">
<correction cram "Ignora falhas em teste para correção de problemas de
construção">
<correction csync2 "Comando HELLO falha quando SSL é requisitado">
<correction cups "Corrige estouro do buffer de pilha [CVE-2020-3898] e
<q>a função `ippReadIO` que pode ser ler de modo insatisfatório (under-read) um
campo de extensão</q> [CVE-2019-8842]">
<correction dbus "Nova versão estável em upstream; previne problema de negação
de serviço [CVE-2020-12049]; previne use-after-free se dois nomes de
usuários(as) compartilham um uid">
<correction debian-installer "Atualização para o kernel Linux ABI para 4.9.0-13">
<correction debian-installer-netboot-images "Reconstruído contra
stretch-proposed-updates">
<correction debian-security-support "Atualização do status de suporte para
 diversos pacotes">
<correction erlang "Corrige o uso de cifras TLS fracas [CVE-2020-12872]">
<correction exiv2 "Corrige problema de negação de serviço [CVE-2018-16336];
corrige correção excessivamente restritiva para CVE-2018-10958 e CVE-2018-10999">
<correction fex "Atualização de segurança">
<correction file-roller "Correção de segurança [CVE-2020-11736]">
<correction fwupd "Nova versão upstream; utiliza um CNAME para redirecionar
ao CDN correto para metadados; não aborta a inicialização se o arquivo XML de
metadados for inválido; adiciona as chaves públicas GPG da Linux Foundation
para firmware e metadados; aumenta o limite de metadados para 10MB">
<correction glib-networking "Retorna erro de identidade ruim se a identidade
estiver indefinida [CVE-2020-13645]">
<correction gnutls28 "Corrige problemas de corrupção de memória [CVE-2019-3829];
corrige vazamento de memória; adiciona suporte para tickets de sessão de
tamanho zero, corrige erros de sessão no TLS1.2 em alguns provedores de
hospedagem">
<correction gosa "Reforça a checagem de sucesso/falha do LDAP [CVE-2019-11187];
corrige compatibilidade com versões PHP mais recentes; porta diversos outros
patches; substitui (un)serialize por json_encode/json_decode para mitigar a
injeção de objetos PHP [CVE-2019-14466]">
<correction heartbleeder "Reconstruído contra golang mais recente para
atualizações de segurança">
<correction intel-microcode "Retorna alguns microcódigos para revisões lançadas
anteriormente, contornando travamentos no boot em Skylake-U/Y e Skylake Xeon E3">
<correction iptables-persistent "Não falha se modprobe falhar">
<correction jackson-databind "Correção de múltiplos problemas de segurança
afetando BeanDeserializerFactory [CVE-2020-9548 CVE-2020-9547 CVE-2020-9546 CVE-2020-8840 CVE-2020-14195 CVE-2020-14062 CVE-2020-14061 CVE-2020-14060 CVE-2020-11620 CVE-2020-11619 CVE-2020-11113 CVE-2020-11112 CVE-2020-11111 CVE-2020-10969 CVE-2020-10968 CVE-2020-10673 CVE-2020-10672 CVE-2019-20330 CVE-2019-17531 and CVE-2019-17267]">
<correction libbusiness-hours-perl "Uso explícito de ano com 4 dígitos,
 correção de construção (build) e problemas de utilização">
<correction libclamunrar "Nova versão estável upstream; adição de metapacotes
não versionados">
<correction libdbi "Retira novamente comentário de chamada _error_handler(),
corrigindo problemas com consumidores">
<correction libembperl-perl "Manipula páginas de erro do Apache &gt;= 2.4.40">
<correction libexif "Correções de segurança [CVE-2016-6328 CVE-2017-7544 CVE-2018-20030
 CVE-2020-12767 CVE-2020-0093]; correções de segurança [CVE-2020-13112 CVE-2020-13113
 CVE-2020-13114]; correção de estouro de buffer de leitura [CVE-2020-0182] e de
  estouro de inteiro não sinalizado [CVE-2020-0198]">
<correction libvncserver "Corrige estouro de pilha [CVE-2019-15690]">
<correction linux "Nova versão estável upstream; atualiza kernel ABI para
 4.9.0-13">
<correction linux-latest "Atualiza kernel ABI para 4.9.0-13">
<correction mariadb-10.1 "Nova versão estável upstream; correções de segurança
 [CVE-2020-2752 CVE-2020-2812 CVE-2020-2814]">
<correction megatools "Adiciona suporte para o novo formato de links de mega.nz">
<correction mod-gnutls "Evita suítes criptográficas obsoletas na suíte de
testes; corrige falhas de teste quando combinado com correção do Apache para
CVE-2019-10092">
<correction mongo-tools "Reconstruído contra golang mais recente para atualizações
 de segurança">
<correction neon27 "Trata falhas em testes relacionadas ao OpenSSL-related como
 não fatais">
<correction nfs-utils "Corrige potencial vulnerabilidade de sobrescrita de
 arquivo [CVE-2019-3689]; não faz com que todo o /var/lib/nfs seja propriedade
  do usuário statd">
<correction nginx "Corrige vulnerabilidade de erro de contrabando de requisição
 de página [CVE-2019-20372]">
<correction node-url-parse "Limpa caminhos e hosts antes de análise [CVE-2018-3774]">
<correction nvidia-graphics-drivers "Nova versão estável upstream; correções
de segurança [CVE-2020-5963 CVE-2020-5967]">
<correction pcl "Corrige falta de dependências em libvtk6-qt-dev">
<correction perl "Corrige múltiplas expressões regulares relacionadas a
 problemas de segurança  [CVE-2020-10543 CVE-2020-10878 CVE-2020-12723]">
<correction php-horde "Corrige vulnerabilidade de cross-site scripting [CVE-2020-8035]">
<correction php-horde-data "Corrige vulnerabilidade de execução de código
remoto autenticado [CVE-2020-8518]">
<correction php-horde-form "Corrige vulnerabilidade de execução de código
remoto autenticado [CVE-2020-8866]">
<correction php-horde-gollem "Corrige vulnerabilidade de crosss-site scripting
na saída de breadcrumb output [CVE-2020-8034]">
<correction php-horde-trean "Corrige vulnerabilidade de execução de código
remoto autenticado [CVE-2020-8865]">
<correction phpmyadmin "Correções de segurança diversas [CVE-2018-19968 CVE-2018-19970 CVE-2018-7260 CVE-2019-11768 CVE-2019-12616 CVE-2019-6798 CVE-2019-6799 CVE-2020-10802 CVE-2020-10803 CVE-2020-10804 CVE-2020-5504]">
<correction postfix "Nova versão estável upstream">
<correction proftpd-dfsg "Corrige manipulação de pacotes SSH_MSG_IGNORE">
<correction python-icalendar "Corrige dependências de Python3">
<correction rails "Corrige possível cross-site scripting através de escape do
Javascript helper [CVE-2020-5267]">
<correction rake "Corrige vulnerabilidade de injeção de comandos [CVE-2020-8130]">
<correction roundcube "Corrige problemas de cross-site scripting através de
mensagens HTML com svg/namespace malicioso [CVE-2020-15562]">
<correction ruby-json "Corrige vulnerabilidade de criação de objetos inseguros
 [CVE-2020-10663]">
<correction ruby2.3 "Corrige vulnerabilidade de criação de objetos inseguros [CVE-2020-10663]">
<correction sendmail "Corrige a busca por processo de controle de fila de
execução em modo <q>split daemon</q>, <q>NOQUEUE: connect from (null)</q>,
remove falha quando usando BTRFS">
<correction sogo-connector "Nova versão upstream, compatível com versões mais novas
do Thunderbird">
<correction ssvnc "Corrige escrita fora dos limites [CVE-2018-20020], laço
infinito [CVE-2018-20021], inicialização imprópria [CVE-2018-20022], negação de
 serviço em potencial [CVE-2018-20024]">
<correction storebackup "Corrige possível vulnerabilidade de escalação de
privilégios [CVE-2020-7040]">
<correction swt-gtk "Corrige dependências ausentes em libwebkitgtk-1.0-0">
<correction tinyproxy "Cria arquivo PID antes de baixar privilégios para conta
não root [CVE-2017-11747]">
<correction tzdata "Nova versão estável upstream">
<correction websockify "Corrige dependências ausentes em python{3,}-pkg-resources">
<correction wpa "Corrige desvio de proteção de desconexão do mo AP PMF
[CVE-2019-16275]; Corrige problemas de aleatorização de MAC com algumas placas">
<correction xdg-utils "Limpa o nome da janela antes de enviar por D-Bus; trata
 corretamente a manipulação de diretórios com nomes contendo espaços; cria o
 diretório <q>applications</q> se necessário">
<correction xml-security-c "Corrige o cálculo de comprimento no método concat">
<correction xtrlock "Corrige o bloqueio de (alguns) dispositivos multitoque
quando bloqueados [CVE-2016-10894]">
</table>


<h2>Atualizações de segurança </h2>


<p>Esta revisão adiciona as seguintes atualizações de segurança da versão antiga
(oldstable). A equipe de segurança já lançou um comunicado para cada uma dessas
atualizações:</p>

<table border=0>
<tr><th>ID Consultivo</th>  <th>Pacote</th></tr>
<dsa 2017 4005 openjfx>
<dsa 2018 4255 ant>
<dsa 2018 4352 chromium-browser>
<dsa 2019 4379 golang-1.7>
<dsa 2019 4380 golang-1.8>
<dsa 2019 4395 chromium>
<dsa 2019 4421 chromium>
<dsa 2020 4616 qemu>
<dsa 2020 4617 qtbase-opensource-src>
<dsa 2020 4618 libexif>
<dsa 2020 4619 libxmlrpc3-java>
<dsa 2020 4620 firefox-esr>
<dsa 2020 4621 openjdk-8>
<dsa 2020 4622 postgresql-9.6>
<dsa 2020 4624 evince>
<dsa 2020 4625 thunderbird>
<dsa 2020 4628 php7.0>
<dsa 2020 4629 python-django>
<dsa 2020 4630 python-pysaml2>
<dsa 2020 4631 pillow>
<dsa 2020 4632 ppp>
<dsa 2020 4633 curl>
<dsa 2020 4634 opensmtpd>
<dsa 2020 4635 proftpd-dfsg>
<dsa 2020 4637 network-manager-ssh>
<dsa 2020 4639 firefox-esr>
<dsa 2020 4640 graphicsmagick>
<dsa 2020 4642 thunderbird>
<dsa 2020 4646 icu>
<dsa 2020 4647 bluez>
<dsa 2020 4648 libpam-krb5>
<dsa 2020 4650 qbittorrent>
<dsa 2020 4653 firefox-esr>
<dsa 2020 4655 firefox-esr>
<dsa 2020 4656 thunderbird>
<dsa 2020 4657 git>
<dsa 2020 4659 git>
<dsa 2020 4660 awl>
<dsa 2020 4663 python-reportlab>
<dsa 2020 4664 mailman>
<dsa 2020 4666 openldap>
<dsa 2020 4668 openjdk-8>
<dsa 2020 4670 tiff>
<dsa 2020 4671 vlc>
<dsa 2020 4673 tomcat8>
<dsa 2020 4674 roundcube>
<dsa 2020 4675 graphicsmagick>
<dsa 2020 4676 salt>
<dsa 2020 4677 wordpress>
<dsa 2020 4678 firefox-esr>
<dsa 2020 4683 thunderbird>
<dsa 2020 4685 apt>
<dsa 2020 4686 apache-log4j1.2>
<dsa 2020 4687 exim4>
<dsa 2020 4688 dpdk>
<dsa 2020 4689 bind9>
<dsa 2020 4692 netqmail>
<dsa 2020 4693 drupal7>
<dsa 2020 4695 firefox-esr>
<dsa 2020 4698 linux>
<dsa 2020 4700 roundcube>
<dsa 2020 4701 intel-microcode>
<dsa 2020 4702 thunderbird>
<dsa 2020 4703 mysql-connector-java>
<dsa 2020 4704 vlc>
<dsa 2020 4705 python-django>
<dsa 2020 4706 drupal7>
<dsa 2020 4707 mutt>
<dsa 2020 4711 coturn>
<dsa 2020 4713 firefox-esr>
<dsa 2020 4715 imagemagick>
<dsa 2020 4717 php7.0>
<dsa 2020 4718 thunderbird>
</table>


<h2>Pacotes removidos</h2>

<p>Os seguintes pacotes foram removidos devido a circunstâncias além de nosso
controle:</p>

<table border=0>
<tr><th>Pacote</th>               <th>Motivo</th></tr>
<correction certificatepatrol "Incompatível com versões mais novas do Firefox ESR">
<correction colorediffs-extension "Incompatível com versões mais novas do Thunderbird">
<correction dynalogin "Depende de simpleid que está prestes a ser removido">
<correction enigmail "Incompatível com versões mais novas do Thunderbird">
<correction firefox-esr "[armel] Não mais suportado (requer nodejs)">
<correction firefox-esr "[mips mipsel mips64el] Não mais suportado (precisa de
rustc mais recente)">
<correction getlive "Quebrado devido a mudanças pelo Hotmail">
<correction gplaycli "Quebrado devido a mudanças na API pelo Google">
<correction kerneloops "Serviço upstream não mais disponível">
<correction libmicrodns "Problemas de segurança">
<correction libperlspeak-perl "Problemas de segurança; sem manutenção">
<correction mathematica-fonts "Depende de local de download indisponível">
<correction pdns-recursor "Problemas de segurança; não suportado">
<correction predictprotein "Depende de profphd que está prestes a ser removido">
<correction profphd "Inutilizável">
<correction quotecolors "Incompatível com versões mais novas do Thunderbird">
<correction selenium-firefoxdriver "Incompatível com versões mais novas do Firefox ESR">
<correction simpleid "Não funciona com PHP7">
<correction simpleid-ldap "Depende de simpleid que está prestes a ser removido">
<correction torbirdy "Incompatível com versões mais novas do Thunderbird">
<correction weboob "Sem manutenção; já removido das últimas versões">
<correction yahoo2mbox "Quebrado a diversos anos">

</table>

<h2>Debian Installer</h2>
<p>O instalador foi atualizado para incluir correções incorporadas na versão
antiga (oldstable) pelo lançamento pontual.</p>

<h2>URLs</h2>

<p>A lista completa de pacotes que sofreram mudanças com essa revisão:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>A versão antiga (oldstable) atual:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable/">
</div>

<p>Atualizações propostas (proposed updates) para a versão antiga (oldstable):</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Informação da versão angita (oldstable) (notas de lançamento, errata, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Anúncios e informações de segurança:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Sobre o Debian</h2>

<p>O Projeto Debian é uma associação de desenvolvedores(as) de software livre
que voluntariamente doam seu tempo e esforço para produzir o sistema operacional
completamente livre Debian.</p>

<h2>Informações para contato</h2>

<p>Para mais informações, por favor visite a página do Debian em
<a href="$(HOME)/">https://www.debian.org/</a>, ou envie um e-mail (em inglês)
para &lt;press@debian.org&gt;, ou entre em contato com a equipe de lançamento
da versão antiga (oldstable) em &lt;debian-release@lists.debian.org&gt;.</p>
