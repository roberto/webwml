#use wml::debian::template title="Debian GNU/NetBSD" BARETITLE="yes" NOHEADER="yes"
#use wml::fmt::verbatim
#use wml::debian::translation-check translation="c9a7e0f78250fe2fea728e669907c9ee47374e1c"

#############################################################################
<div class="important">
<p><strong>
Tilpasningsarbejdet har længe været opgivet.  Der har ikke været opdateringer
siden oktober 2002.  Oplysningerne på denne side er bevaret af historiske 
årsager.
</strong></p>
</div>

<h1>Debian GNU/NetBSD</h1>

<p>Debian GNU/NetBSD (i386) var en tilpasning af Debians styresystem til 
NetBSD-kernen og libc (ikke at forveksle med Debians andre BSD-tilpasninger, som 
er baseret på glibc).  Da tilpasningen blev opgivet (omkring oktober 2002), var 
den i en tidlig fase af udviklingen, men kunne dog installeres fra bunden.</p>

<p>Der var også et forsøg på at sætte en Debian GNU/NetBSD-tilpasning i gang 
(alpha), som kunne køre fra en chroot under et <q>rigtigt</q> NetBSD-system 
(alpha), men den var ikke i stand til selv at starte, og anvendte primært den 
<q>rigtige</q> NetBSD's biblioteker.  En 
<a href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200201/msg00203.html">\
statusmeddelelse</a> blev sendt til listen.</p>


<h2>Historiske nyheder</h2>

<dl class="gloss">
  <dt class="new">06-10-2002:</dt>
  <dd>
      Eksperimentelle startdisketter er nu tilgængelige til installering af et
      Debian GNU/NetBSD-system.
  </dd>
  <dt>06-03-2002:</dt>
  <dd>
      Matthew har fået <a href="https://packages.debian.org/ifupdown">ifupdown</a>
      til at fungere.
  </dd>
  <dt>25-02-2002:</dt>
  <dd>Matthew rapporterer at shadow-understøttelse og PAM nu fungerer under 
      NetBSD.  <a href="https://packages.debian.org/fakeroot">fakeroot</a> lader
      til at virke under FreeBSD, mens der stadig er problemer under NetBSD.
  </dd>
  <dt>07-02-2002:</dt>
  <dd>Nathan har lige 
      <a href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00091.html">rapporteret</a>
      at han har fået Debian GNU/FreeBSD til at starte i flerbruger-tilstand.  
      Desuden arbejder han på en kun-pakker installering (ved hjælp af en 
      modificeret debootstrap) med en betydeligt mindre tarball.</dd>
  <dt>06-02-2002:</dt>
  <dd>Ifølge Joel har gcc-2.95.4 gennemført størstedelen af sin test-suite og
      er pakket.</dd>
  <dt>06-02-2002:</dt>
  <dd>X11 fungerer på NetBSD!  Igen, takket være Joel Baker.</dd>
  <dt>04-02-2002:</dt> 
  <dd>Det første skridt på vejen mod et Debian/*BSD-arkiv: <br />
      <a href="mailto:lucifer@lightbearer.com">Joel Baker</a>
      <a href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00067.html">
      annoncerede</a> et <kbd>dupload</kbd>-bart arkiv til FreeBSD- og
      NetBSD-Debian-pakker.</dd>
  <dt>03-02-2002:</dt>
  <dd>Debian GNU/NetBSD kan nu 
      <a href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00043.html">
      køre alene</a>!  Bemærk at en fungerende NetBSD stadig er krævet for at
      installere.</dd>
  <dt><strong>31-01-2002:</strong></dt>
  <dd>Debian GNU/*BSD-tilpasningen har nu en hjemmeside!</dd>
</dl>


<h2>Hvorfor Debian GNU/NetBSD?</h2>

<ul>
<li>NetBSD kører på hardware som ikke er understøttet af Linux. Tilpasning af
Debian til NetBSD-kernen øger antallet af platforme som kan køre et 
Debian-baseret system.</li>

<li>Debian GNU/Hurd-projektet demonstrer at Debian ikke er bundet til en 
specifik kerne. Men Hurd-kernen er stadig forholdsvis umoden - et Debian 
GNU/NetBSD-system kan anvendes i et produktionsmiljø.</li>

<li>Erfaringerne med at tilpasse Debian til NetBSD kan bruges til at tilpasse
Debian til andre kerner (såsom dem fra <a href="https://www.freebsd.org/">\
FreeBSD</a> og <a href="http://www.openbsd.org/">OpenBSD</a>).</li>

<li>I modsætning til projekter som <a href="http://www.finkproject.org/">Fink</a>
eller <a href="http://debian-cygwin.sf.net/">Debian GNU/w32</a>, findes Debian
GNU/NetBSD ikke for at give adgang til flere programmer eller et Unix-lignende
miljø under et eksisterende styresystem (*BSD-ports-træerne er allerede
omfattende, og der er ikke tvivl om at de giver adgang til et Unix-lignende
miljø). I stedet for vil en bruger eller administrator som er vant til et mere
traditionelt Debian-system, med det samme føle sig hjemmevant med et Debian
GNU/NetBSD-system, og kompentent i løbet af forholdsvis kort tid.</li>

<li>Alle kan ikke lide *BSD-ports-træet eller *BSD-brugermiljøet (dette er et
spørgsmål om personlig præferencer, snarere end nogen som helst form for 
kommentar vedrørende kvaliteten). Der er fremstillet Linux-distributioner som
giver *BSD-lignende 'ports' eller et *BSD-lignende brugermiljø til dem der 
kan lide BSD's brugermiljø, men også ønsker at bruge en Linux-kerne - Debian
GNU/NetBSD er det logiske, omvendte alternativ, som giver folk der kan lide 
GNU-brugermiljøet eller et Linux-pakkehåndteringssystem mulighed for at bruge 
NetBSD-kernen.</li>

<li>Fordi vi kan.</li>
</ul>


<h2>Ressourcer</h2>

<p>Der er en Debian GNU/*BSD-postliste.  De fleste historiske diskussioner om 
tilpasningen fandt sted der, og de er tilgængelige fra webarkivet på 
<url "https://lists.debian.org/debian-bsd/" />.</p>
