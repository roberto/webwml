#use wml::debian::translation-check translation="331afea01107f53bbf91cea35ba76463dba7f243" maintainer="Jean-Paul Guillonneau"
#use wml::debian::template title="Programme de Sam Hartman" BARETITLE="true" NOHEADER="true"
#include "$(ENGLISHDIR)/vote/style.inc"

<header>
<h1 class="title">Programme de DPL 2019 de Sam Hartman</h1>
</header>
<h2 id="biography">Biographie</h2>
<p>J’ai rejoint Debian en 2000. Les grandes sociétés possédaient un tas d’avantages
en ayant une infrastructure d’entreprise qui fournissait des authentifications,
des autorisations et des approvisionnements centralisés. En observant le Project
Athena au MIT, j’étais conscient combien cette infrastructure était bénéfique.
J’ai travaillé avec l’amont de Kerberos jusqu’à la fin de mon parcours
universitaire. Je voulais être sûr que la même infrastructure existait dans la
communauté du logiciel libre. Donc, j’ai commencé à empaqueter Kerberos et les
logiciels liés.</p>

<p>À ce moment-là, les logiciels de chiffrement ont été scindés dans la
distribution non-US à cause des lois sur l’exportation. Cela rendait mon travail
beaucoup plus difficile. J’ai empaqueté les logiciels, mais j’ai dû travailler
avec d’autres responsables pour construire la prise en charge de Kerberos et
d’autres technologies d’infrastructure dans de plus en plus de paquets.</p>

<p>Ben Collins a joint un groupe de personnes pour voir s’il était possible
d’avoir les logiciels de chiffrement dans l’archive principale. Ma contribution
pour cet initiative a été du côté légal plus que du côté technique. Avec un groupe
d’amis, nous avons passé plusieurs jours à parcourir ligne par ligne la
réglementation US pour préparer un ensemble de questions à poser aux juristes.
Avec Bdale et une aide généreuse d’HP, les réponses à mes questions nous ont
donné confiance en une voie légale à suivre. D’autres ont réalisé le travail
technique pour permettre à dak de fournir les notifications nécessaires pour
répondre aux règlements idiots du jour. Finalement, nous avons pu déplacer les
logiciels de chiffrement dans l’archive principale et supprimer la distribution
non-US.</p>

<p>Depuis lors, j’ai continué à m’impliquer dans Debian. Pendant quelques années
je n’ai fait qu’entretenir des paquets. D’autres années, Debian est devenue
une préoccupation centrale. Dans mon travail sur le
<a href="http://www.project-moonshot.org/">Projet Moonshot</a>, Debian a été
essentielle pour démontrer comment un nouveau mécanisme de sécurité pourrait être
intégré dans un système d’exploitation. Mon travail sur Kerberos et le Projet
Moonshot ne concernait que les logiciels libres. Quelquefois mes employeurs
s’intéressaient plus aux travaux propriétaires, quoique je me suis toujours
retrouvé dans des entreprises acceptant une implication dans la communauté du
logiciel libre. Au fil des années, le logiciel libre est devenu plus important
pour moi, et j’ai considéré que la liberté d’une solution m’importait plus lorsque
je cherchais à résoudre mes problèmes informatiques.</p>

<p>Pendant un certain nombre d’années, j’étais le directeur de la technologie du
Consortium Kerberos du MIT. Dans cet emploi, nous travaillions avec plusieurs
fournisseurs de système d’exploitation et d’autres parties prenantes dans
Kerberos pour satisfaire les besoins et l’évolution de notre communauté. J’ai
aussi travaillé pendant trois ans comme administrateur du périmètre de sécurité
de l’<a href="https://www.ietf.org/">Internet Engineering Taskforce</a>.</p>

<p>Lorsque je ne travaille pas sur Debian ou dans mon emploi de tous les jours,
j’écris, je lis, j’apprends à naviguer ou je profite pleinement de la vie.
L’année dernière, j’ai appris à devenir DJ. Cela m’a conduit à écrire mes
propres logiciels de DJ parce que l’existant est trop visuel pour être utilisé
par un DJ non voyant. Ce n’est pas encore empaqueté dans Debian parce que j’ai
besoin d’un greffon n’existant pas en amont et n’ai pas écrit suffisamment de
documentation pour que ce soit utilisable par quelqu’un d’autre.</p>

<p>La spiritualité est très importante pour moi. J’ai passé beaucoup de temps à
écrire et penser comment apporter plus de compassion, de communication et
d’amour dans le monde. Debian ressemble à un foyer à cause de sa communauté si
diverse et parce que nous voyons notre travail comme un moyen de rendre le monde
meilleur, pas comme un moyen d’avoir un salaire toujours croissant.</p>

<h2 id="keeping-debian-fun">Toujours garder Debian amusant</h2>
<p>Lucas Nussbaum a écrit un excellent
<a href="https://lists.debian.org/20190313230516.zbgts5hwrhqzicj4@xanadu.blop.info">résumé</a>
sur les responsabilités du DPL. Parmi celles-ci, je pense que la plus
importante est de conserver Debian amusante. Nous voulons des gens qui aiment
contribuer à Debian de façon qu’ils lui donnent une priorité dans la programmation
de leurs tâches. Nous voulons qu'il soit facile de travailler : les processus
et les interactions devraient être simplifiés. Lorsque des gens ont des
préoccupations ou des choses qui ne fonctionnent pas, nous voulons les écouter
et tenir compte de ce qu’ils expriment. Nous voulons que Debian soit accueillante aux
nouveaux contributeurs.</p>

<p>Debian n’est pas amusante quand nous faisons face à des discussions épuisantes,
longues et échauffées. Ce n’est pas agréable quand nous ne pouvons pas faire
avancer un projet parce nous ne pouvons pas découvrir comment faire que nos
idées soient prises en compte ou comment réellement participer. Debian n’est pas
amusante lorsque les procédés et les outils sont fastidieux. Lorsque les
équipes clefs sont cassées ou se bloquent, Debian n’est pas amusante pour
les membres de ces équipes ou pour ceux qui dépendent d’elles. <em>Debian n’est
pas amusante quand elle n’est par sûre, quand nous ne sommes pas respectés,
lorsque nous sommes persécutés ou quand nous sommes (plutôt que nos idées)
jugés</em>. Je défends notre Code de conduite.</p>

<p>Il existe plusieurs manières que j’envisage de développer pour garder Debian
amusante.</p>
<h3 id="listening-disagreement-without-escalation">Écoute : désaccord sans escalade</h3>
<p>J’ai eu le plaisir de servir dans le Comité technique (CT) de Debian. Dans ce rôle,
la chose dont je me souviens avec le plus de fierté est l’écriture d’une
<a href="https://lists.debian.org/debian-ctte/2015/03/msg00084.html">explication</a>
de pourquoi l’équipe de publication aurait dû rejeter les
changements dans busybox-static que le responsable considérait comme importants.
Le responsable n’a pas compris la décision de l’équipe de publication et a fait
appel au Comité technique, nous demandant soit d’annuler cette décision, soit de
l’expliquer.</p>

<p>Le ressenti importe. Avoir ses besoins émotionnels satisfaits est une partie
critique pour une communauté amusante et sécurisante. En 2012, j’ai découvert la
<a href="https://www.cnvc.org/">Communication non violente</a>. La CNV est un
cadriciel d’empathie qui m’a donné les outils pour comprendre et travailler avec
mes émotions et leur provenance. C’est un cadre pour parler de choses qui
sont importantes pour nous et pour communiquer, même si nous ne sommes pas
d’accord. Lorsque je comprend que quelqu’un a entendu ce que je dis, mes
muscles se relâchent et ma respiration s’apaise. Je me sens
soulagé et parfois joyeux, souvent même quand mon option préférée n’est pas
choisie. <strong>Debian nécessite le plus de tout cela.</strong></p>

<p>Mes pensées sur comment veiller à nos besoins émotionnels ont évolué au cours des ans
mais sont certainement influencées et inspirées par les remarques d’Enrico Zini
dans son
<a href="https://www.enricozini.org/blog/2018/debian/multiple-people">exposé
lors de Debconf 18</a>.</p>

<p>Debian est une communauté. Nous devons nous donner les uns les autres les
outils dont nous avons besoin pour réussir dans Debian. Une façon de traiter
avec respect les membres de notre communauté est de les écouter. Quand vous
recommandez quelqu’un, quand vous remplissez un rapport de gestionnaire
d’application recommandant que quelqu’un rejoigne notre communauté, ou quand vous
acceptez d’être un parrain, vous êtes en train de dire que quelqu’un est
suffisamment précieux pour notre communauté, pour que nous dépensions des
moyens pour qu’il puisse réussir. Le succès dans une communauté n’est pas
seulement technique mais aussi émotionnel.</p>

<p>Cependant, expliquer les choses, particulièrement de manière répétée,
particulièrement quand d’autres sont plus concentrés à nous convaincre qu’ils ont
raison que de commencer à comprendre, est exténuant. Une raison pour laquelle
j’ai choisi l’exemple de busybox-static est que je l’ai écrit, pas l’équipe de
publication. Elle était occupée à travailler sur une publication de Debian et j’étais
occupé à essayer de prendre soin d’un membre de notre communauté.</p>

<p>Un de mes principaux rôles comme DPL sera d’assurer que Debian soit une
communauté où nous pouvons être entendus et où nous avons l’opportunité de
parvenir à être compris, que nos idées l’emportent ou non. Je ferai
cela en participant personnellement à de telles médiations et en recrutant
d’autres personnes pour ces efforts de médiations. Finalement, j’espère
que beaucoup d’entre nous s’amélioreront en chercher à comprendre et à éviter les
débordements de discussions d'eux-mêmes. Depuis mon
<a href="https://lists.debian.org/debian-devel/2014/11/msg00133.html">courriel
sur la compassion et les systèmes init</a>, j’ai travaillé à trouver un lieu
pour réaliser cela. J’en ai parlé quand j’ai
<a href="https://lists.debian.org/debian-ctte/2015/03/msg00008.html">rejoint le
CT</a>. Le CT
 <a href="https://lists.debian.org/debian-ctte/2017/11/msg00011.html">a fini par
ne pas être</a> le bon endroit pour cette tâche.</p>

<p>Et pourtant le travail est toujours important. Ce que j’ai écrit sur les
<a href="https://hartmans.livejournal.com/97174.html">responsables</a> qui
abandonnent leurs paquets plutôt que de se concerter avec le CT est juste une
escalade de plus dans une longue série. Nous sommes conscients que des gens se
sont retirés parce qu’ils étaient frustrés. Je ne m’inquiète pas du
renouvellement : Debian continue d’être une communauté dynamique qui est
amusante pour beaucoup d’entre nous. Si des personnes nous ont quittés parce que
leurs besoins (ou même les objectifs de Debian) ont changé, tout va
bien. J’espère que nous pouvons faire mieux pour éviter que les gens
partent du projet amers et frustrés. J’espère que nous pouvons offrir de la compassion.</p>

<p>Le travail de médiation est trop important pour être géré par une seule personne.
Comment l’organiser n’est pas très clair. Ce qui est clair
aujourd’hui, c’est que le DPL est au centre de cette tâche. L’équipe
d’anti-harcèlement a aussi un rôle. Je serai volontaire pour les aider au début
de l’année et nous travaillons à déterminer comment je pourrais m’intégrer dans
ce processus indépendamment de l’élection du DPL. À long terme, il semble que
l’équipe d’anti-harcèlement, les responsables d’accréditation, et avec un peu
de chance le DPL, planifient une rencontre. Je pense que travailler sur comment
organiser ce travail de médiation de façon à réussir et éviter un surmenage est
une question importante à l'ordre du jour.</p>

<p>Comme DPL, la médiation et l’apport de compassion dans Debian seront parmi mes
objectifs clefs. J’ai des idées particulières sur la façon dont j’aimerais
essayer faire ce travail, donc je serai personnellement impliqué. Cependant,
bâtir une équipe de façon à pouvoir gérer plus d’une valeur de médiation de DPL
et donc que les futurs DPL puissent choisir leur propre objectif, est aussi
important que la réussite de médiations individuelles.</p>

<h3 id="decisions-that-take-forever">Prises de décisions interminables</h3>
<p>Nous avons perdu beaucoup de développeurs parce qu’ils étaient frustrés avec
nos outils et nos procédés. Une des plaintes les plus courantes que j’ai entendue
est que la large diversité de déroulement des opérations dans Debian vous
concerne quand vous voulez contribuer à un grand nombre de paquets. Je sais
que pour moi-même, nos procédés rendent si difficile de corriger un bogue
critique pour la publication dans un paquet dont je ne suis pas responsable que
je suis moins susceptible de le faire.</p>

<p>Plus que la seule frustration avec les outils et procédés, existe la
perception que faire des modifications est difficile. Nous avons eu des tas de
discussions sur les améliorations potentielles, mais quelquefois il est difficile
de prendre réellement une décision. Comme quelqu’un l’a déclaré récemment sur
IRC, il est difficile d’aller de l’avant quand la frustration de quelqu’un est
qu’une autre personne veut la main sur le système.</p>

<p>Je ne pense pas que le DPL puisse ou devrait trancher ces problèmes. Cependant,
comme DPL je voudrais avoir des outils pour faire avancer ces discussions, être
sûr d’être à l’écoute de la communauté et finalement prendre une décision.
Quelquefois la décision devrait être que nous aimons les choses telles qu’elles
sont. Même cela est bénéfique : cela met fin à la discussion. Si nous faisons un
bon travail, même les partisans d’autres approches se sentiront écoutés. Même
s’ils nous quittent, cela sera avec plus d’empathie et de respect que nous avons
quelquefois aujourd’hui.</p>

<p>J’ai passé beaucoup d’années à construire un consensus à la fois dans Debian
et dans des organisations telles que <a href="https://www.ietf.org/">IETF</a>.
Je sais comment synthétiser où nous en sommes dans une discussion, relever là
où il y a consensus et quels points semblent bloquer le processus.</p>

<p>Il est utile que le DPL examine ces problèmes et trouve quand prendre une
décision serait bénéfique. En général, ce sont des secteurs où un plus large
soutien aiderait à renforcer des gens essayant de réaliser quelque chose et
où suffisamment de discussions ont eu lieu pour que nous soyons en position de parvenir à
une décision. Une fois prise, une décision est bénéfique. Le DPL possède
des outils pour conduire le processus même si un consensus n’est pas possible.</p>

<p>Le DPL peut transférer des problèmes au Comité technique. N’importe quel
développeur peut le faire, mais je pense que le DPL peut le faire d’une manière
moins conflictuelle qu’un développeur individuel. « Nous avons discuté de cela
le plus possible dans le cadre du projet. Je pense qu’une décision est bénéfique pour le
projet. Voici où nous sommes arrivés dans nos discussions » me semble moins
conflictuel que de demander de passer outre quelque responsable.</p>

<p>Je pense que la possibilité du DPL de proposer des résolutions générales est
sous-utilisée. Je pense qu’une proposition de GR par le DPL, incluant toutes les
options majeures pourrait être une manière moins conflictuelle pour clore un
problème qu’un développeur individuel proposant une GR et recherchant des
seconds. Si le procédé ressemble beaucoup plus à un sondage du projet et à un
guidage de la résolution vers une discussion politique, je pense que voter amène
les choses à leur fin.</p>

<p>J’espère trouver des moyens de réduire les stigmates associés au fait
d’élargir des problèmes importants à une plus large audience. Je pense que ce
devrait toujours être le cas lorsqu’outrepasser une décision particulière n’est
possible qu’à un coût plus grand. Cependant, souvent lorsque nous
avons dû faire face à une décision controversée, nous avons réalisé que c’était
une question politique plus large à laquelle il est répondu et qui anticipe de
futures décisions. Une fois détaché de la décision initiale, je pense que
répondre à ces questions politiques peut être bénéfique. Quelquefois, il serait
utile de répondre à ces questions dans un forum plus important que le groupe
rendant la décision originale. Élargir le forum comme cela ne devrait pas être
vu comme un manque de confiance dans aucune de nos équipes ou faiseurs de
décision. Il s’agit plutôt de travailler avec une plus grande communauté.</p>

<h2 id="point-of-contact-explaining-debian">Point de contact : explication de Debian</h2>
<p>Le DPL remplit un rôle important comme point de contact pour Debian. Le DPL
a la possibilité d’expliquer Debian aux autres organisations et à la presse. Le
DPL a la possibilité de comprendre les préoccupations d’autres organisations et
de les aider à contacter les parties concernées de Debian pour progresser.</p>

<p>Je pense que ce travail est très important et je pense que je suis bon pour ça.
J’ai l’expérience de la construction de ces sortes de pont avec mon emploi
précédent comme directeur de la technologie du Consortium Kerberos du MIT ainsi que
dans mes emplois à l’IETF. Je n’ai pas eu ce rôle officiellement dans Debian, mais
j’ai servi régulièrement de porte-parole dans d’autres cercles professionnels.</p>

<h2 id="speaking-at-conferences">Discours dans des conférences</h2>
<p>Le DPL traditionnellement a joué le rôle de représentant de Debian dans des
conférences. Ces dernières années, je n’ai pas réellement participé à
beaucoup de conférences informatiques. Si je suis élu, j’envisage de prêter plus
d’attention sur les conférences que je ne l’ai fait récemment. Je pense que c’est
un domaine dans lequel je peux m’améliorer.</p>

<p>Même ainsi, je ne peux pas passer autant de temps dans des conférences que
certains anciens DPL. Je pense que c’est un domaine où travailler avec une équipe
devrait bien fonctionner.</p>

<h2 id="supporting-free-software">Soutien du logiciel libre</h2>
<p>La communauté du logiciel libre équilibre quelques compromis intéressants. Il
y a plus de logiciels libres que jamais auparavant. Linux et les logiciels au
code source ouvert sont largement acceptés. Cependant, un certain nombre de
groupes bien pourvus travaillent très durement pour être sûrs de contrôler à quelle
liberté leurs utilisateurs auront droit. Ils travaillent pour éviter le copyleft
et pour utiliser le logiciel libre comme plateforme sur laquelle leurs solutions
propriétaire sont basées. Même quand le logiciel est libre, il se connecte souvent
à des services web non libres. Une liberté de l’utilisateur serait limitée par les
agréments autour de ces services web et en n’ayant pas de contrôle sur leurs
données.</p>

<p>Debian joue un rôle critique dans cette communauté parce que nous sommes un
point de rencontre entre l’amont, l’aval et le spectre entier des points de vue
de ce que le logiciel libre devrait être et sur comment nous devrions protéger
la liberté de nos utilisateurs. Aussi longtemps que vous suivez les DFSG et le
contrat social, dans votre travail sur Debian, vous êtes le bienvenu ici.</p>

<p>Je pense que le DPL a un rôle important dans le travail dans cet environnement.
Les spécificités tendent à être différentes d’année en année. Debian a des opportunités
pour aider certaines initiatives ou faire entendre sa voix. Je ne sais pas ce
que 2019 va apporter, mais reconnais que ce sera important
et quelque chose sur quoi je me concentrerai en tant que DPL.</p>

<h2 id="supporting-ongoing-efforts">Soutien des efforts en cours</h2>
<p>Le DPL se doit de soutenir les efforts en cours. Dans beaucoup de cas, cela
implique rester en dehors en attendant que l’aide du DPL soit nécessaire. Je
soutiens l’effort en cours de toutes les équipes déléguées.</p>

<p>Je voudrais rappeler spécialement les efforts des équipes Diversity et
Outreach. Ces équipes travaillent sur des améliorations culturelles dans Debian
et travaillent pour attirer de nouveaux membres dans notre communauté. Les deux
efforts à long terme sont importants pour faire de Debian une communauté
divertissante dans le futur.</p>

<h2 id="finances">Finances</h2>
<p>Je continuerai à soutenir une politique similaire d’approbation des dépenses
à celle suivie par le DPL actuel. En particulier, je soutiens l’approbation des
dépenses pour la participation aux chasses aux bogues de Debian.</p>

<p>J’ai travaillé sur des budgets aussi imposants que celui de Debian, à la fois comme
propriétaire d’une petite entreprise et comme gestionnaire dans des organisations
plus grandes. Dans ces occupations, j’avais accès à plus d’informations
financières que j’espère avoir comme DPL. Ma compréhension est que notre argent
est réparti à travers plusieurs organisations de confiance, chacune avec ses
propres politiques et procédures. En conséquence, c’est un challenge pour Debian
de comprendre entièrement son financement.</p>

<p>Je pratiquerai un niveau approprié de diligence raisonnable et soutiendrai les
efforts en cours de l’équipe trésorière. Cependant, améliorer les finances de
Debian et la responsabilité financière sont quelque chose que je n’attends pas être
des points clefs si je suis élu DPL. C’est un travail formidable, mais cette
année, je pense que les facteurs humains et garder Debian amusante sont plus
importants. Je suis très ouvert pour travailler avec des gens qui veulent
avancer dans ce domaine.</p>

<h2 id="dpl-team">Équipe du DPL</h2>
<p>Il y a eu un tas de discussions cette année à propos de faire des changements
dans la gouvernance de Debian et de créer une équipe de DPL plutôt qu’un seul
DPL. Je pense qu’une équipe de DPL est une grande idée, mais je ne vois pas de
modifications significatives nécessaires dans la gouvernance.</p>

<p>Je soupçonne que les délégations existantes de publicité, trésorerie et
quelques autres ont débuté comme tâches que le DPL réalisait mais étaient
devenues trop lourdes à gérer pour une seule personne. Pour quelques tâches, une
délégation serait appropriée. Pour d’autres, il suffirait simplement travailler
avec d’autres personnes.</p>

<p>Je pense qu’une équipe peut aider dans les domaines suivants :</p>
<ul>
<li><p>le travail de médiation est suffisamment important pour qu’il soit nécessaire de
le répartir entre plusieurs personnes ;</p></li>
<li><p>la responsabilité de représenter Debian aux conférences pourrait être
répartie au sein d'une équipe ;</p></li>
<li><p>j’aimerais répartir une partie du travail financier en cours dans une
équipe.</p></li>
</ul>
<p>Je suis ouvert pour employer des équipes dans d’autres domaines où le travail
est établi comme plus important que celui attendu.</p>
<hr />
<p>Globalement, je pense que Debian est une grande communauté. Le panorama
entourant les systèmes d’exploitation change et évolue rapidement. Les gens
trouvent de nouvelles façons d’utiliser Debian ainsi que les outils et blocs de
construction que nous fournissons. Notre universalité est plus importante que
jamais. Nous pouvons réussir comme communauté en nous emparant, accueillant et
construisant sur ces efforts d’où qu’ils viennent, de nos membres ou de
l’extérieur.</p>

<p>Je suis enthousiaste à propos de Debian. Je suis impatient de construire une grande
communauté encore meilleure. Je suis impatient de parler avec vous tous puisque nous
allons explorer et parfaire les plans de tous les candidats pendant les prochaines
semaines. Merci pour votre attention.</p>

<h1 id="rebuttal">Réfutations</h1>

<p>Suivre le début de la discussion a été excitant. Nous avons constaté le grand
intérêt à améliorer notre projet. Heureusement, parmi un tas de propositions,
nous pouvons choisir toutes les précédentes plutôt que d’en choisir juste une.</p>

<p>Je pense que je suis le meilleur choix de DPL parce que je suis bon pour faire
consensus, prendre des décisions et à communiquer. Cependant, je regarde comment
je pourrais travailler avec les autres candidats et le projet dans son ensemble.
Je voudrais prendre du temps pour examiner comment précisément j’aimerais
travailler avec les autres candidats.</p>

<h2 id="jonathan-carter">Jonathan Carter</h2>

<p>Si je suis élu, je voudrais travailler avec Jonathan pour mettre en œuvre un
certain nombre de ses idées :</p>
<ul>
<li><p>j’espère que Jonathan va mener à bien son projet de recenser les
cent petits problèmes et identifier les domaines qui entravent le travail des
membres de Debian ;</p>
</li>
<li><p>j’aimerais expérimenter l’idée de rencontre de communauté que Jonathan a
évoquée. Je pense que des rencontres plus larges (IRC ou voix/vidéo) peut nous
aider à acquérir un sentiment de communauté et régler les préoccupations ;</p>
</li>
<li><p>je pense que poursuivre l’idée de Jonathan pour des rencontres de femmes
avec le concours de l’équipe de diversité serait une bonne idée. Je veux être
sûr que les femmes dans Debian en bénéficieront vraiment. J’ai parlé avec
Jonathan et il a commencé le travail de base. Aussi je suis favorable à cette
idée à condition d’obtenir l’assentiment nécessaire de la communauté ;</p>
</li>
<li><p>le travail doit continuer pour prendre en charge le matériel libre et
aider les développeurs à y parvenir.</p></li>
</ul>

<h2 id="joerg-jaspert">Joerg Jaspert</h2>

<p>J’ai été réellement ravi d’apprendre que Joerg est intéressé par le projet de
remplacer les paquets source de Debian par Git. Je pense que le projet devrait
l’aider et lui fournir l’aide qu’il pense nécessaire en confirmant que c’est
la direction que nous voulons et en mettant en œuvre une solution.</p>
<p>J’espère travailler avec Joerg dans toutes ses responsabilités.</p>

<h2 id="martin-michlmayr">Martin Michlmayr</h2>

<p>Martin a évoqué deux idées intéressantes :</p>
<ul>
<li><p>trouver plus d’entreprises pour aider Debian et les encourager à
contribuer à Debian ;</p></li>
<li><p>utiliser des subventions et d’autres mécanismes pour accroitre le nombre
de membres de Debian payés pour leur travail à propos de Debian.</p></li>
</ul>
<p>Je suis intéressé par ces idées. Martin n’a pas encore répondu à ma question
d’étudier comment régler les problèmes qui sont apparus la dernière fois que des
idées semblables ont été soulevées. En supposant que nous puissions trouver de
bonnes réponses à ces problèmes, je suis très favorable à ces idées et
approcherai Martin et lui demanderai s’il est disponible pour faire avancer ces
idées.</p>
