#use wml::debian::translation-check translation="3508f9a531af1aca0b0e166eb9e274f958f07db9" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans BIND, une
implémentation de serveur DNS.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2795">CVE-2022-2795</a>

<p>Yehuda Afek, Anat Bremler-Barr et Shani Stajnrod ont découvert qu'un
défaut dans le code du solveur peut faire que <q>named</q> consomme une
quantité excessive de temps lors du traitement de grandes délégations,
dégradant significativement les performances du solveur ce qui a pour
conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3080">CVE-2022-3080</a>

<p>Maksym Odinintsev a découvert que le solveur peut planter quand le cache
de données périmées et les réponses périmées sont activées avec un
<q>stale-answer-timeout</q> à zéro. Un attaquant distant peut tirer
avantage de ce défaut pour provoquer un déni de service (plantage du démon)
au moyen de requêtes contrefaites pour l'occasion au solveur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-38177">CVE-2022-38177</a>

<p>Le code de vérification de DNSSEC pour l'algorithme ECDSA est exposé à
un défaut de fuite de mémoire. Un attaquant distant peut tirer avantage de
ce défaut pour provoquer la consommation de ressources par BIND, avec pour
conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-38178">CVE-2022-38178</a>

<p>Le code de vérification de DNSSEC pour l'algorithme EdDSA est exposé à
un défaut de fuite de mémoire. Un attaquant distant peut tirer avantage de
ce défaut pour provoquer la consommation de ressources par BIND, avec pour
conséquence un déni de service.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 1:9.16.33-1~deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bind9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de bind9, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/bind9">\
https://security-tracker.debian.org/tracker/bind9</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5235.data"
# $Id: $
