#use wml::debian::translation-check translation="eff7900824fe89c99c2c82825cc8c0ab462b94f3" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le programme de
compression lrzip qui pouvaient avoir pour conséquences un déni de service
ou éventuellement l'exécution de code arbitraire.</p>

<p>Pour la distribution oldstable (Buster), ces problèmes ont été corrigés
dans la version 0.631+git180528-1+deb10u1. Cette mise à jour corrige aussi
les
<a href="https://security-tracker.debian.org/tracker/CVE-2021-27345">CVE-2021-27345</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2020-25467">CVE-2020-25467</a> et
<a href="https://security-tracker.debian.org/tracker/CVE-2021-27347">CVE-2021-27347</a>.</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 0.641-1+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets lrzip.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de lrzip, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/lrzip">\
https://security-tracker.debian.org/tracker/lrzip</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5145.data"
# $Id: $
