#use wml::debian::translation-check translation="53ca362f9b58e081235f8e56fb5ad30a5a8a1ad9" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Apache Commons Configuration, une bibliothèque Java fournissant une
interface de configuration générique, réalise une interpolation de
variables permettant que des propriétés soient évaluées et développées
dynamiquement. De la version 2.4 à la version 2.7 comprise, le jeu
d'instances de <q>Lookup</q> comprenait des interpolateurs qui pouvaient
avoir pour conséquences l'exécution de code arbitraire ou le contact avec
des serveurs distants. Ces recherches sont :</p>
<ul>
<li><q>script</q> – exécution d'expressions utilisant le moteur d'exécution
de script JVM (javax.script) ;</li>
<li><q>dns</q> – résolution des enregistrements DNS ;</li>
<li><q>url</q> – chargement de valeurs à partir d'URL, y compris d'un
serveur distant.</li>
</ul>
<p>Les applications utilisant l'interpolation par défaut dans les versions
affectées peuvent être vulnérables à une exécution de code à distance ou au
contact non intentionnel avec des serveurs distants si des valeurs de
configuration non sûres sont utilisées.</p>

<p>Pour la distribution stable (Bullseye), ce problème a été corrigé dans
la version 2.8.0-1~deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets commons-configuration2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de commons-configuration2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/commons-configuration2">\
https://security-tracker.debian.org/tracker/commons-configuration2</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5290.data"
# $Id: $
