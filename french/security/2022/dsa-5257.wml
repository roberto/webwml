#use wml::debian::translation-check translation="e97f094e81f26c7a43420d43c5e0db2a2d42478b" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pouvaient conduire à une élévation de privilèges, un déni de service ou des
fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4037">CVE-2021-4037</a>

<p>Christian Brauner a signalé que la fonction inode_init_owner pour le
système de fichiers XFS dans le noyau Linux permet aux utilisateurs locaux
de créer des fichiers avec la propriété d'un groupe non prévu permettant à
des attaquants d'élever leurs privilèges en rendant un simple fichier
exécutable et SGID.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0171">CVE-2022-0171</a>

<p>Mingwei Zhang a signalé qu'un problème d'incohérence de cache dans l'API
SEV dans le sous-système KVM peut avoir pour conséquence un déni de
service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1184">CVE-2022-1184</a>

<p>Un défaut a été découvert dans le pilote du système de fichiers ext4 qui
peut conduire à une utilisation de mémoire après libération. Un utilisateur
local autorisé à monter des systèmes de fichiers arbitraires pouvait
exploiter cela pour provoquer un déni de service (plantage ou corruption de
mémoire) ou éventuellement pour une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2602">CVE-2022-2602</a>

<p>Une situation de compétition entre le traitement d'une requête io_uring
et le ramasse-miettes d'un socket Unix a été découvert. Un attaquant peut
tirer avantage de ce défaut pour une élévation locale de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2663">CVE-2022-2663</a>

<p>David Leadbeater a signalé des défauts dans le module du protocole de
connexion nf_conntrack_irc. Quand ce module est activé sur un pare-feu, un
utilisateur externe sur le même réseau IRC qu'un utilisateur interne
pouvait exploiter son analyse laxiste pour ouvrir des ports TCP arbitraires
dans le pare-feu, révéler son adresse IP publique ou pour bloquer sa
connexion IRC au pare-feu.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3061">CVE-2022-3061</a>

<p>Un défaut a été découvert dans le pilote i740 qui peut avoir pour
conséquence un déni de service.</p>

<p>Ce pilote est désactivé dans les configurations officielles du noyau de
Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3176">CVE-2022-3176</a>

<p>Un défaut d'utilisation de mémoire après libération a été découvert dans
le sous-système io_uring qui peut avoir pour conséquence une élévation
locale de privilèges vers ceux du superutilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3303">CVE-2022-3303</a>

<p>Une situation de compétition dans la fonction snd_pcm_oss_sync dans le
sous-système son dans le noyau Linux, due à un verrouillage incorrect, peut
avoir pour conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-20421">CVE-2022-20421</a>

<p>Une vulnérabilité d'utilisation de mémoire après libération a été
découverte dans la fonction binder_inc_ref_for_node dans le pilote de
création de lien d'Android. Sur les systèmes où le créateur de lien est
chargé, un utilisateur local pouvait exploiter cela pour une élévation de
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39188">CVE-2022-39188</a>

<p>Jann Horn a signalé une situation de compétition dans le traitement par
le noyau du <q>démappage</q> de certaines plages de mémoire. Quand un
pilote créait un mappage de mémoire avec l'indicateur VM_PFNMAP, ce que
font de nombreux pilotes GPU, le mappage de mémoire pouvait être supprimé
et libéré avant d'avoir été purgé des TLB du processeur. Cela pouvait avoir
pour conséquence une utilisation de mémoire de page après libération. Un
utilisateur local doté de l'accès à ce type de périphérique pouvait
exploiter cela pour provoquer un déni de service (plantage ou corruption de
mémoire) ou éventuellement pour une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39842">CVE-2022-39842</a>

<p>Un dépassement d'entier a été découvert dans le pilote vidéo pxa3xx-gcu
qui pouvait conduire à une écriture de tas hors limites.</p>

<p>Ce pilote n'est pas activé dans les configurations du noyau officiel
de Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-40307">CVE-2022-40307</a>

<p>Une situation de compétition a été découverte dans le pilote
capsule-loader d'EFI, qui pouvait conduire à une utilisation de mémoire
après libération. Un utilisateur local autorisé à accéder à ce périphérique
(/dev/efi_capsule_loader) pouvait exploiter cela pour provoquer un déni de
service (plantage ou corruption de mémoire) ou éventuellement pour une
élévation de privilèges. Cependant, ce périphérique est normalement
accessible uniquement au superutilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41674">CVE-2022-41674</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2022-42719">CVE-2022-42719</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2022-42720">CVE-2022-42720</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2022-42721">CVE-2022-42721</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2022-42722">CVE-2022-42722</a>

<p>Soenke Huster a découvert plusieurs vulnérabilités dans le sous-système
mac80211, déclenchées par des trames, qui peuvent avoir pour conséquence un
déni de service ou l'exécution de code arbitraire.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 5.10.149-1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5257.data"
# $Id: $
