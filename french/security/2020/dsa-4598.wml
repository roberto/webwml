#use wml::debian::translation-check translation="f141e134bdc80eb626430d22803e62ca0e69b576" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Simon Charette a signalé que la fonctionnalité de réinitialisation de
mot de passe dans Django, un environnement de développement web de haut
niveau en Python, utilisait une requête Unicode, sans tenir compte de la
casse, pour retrouver les comptes correspondant à l'adresse courriel
demandant la réinitialisation de mot de passe. Un attaquant peut tirer
avantage de ce défaut pour éventuellement récupérer des jetons de
réinitialisation de mot de passe et détourner des comptes.</p>

<p>Pour plus de détails, veuillez consulter la page
<a href="https://www.djangoproject.com/weblog/2019/dec/18/security-releases/">\
https://www.djangoproject.com/weblog/2019/dec/18/security-releases/</a>.</p>

<p>Pour la distribution oldstable (Stretch), ce problème a été corrigé
dans la version 1:1.10.7-2+deb9u7.</p>

<p>Pour la distribution stable (Buster), ce problème a été corrigé dans la
version 1:1.11.27-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-django.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python-django,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python-django">\
https://security-tracker.debian.org/tracker/python-django</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4598.data"
# $Id: $
