#use wml::debian::translation-check translation="b0edd1764a76437c80f301cbaf810e5905904df2" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans OpenSSL, une boîte à
outils SSL (Secure Socket Layer). Cela pouvait avoir pour conséquences un
chiffrement incomplet, des attaques par canal auxiliaire, un déni de
service ou une divulgation d'informations.</p>

<p>Pour plus de détails, consultez les avertissements amont :
<a href="https://www.openssl.org/news/secadv/20220705.txt">https://www.openssl.org/news/secadv/20220705.txt</a> et
<a href="https://www.openssl.org/news/secadv/20230207.txt">https://www.openssl.org/news/secadv/20230207.txt</a></p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 1.1.1n-0+deb11u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openssl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openssl, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openssl">\
https://security-tracker.debian.org/tracker/openssl</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5343.data"
# $Id: $
