#use wml::debian::translation-check translation="af029e854fe1fececae28ff370ad9ebc1a58228c" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité de sécurité a été découverte dans libhtmlcleaner-java,
une bibliothèque Java d'analyse du HTML. Un attaquant pouvait provoquer un
déni de service (StackOverflowError) si l'analyseur travaillait sur une
entrée fournie par l'utilisateur avec des éléments HTML profondément
imbriqués. Cette mise à jour introduit une nouvelle limite de profondeur
d'imbrication qui peut être surchargée dans les propriétés du nettoyeur.</p>

<p>Pour la distribution oldstable (Bullseye), ce problème a été corrigé
dans la version 2.24-1+deb11u1.</p>

<p>Pour la distribution stable (Bookworm), ce problème a été corrigé dans
la version 2.26-1+deb12u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libhtmlcleaner-java.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libhtmlcleaner-java,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libhtmlcleaner-java">\
https://security-tracker.debian.org/tracker/libhtmlcleaner-java</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5471.data"
# $Id: $
