#use wml::debian::translation-check translation="4692989ebeae7e64268c668e9689af22f819d9ed" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Les vulnérabilités suivantes ont été découvertes dans le moteur web
WebKitGTK :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-39928">CVE-2023-39928</a>

<p>Marcin Noga a découvert qu'une page web contrefaite pour l'occasion
pouvait exploiter une vulnérabilité dans l'API MediaRecorder pour provoquer
une corruption de mémoire et éventuellement l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-41074">CVE-2023-41074</a>

<p>Junsung Lee et Me Li ont découvert que le traitement d'un contenu web
pouvait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-41993">CVE-2023-41993</a>

<p>Bill Marczak et Maddie Stone ont découvert que le traitement d'un
contenu web pouvait conduire à l'exécution de code arbitraire. Apple a été
informé d'un rapport indiquant que ce problème peut avoir été activement
exploité.</p></li>

</ul>

<p>Pour la distribution oldstable (Bullseye), ces problèmes ont été
corrigés dans la version 2.42.1-1~deb11u1.</p>

<p>Pour la distribution stable (Bookworm), ces problèmes ont été corrigés
dans la version 2.42.1-1~deb12u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets webkit2gtk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de webkit2gtk, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5527.data"
# $Id: $
