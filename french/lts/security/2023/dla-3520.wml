#use wml::debian::translation-check translation="55c74299730c420830c656129414b39df1038c22" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité de sécurité a été découverte dans libhtmlcleaner-java,
une bibliothèque Java d'analyse du HTML. Un attaquant pouvait provoquer un
déni de service (StackOverflowError) si l'analyseur travaillait sur une
entrée fournie par l'utilisateur avec des éléments HTML profondément
imbriqués. Cette mise à jour introduit une nouvelle limite de profondeur
d'imbrication qui peut être surchargée dans les propriétés du nettoyeur.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2.21-5+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libhtmlcleaner-java.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libhtmlcleaner-java,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libhtmlcleaner-java">\
https://security-tracker.debian.org/tracker/libhtmlcleaner-java</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3520.data"
# $Id: $
