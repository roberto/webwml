#use wml::debian::translation-check translation="8afbe3a00a5793e3d8438b46c790fa3ca5eb8340" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans freelrdp2, une
implémentation libre de RDP (Remote Desktop Protocol). Ces vulnérabilités
pouvaient permettre des lectures hors limites de tampon, des dépassements de
tampons ou d’entiers, une utilisation de mémoire après libération ou des
vecteurs de déni de service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-4030">CVE-2020-4030</a>

<p>Dans FreeRDP avant la version 2.1.2, une lecture hors limites existait dans
TrioParse. La connexion pouvait contourner les vérifications de longueur à
cause d’un dépassement d'entier (correction faite dans la version 2.1.2).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-4031">CVE-2020-4031</a>

<p>Dans FreeRDP avant la version 2.1.2, une utilisation de mémoire après
libération existait dans gdi_SelectObject. Tous les clients FreeRDP utilisant le
mode compatible avec /relax-order-checks étaient touchés (correction faite dans
la version 2.1.2).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-4032">CVE-2020-4032</a>

<p>Dans FreeRDP avant la version 2.1.2, une vulnérabilité de conversion d’entier
existait dans update_recv_secondary_order. Tous les clients avec
+glyph-cache/relax-order-checks étaient touchés (correction faite dans
la version 2.1.2).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-4033">CVE-2020-4033</a>

<p>Dans FreeRDP avant la version 2.1.2, une lecture hors limites existait dans
RLEDECOMPRESS. Tous les clients basés sur FreeRDP avec des sessions avec une
profondeur de couleur &lt; 32 étaient touchés (correction faite dans la
version 2.1.2).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11017">CVE-2020-11017</a>

<p>Dans FreeRDP jusqu’à la version 2.0.0, en fournissant une entrée trafiquée
un client malveillant pouvait créer une double libération de zone de mémoire et
planter le serveur (correction faite dans la version 2.1.0).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11018">CVE-2020-11018</a>

<p>Dans FreeRDP jusqu’à la version 2.0.0, un épuisement de ressource était
possible. Un client malveillant pouvait déclencher une lecture hors limites,
causant une allocation de mémoire de taille aléatoire (correction faite dans
la version 2.1.0).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11019">CVE-2020-11019</a>

<p>Dans FreeRDP jusqu’à la version 2.0.0, lors d’une exécution avec le logger
défini à <q>WLOG_TRACE</q>, un plantage d’application pouvait se produire dû
à une lecture d’indice non valable de tableau. Des données pouvaient être
affichées sous forme de chaine dans un terminal local (correction faite dans
la version 2.1.0).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11038">CVE-2020-11038</a>

<p>Dans FreeRDP jusqu’à la version 2.0.0, un dépassement d'entier pour un
dépassement de tampon existait. Lors de l’utilisation de redirection de /video,
un serveur manipulé pouvait demander au client d’allouer un tampon de taille
plus petite que demandée à cause d’un dépassement d'entier dans le calcul de la
taille. Dans des messages ultérieurs, le serveur pouvait manipuler le client
pour écrire des données hors limites du tampon alloué (correction faite dans
la version 2.1.0).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11039">CVE-2020-11039</a>

<p>Dans FreeRDP jusqu’à la version 2.0.0, lors de l’utilisation d’un serveur
trafiqué avec une redirection activée d’USB, une mémoire (presque) arbitraire
pouvait être lue ou écrite à cause d’un dépassement d'entier dans la
vérification de taille (correction faite dans la version 2.1.2).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11040">CVE-2020-11040</a>

<p>Dans FreeRDP jusqu’à la version 2.0.0, une lecture hors limites de mémoire existait
dans clear_decompress_subcode_rlex, visualisée sur l’écran comme couleur
(correction faite dans la version 2.1.0).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11041">CVE-2020-11041</a>

<p>Dans FreeRDP jusqu’à la version 2.0.0, un indice extérieur au tableau
contrôlé était utilisé non vérifié comme configuration pour le dorsal audio (alsa,
oss, pulse, ...). Le résultat le plus probable était un plantage de l’instance du
client suivi par un problème de son ou une déconnexion de session. Si un
utilisateur ne pouvait pas mettre à niveau, un contournement était de désactiver
le son (correction faite dans la version 2.1.0).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11042">CVE-2020-11042</a>

<p>Dans FreeRDP après 1.1 et avant 2.0.0, une lecture hors limites existait dans
update_read_icon_info. Elle permettait de lire une quantité définie par
l’attaquant de la mémoire du client (32 bits non signé -> 4Go) vers un tampon
intermédiaire. Cela pouvait être utilisé pour planter le client ou stocker de
l’information pour une exploitation future (correction faite dans la
version 2.0.0).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11043">CVE-2020-11043</a>

<p>Dans FreeRDP jusqu’à la version 2.0., une lecture hors limites existait dans
rfx_process_message_tileset. Des données non valables fournies au décodeur RFX
aboutissaient à de mauvaises couleurs sur l’écran (correction faite dans
la version 2.1.0).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11044">CVE-2020-11044</a>

<p>Dans FreeRDP après 1.2 et avant 2.0.0, une double libération de zone de
mémoire dans update_read_cache_bitmap_v3_order plantait l’application cliente
si des données corrompues d’un serveur trafiqué étaient analysées (correction
faite dans la version 2.0.0).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11045">CVE-2020-11045</a>

<p>Dans FreeRDP après 1.0 et avant 2.0.0, une lecture hors limites existait dans
update_read_bitmap_data qui permettait une lecture de mémoire du client. Le
résultat était affiché sur l’écran comme couleur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11046">CVE-2020-11046</a>

<p>Dans FreeRDP après 1.0 et avant 2.0.0, une recherche hors limites de flux
dans update_read_synchronize pouvait conduire à une lecture hors limites
postérieure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11047">CVE-2020-11047</a>

<p>Dans FreeRDP après 1.1 et avant 2.0.0, une lecture hors limites existait dans
autodetect_recv_bandwidth_measure_results. Un serveur malveillant pouvait
extraire jusqu’à huit octets de mémoire du client avec un message trafiqué en
fournissant une courte entrée et lisant les données de mesure résultantes
(correction faite dans la version 2.0.0).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11048">CVE-2020-11048</a>

<p>Dans FreeRDP après 1.0 et avant 2.0.0, une lecture hors limites existait.
Elle permettait seulement d’abréger la session. Aucune extraction de données
n’était possible (correction faite dans la version 2.0.0).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11049">CVE-2020-11049</a>

<p>Dans FreeRDP après 1.1 et avant 2.0.0, une lecture hors limites de mémoire
de client existait qui était passée à l’analyseur de protocole (correction faite
dans la version 2.1.2).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11058">CVE-2020-11058</a>

<p>Dans FreeRDP après 1.1 et avant 2.0.0, une recherche hors limites dans le
flux dans rdp_read_font_capability_set pouvait conduire à une lecture hors
limites postérieure. En conséquence, un client manipulé ou un serveur pouvait
forcer une déconnexion à cause d'une lecture de données non valables (correction
faite dans la version 2.0.0).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11085">CVE-2020-11085</a>

<p>Dans FreeRDP jusqu’à la version 2.0.0, une lecture hors limites dans
cliprdr_read_format_list existait. Une lecture de données au format
presse-papiers (par le client ou le serveur) pouvait être hors limites
(correction faite dans la version 2.1.0).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11086">CVE-2020-11086</a>

<p>Dans FreeRDP jusqu’à la version 2.0.0, une lecture hors limites dans
ntlm_read_ntlm_v2_client_challenge lisait jusqu’à 28 octets en dehors de la
structure interne (correction faite dans la version 2.1.0).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11087">CVE-2020-11087</a>

<p>Dans FreeRDP jusqu’à la version 2.0.0, une lecture hors limites dans
ntlm_read_AuthenticateMessage existait (correction faite dans la version 2.1.0).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11088">CVE-2020-11088</a>

<p>Dans FreeRDP jusqu’à la version 2.0.0, une lecture hors limites dans
ntlm_read_NegotiateMessage existait (correction faite dans la version 2.1.0).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11089">CVE-2020-11089</a>

<p>Dans FreeRDP jusqu’à la version 2.0.0, une lecture hors limites dans les
fonctions irp (parallel_process_irp_create, serial_process_irp_create,
drive_process_irp_write, printer_process_irp_write, rdpei_recv_pdu,
serial_process_irp_write) existait (correction faite dans la version 2.1.0).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11095">CVE-2020-11095</a>

<p>Dans FreeRDP avant la version 2.1.2, une lecture hors limites se produisait,
aboutissant à un accès d’emplacement de mémoire extérieur au tableau statique
PRIMARY_DRAWING_ORDER_FIELD_BYTES (correction faite dans la version 2.1.2).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11096">CVE-2020-11096</a>

<p>Dans FreeRDP avant la version 2.1.2, une lecture globale hors limites
dans update_read_cache_bitmap_v3_order existait. Comme contournement, il était
possible de désactiver le cache de bitmap avec -bitmap-cache (par défaut)
(correction faite dans la version 2.1.2).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11097">CVE-2020-11097</a>

<p>Dans FreeRDP avant la version 2.1.2, une lecture hors limites se produisait,
aboutissant à un accèss d’emplacement de mémoire extérieur au tableau statique
PRIMARY_DRAWING_ORDER_FIELD_BYTES (correction faite dans la version 2.1.2).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11098">CVE-2020-11098</a>

<p>Dans FreeRDP avant la version 2.1.2, une lecture hors limites dans
glyph_cache_put existait. Cela affectait tous les clients FreeRDP avec l’option
<q>+glyph-cache</q> activée (correction faite dans la version 2.1.2).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11099">CVE-2020-11099</a>

<p>Dans FreeRDP avant la version 2.1.2, une lecture hors limites dans
license_read_new_or_upgrade_license_packet existait. Un paquet de licence
trafiqué pouvait conduire à une lecture hors limites d’un tampon interne
(correction faite dans la version 2.1.2).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13396">CVE-2020-13396</a>

<p>Dans FreeRDP avant la version 2.1.2, une lecture hors limites dans
license_read_new_or_upgrade_license_packet existait. Un paquet de licence
trafiqué pouvait conduire à une lecture hors limites d’un tampon interne
(correction faite dans la version 2.1.2).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13397">CVE-2020-13397</a>

<p>Un problème a été découvert dans FreeRDP avant la version 2.1.1. Une
vulnérabilité de lecture hors limites a été détectée dans security_fips_decrypt
dans libfreerdp/core/security.c due à une valeur non initialisée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13398">CVE-2020-13398</a>

<p>Un problème a été découvert dans FreeRDP avant la version 2.1.1. Une
vulnérabilité d’écriture hors limites a été détectée dans crypto_rsa_common dans
libfreerdp/crypto/crypto.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15103">CVE-2020-15103</a>

<p>Dans FreeRDP jusqu’à la version 2.0., un dépassement d'entier existait dû à
un nettoyage manquant d’entrée dans le canal rdpegfx. Tous les clients FreeRDP
étaient affectés. Les rectangles d’entrée du serveur n’étaient pas confrontés
aux coordonnées de surface locale et acceptées d’office. Un serveur malveillant
pouvait envoyer des données qui plantaient ultérieurement le client (arguments
de longueur non valables pour un <q>memcpy</q>). Cela a été corrigé dans la
version 2.2.0. Comme contournement, ne plus utiliser les arguments de ligne de
commande /gfx, /gfx-h264 et /network:auto</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-39350">CVE-2023-39350</a>

<p>Ce problème affectait seulement les clients. Un dépassement d'entier par le
 bas conduisant à un déni de service (par exemple, abandon dû à
<q>WINPR_ASSERT</q> avec les drapeaux de compilation par défaut). Quand une
longueur de bloc insuffisante était fournie et une validation correcte de
longueur n’était pas réalisée, un dépassement d'entier par le bas se produisait,
conduisant à une vulnérabilité de déni de service (DOS). Ce problème a été
corrigé dans les versions 2.11.0 et 3.0.0-beta3. Une mise à niveau est
conseillée. Il n’existe pas de contournement connu pour cette vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-39351">CVE-2023-39351</a>

<p>Des versions de FreeRDP étaient sujettes à un déréférencement de pointeur
NULL conduisant à un plantage dans la gestion de RemoteFX (rfx). Dans la
fonction <q>rfx_process_message_tileset</q>, le programme allouait des tuiles
en utilisant <q>rfx_allocate_tiles</q> pour le nombre numTiles. Si le processus
d’initialisation de tuiles ne se terminait pas pour diverses raisons, les tuiles
avaient un pointeur NULL qui pouvait être accédé dans un traitement ultérieur
et causer un plantage de programme. Ce problème a été corrigé dans les
versions 2.11.0 et 3.0.0-beta3. Une mise à niveau est conseillée. Il n’existe
pas de contournement connu pour cette vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-39352">CVE-2023-39352</a>

<p>Des versions de FreeRDP étaient sujettes à une validation incorrecte de
position conduisant à une écriture hors limites. Cela pouvait être provoqué
quand les valeurs <q>rect->left</q> et <q>rect->top</q> étaient exactement
égales à <q>surface->width</q> et <q>surface->height</q>. Par exemple,
`rect->left` == `surface->width` &amp; `rect->top` == `surface->height`. En
pratique cela provoquait un plantage. Ce problème a été corrigé dans les
versions 2.11.0 et 3.0.0-beta3. Une mise à niveau est conseillée. Il n’existe
pas de contournement connu pour cette vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-39353">CVE-2023-39353</a>

<p>Des versions de FreeRDP étaient sujettes à une validation incorrecte de
position conduisant à une écriture hors limites. Dans le fichier
<q>libfreerdp/codec/rfx.c</q>, il n’existait pas de validation de position dans
<q>tile->quantIdxY</q>, <q>tile->quantIdxCb</q> et <q>tile->quantIdxCr</q>. Une
entrée contrefaite pouvait conduire à un accès hors limites de lecture qui
provoquait un plantage. Ce problème a été corrigé dans les
versions 2.11.0 et 3.0.0-beta3. Une mise à niveau est conseillée. Il n’existe
pas de contournement connu pour cette vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-39354">CVE-2023-39354</a>

<p>Des versions de FreeRDP étaient sujettes à une lecture hors limites dans la
fonction <q>nsc_rle_decompress_data</q>, due à un traitement de
<q>context->Planes</q> sans vérifier si les données étaient de longueur
suffisante. Cela pouvait être utilisé pour provoquer un plantage. Ce problème a
été corrigé dans les versions 2.11.0 et 3.0.0-beta3. Une mise à niveau est
conseillée. Il n’existe pas de contournement connu pour cette vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-39355">CVE-2023-39355</a>

<p>Des versions de FreeRDP de la branche 3.x avant la version beta3 étaient
sujettes à une utilisation de mémoire après libération lors du traitement de
paquets <q>RDPGFX_CMDID_RESETGRAPHICS</q>. Si <q>context->maxPlaneSize</q>
était égal à 0, <q>context->planesBuffer</q> était libéré. Cependant, sans mise à jour
de <q>context->planesBuffer</q>, cela conduisait à un vecteur d’exploitation
d’utilisation de mémoire après libération. Dans la plupart des environnements,
cela conduisait seulement à un plantage. Ce problème a été corrigé dans les
versions 2.11.0 et 3.0.0-beta3. Une mise à niveau est conseillée. Il n’existe
pas de contournement connu pour cette vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-39356">CVE-2023-39356</a>

<p>Des versions de FreeRDP étaient sujettes à une validation manquante de
position pouvant conduire à une lecture hors limites dans la fonction
<q>gdi_multi_opaque_rect</q>. En particulier, il n’existait pas de code pour
valider que la valeur <q>multi_opaque_rect->numRectangles</q> était inférieure
à 45. Boucler jusqu’à <q>multi_opaque_rect->`numRectangles</q> sans
vérifications appropriées de limites pouvait conduire à des lectures hors
limites, sources probables de plantage. Ce problème a été corrigé dans les
versions 2.11.0 et 3.0.0-beta3. Une mise à niveau est conseillée. Il n’existe
pas de contournement connu pour cette vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-40567">CVE-2023-40567</a>

<p>Des versions de FreeRDP étaient sujettes à une écriture hors limites dans la
fonction <q>clear_decompress_bands_data</q> dans lesquelles aucune vérification
de position n’existait. Ce problème a été corrigé dans les versions 2.11.0
et 3.0.0-beta3. Une mise à niveau est conseillée. Il n’existe pas de
contournement connu pour cette vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-40181">CVE-2023-40181</a>

<p>Des versions de FreeRDP étaient sujettes à un dépassement d'entier par le bas
conduisant à une lecture hors limites dans la fonction
<q>zgfx_decompress_segment</q>. Dans le contexte de <q>CopyMemory</q>, il était
possible de lire des données au-delà de la plage de paquets transmis et de
provoquer un plantage. Ce problème a été corrigé dans les
versions 2.11.0 et 3.0.0-beta3. Une mise à niveau est conseillée. Il n’existe
pas de contournement connu pour cette vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-40186">CVE-2023-40186</a>

<p>Des versions de FreeRDP étaient sujettes à un dépassement d’entier, conduisant à
une vulnérabilité d’écriture hors limites dans la fonction
<q>gdi_CreateSurface</q>. Ce problème affectait seulement les clients basés sur
FreeRDP. Les mandataires FreeRDP n’étaient pas affectés parce qu’ils ne
réalisent pas de décodage d’image. Ce problème a été corrigé dans les
versions 2.11.0 et 3.0.0-beta3. Une mise à niveau est conseillée. Il n’existe
pas de contournement connu pour cette vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-40188">CVE-2023-40188</a>

<p>Des versions de FreeRDP étaient sujettes à une lecture hors limites dans la
fonction <q>general_LumaToYUV444</q>. Cette lecture hors limites se produisait
parce qu’un traitement de la variable <q>in</q> était réalisé sans vérifier la
longueur suffisante des données. Une insuffisance pouvait provoquer des erreurs
ou des plantages. Ce problème a été corrigé dans les
versions 2.11.0 et 3.0.0-beta3. Une mise à niveau est conseillée. Il n’existe
pas de contournement connu pour cette vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-40569">CVE-2023-40569</a>

<p>Des versions de FreeRDP étaient sujettes à une écriture hors limites dans la
fonction <q>progressive_decompress</q>. Ce problème était probablement dû à un
mauvais calcul des variables <q>nXSrc</q> et <q>nYSrc</q>. Ce problème a été
corrigé dans les versions 2.11.0 et 3.0.0-beta3. Une mise à niveau est
conseillée. Il n’existe pas de contournement connu pour cette vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-40589">CVE-2023-40589</a>

<p>Des versions de FreeRDP étaient sujettes à un dépassement global de tampon dans
la fonction <q>ncrush_decompress</q>. Une fourniture d’entrée contrefaite dans
la fonction pouvait déclencher ce dépassement connu pour ne provoquer qu’un
plantage. Ce problème a été corrigé dans les versions 2.11.0 et 3.0.0-beta3. Une
mise à niveau est conseillée. Il n’existe pas de contournement connu pour cette
vulnérabilité.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.3.0+dfsg1-2+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets freerdp2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de freerdp2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/freerdp2">\
https://security-tracker.debian.org/tracker/freerdp2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3606.data"
# $Id: $
