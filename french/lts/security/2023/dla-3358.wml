#use wml::debian::translation-check translation="6836ca7806a5198b438d38ec716d1f0758158f66" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Un problème a été découvert dans mpv, un lecteur de vidéos basé sur
MPlayer/mplayer2. À cause d’une utilisation de mémoire après libération, un
attaquant pouvait exécuter du code arbitraire ou planter le programme à l’aide
du paramètre ao_c.</p>


<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 0.29.1-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mpv.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de mpv,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/mpv">\
https://security-tracker.debian.org/tracker/mpv</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3358.data"
# $Id: $
