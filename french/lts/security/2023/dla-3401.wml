#use wml::debian::translation-check translation="0a4924c3ed782f3c8b2221721240e1f7782b341e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans apache2, un serveur web
qui peut être utilisé comme mandataire de frontal pour d’autres applications.
Ces vulnérabilités pouvaient conduire à une dissimulation de requête HTTP, et
par conséquent des contrôles de sécurité du frontal pouvaient être contournés.</p>

<p>Malheureusement, la correction de ces vulnérabilités de sécurité peuvent
nécessiter des modifications de fichier de configuration. Quelques directives
RewriteRule hors spécifications, qui étaient précédemment
silencieusement acceptées, sont désormais rejetées avec l’erreur AH10409. Par
exemple, certaines RewriteRules qui incluent une référence arrière et les
attributs <q>[L,NC]</q>, nécessitent d’être écrites avec des attributs
d’échappement supplémentaires tels que <q>[B= ?,BNP,QSA]</q>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-25690">CVE-2023-25690</a>

<p>Certaines configurations de mod_proxy permettaient des attaques par
dissimulation de requête HTTP. Des configurations étaient affectées quand
mod_proxy était activé avec certaines formes de RewriteRule ou ProxyPassMatch
dans lesquelles un motif non spécifique correspondait à une partie de données
de cible de requête (URL) fournie par l’utilisateur et était alors réinsérée
dans la cible de requête mise dans le mandataire en utilisant une substitution
de variable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-27522">CVE-2023-27522</a>

<p>Dissimulation de requête de réponse HTTP dans mod_proxy_uwsgi</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.4.38-3+deb10u10.</p>

<p>Nous vous recommandons de mettre à jour vos paquets apache2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de apache2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/apache2">\
https://security-tracker.debian.org/tracker/apache2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3401.data"
# $Id: $
