#use wml::debian::translation-check translation="dd6325bdfd61166ccb08efb355660272d22d7a00" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Florent Saudel et Arnaud Gatignol ont découvert une vulnérabilité de
confusion de type dans les fonctions RPC Spotlight dans afpd dans Netatalk.
Lors de l’analyse de paquets RPC Spotlight, une structure de données encodées
est un dictionnaire de style clé-valeur où les clés sont des chaines de
caractères et où les valeurs peuvent être n’importe quels types pris en charge
dans le protocole sous-jacent. À cause de l’absence de vérification de type dans
les appelants de la fonction dalloc_value_for_key(), qui renvoie l’objet
associé à une clé, un intervenant malveillant pouvait contrôler complètement
la valeur du pointeur et théoriquement réaliser une exécution de code à distance
dans l’hôte.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 3.1.12~ds-3+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets netatalk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de netatalk,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/netatalk">\
https://security-tracker.debian.org/tracker/netatalk</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3584.data"
# $Id: $
