#use wml::debian::translation-check translation="600683bdb7f906d6d3c36ef6f665a2956f6421f1" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans imagemagick qui pouvaient
conduire à une élévation des privilèges, un déni de service ou une fuite
d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-19667">CVE-2020-19667</a>

<p>Un dépassement de pile et un saut inconditionnel ont été découverts dans
ReadXPMImage dans coders/xpm.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25665">CVE-2020-25665</a>

<p>Une lecture hors limites dans le codeur d’image PALM a été découverte dans
WritePALMImage dans coders/palm.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25666">CVE-2020-25666</a>

<p>Un dépassement d'entier était possible lors de calculs mathématiques simples
dans HistogramCompare() dans MagickCore/histogram.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25674">CVE-2020-25674</a>

<p>Une boucle for avec une condition de sortie impropre a été découverte, qui
pouvait permettre une lecture hors limites à l’aide d’un dépassement de tampon de
tas dans WriteOnePNGImage de coders/png.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25675">CVE-2020-25675</a>

<p>Un comportement indéfini a été découvert sous la forme d’un dépassement
d'entier et de valeurs hors limites pour cause de calculs arrondis réalisés sur
des décalages de pixels libres dans les routines CropImage() et
 CropImageToTiles() de MagickCore/transform.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25676">CVE-2020-25676</a>

<p>Un comportement indéfini a été découvert sous la forme d’un dépassement
d'entier et de valeurs hors limites pour cause de calculs arrondis réalisés sur
des décalages de pixels libres dans CatromWeights(), MeshInterpolate(),
InterpolatePixelChannel(), InterpolatePixelChannels() et InterpolatePixelInfo(),
toutes des fonctions de /MagickCore/pixel.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27560">CVE-2020-27560</a>

<p>Une division par zéro a été découverte dans OptimizeLayerFrames dans
MagickCore/layer.c, qui pouvait provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27750">CVE-2020-27750</a>

<p>Une division par zéro a été découverte dans MagickCore/colorspace-private.h
and MagickCore/quantum.h, qui pouvait provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27751">CVE-2020-27751</a>

<p>Un comportement indéfini a été découvert sous la forme de valeurs en dehors
de l’intervalle de type <q>unsigned long long</q> ainsi qu’un exposant décalé
qui est trop grand pour le type pour 64 bits dans MagickCore/quantum-export.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27754">CVE-2020-27754</a>

<p>Un dépassement d'entier a été découvert dans IntensityCompare() de
/magick/quantize.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27756">CVE-2020-27756</a>

<p>Une division par zéro a été découverte dans ParseMetaGeometry() de
MagickCore/geometry.c. Des calculs de hauteur et de largeur d’image pouvaient
conduire à une condition de division par zéro, qui pouvaient aussi conduire à
un comportement indéfini.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27757">CVE-2020-27757</a>

<p>Un comportement indéfini a été découvert dans MagickCore/quantum-private.h.
Un calcul mathématique avec virgule flottante dans ScaleAnyToQuantum() de
/MagickCore/quantum-private.h pouvait conduire à un comportement indéfini sous
la forme d’une valeur en dehors de l’intervalle de type <q>unsigned long long</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27758">CVE-2020-27758</a>

<p>Un comportement indéfini a été découvert sous la forme de valeurs en dehors
de l’intervalle de type <q>unsigned long long</q> dans coders/txt.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27759">CVE-2020-27759</a>

<p>Dans IntensityCompare() de /MagickCore/quantize.c, une valeur double était
changée en int et renvoyée, ce qui dans certains cas faisait qu’une valeur
en dehors de l’intervalle de type int était renvoyée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27760">CVE-2020-27760</a>

<p>Dans <q>GammaImage()</q> de /MagickCore/enhance.c, selon la valeur de
<q>gamma</q>, il était possible de déclencher une condition de division par zéro
quand un fichier d’entrée contrefait était traité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27761">CVE-2020-27761</a>

<p>WritePALMImage() dans /coders/palm.c utilisait des modifications de type
size_t dans plusieurs zones de calcul, ce qui pouvait conduire à des valeurs en
dehors du type représentable <q>unsigned long</q> à avoir un comportement
indéfini quand un fichier d’entrée contrefait était traité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27762">CVE-2020-27762</a>

<p>Un comportement indéfini a été découvert sous la forme de valeur en dehors
de l’intervalle de type <q>unsigned char</q> dans coders/hdr.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27763">CVE-2020-27763</a>

<p>Un comportement indéfini a été découvert sous la forme d’une division
mathématique par zéro dans MagickCore/resize.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27764">CVE-2020-27764</a>

<p>Des valeurs hors intervalle ont été découvertes dans certaines circonstances
quand un fichier d’entrée contrefait était traité dans /MagickCore/statistic.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27765">CVE-2020-27765</a>

<p>Un comportement indéfini a été découvert sous la forme d’une division
mathématique par zéro dans MagickCore/segment.c quand un fichier d’entrée
contrefait était traité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27766">CVE-2020-27766</a>

<p>Un fichier contrefait qui était traité par ImageMagick pouvait déclencher
un comportement indéfini sous la forme de valeurs en dehors de l’intervalle de
type <q>unsigned long</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27767">CVE-2020-27767</a>

<p>Un comportement indéfini a été découvert sous la forme de valeurs en dehors
de l’intervalle de types <q>float</q> et <q>unsigned char</q> dans 
MagickCore/quantum.h.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27768">CVE-2020-27768</a>

<p>Une valeur en dehors l’intervalle des valeurs représentables de type
<q>unsigned int</q> a été découverte dans MagickCore/quantum-private.h</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27769">CVE-2020-27769</a>

<p>Une valeur en dehors l’intervalle des valeurs représentables de type
<q>float</q> a été découverte dans MagickCore/quantum-private.h</p></li>


<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27770">CVE-2020-27770</a>

<p>À cause d’une vérification manquante pour une valeur 0 de <q>replace_extent</q>,
il était possible pour le décalage <q>p</q> de déborder dans SubstituteString()</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27771">CVE-2020-27771</a>

<p>Dans RestoreMSCWarning() de /coders/pdf.c, il existait plusieurs zones où les
appels à GetPixelIndex() pouvaient aboutir à des valeurs en dehors de
l’intervalle représentable pour le type <q>unsigned char</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27772">CVE-2020-27772</a>

<p>Un comportement indéfini a été découvert sous la forme de valeurs en dehors
de l’intervalle de type <q>unsigned int</q> dans coders/bmp.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27773">CVE-2020-27773</a>

<p>Un comportement indéfini a été découvert sous la forme de valeurs en dehors
de l’intervalle de type <q>unsigned char</q> ou d’une division par zéro</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27774">CVE-2020-27774</a>

<p>Un fichier contrefait traité par ImageMagick pouvait déclencher un
comportement indéfini sous la forme d’un décalage trop grand pour le type 64 bits
<q>ssize_t</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27775">CVE-2020-27775</a>

<p>Un comportement indéfini a été découvert sous la forme de valeurs en dehors
de l’intervalle de type <q>unsigned char</q> dans MagickCore/quantum.h.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27776">CVE-2020-27776</a>

<p>Un fichier contrefait traité par ImageMagick pouvait déclencher un
comportement indéfini sous la forme de valeurs en dehors de l’intervalle de
type <q>unsigned long</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29599">CVE-2020-29599</a>

<p>ImageMagick gérait incorrectement l’option -authenticate, qui permettait le
réglage du mot de passe pour les fichiers PDF protégés par un mot de passe.
Le mot de passe contrôlé par l’utilisateur n’était pas correctement
protégé ou nettoyé, et il était possible d’injecter des commandes d’interpréteur
supplémentaires à l’aide de coders/pdf.c. « %s »ur les systèmes Debian, par défaut,
la politique d’imagemagick atténue la portée de ce CVE.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3574">CVE-2021-3574</a>

<p>Une fuite de mémoire a été découverte lors de la conversion d’un fichier TIFF
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3596">CVE-2021-3596</a>

<p>Un déréférencement de pointeur NULL a été découvert dans ReadSVGImage() dans
coders/svg.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20224">CVE-2021-20224</a>

<p>Un problème de dépassement d'entier a été découvert dans la fonction
ExportIndexQuantum() d’ImageMagick dans MagickCore/quantum-export.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-44267">CVE-2022-44267</a>

<p>Un déni de service a été découvert. Lors de l’analyse d’une image PNG,
le processus <q>convert</q> pouvait demeurer en attente d’une entrée stdin.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-44268">CVE-2022-44268</a>

<p>Une divulgation d'informations a été découverte. Lors de l’analyse d’une
image PNG (par exemple, pour un redimensionnement), l’image résultante pouvait
incorporée le contenu d’un fichier arbitraire.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 8:6.9.10.23+dfsg-2.1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets imagemagick.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de imagemagick,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/imagemagick">\
https://security-tracker.debian.org/tracker/imagemagick</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3357.data"
# $Id: $
