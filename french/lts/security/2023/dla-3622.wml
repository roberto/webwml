#use wml::debian::translation-check translation="de65802fd8e2d6c8f8d3e5b8a04d9f1acfe0061f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Letian Yuan a découvert un défaut dans Apache Axis 1.x, une implémentation de
SOAP écrite en Java. Il pouvait ne pas être évident que rechercher un service
à l’aide de <q>ServiceFactory.getService</q> permettait éventuellement des
mécanismes de recherche dangereux tels que LDAP. Lors du passage d’une entrée
non vérifiée à cette méthode d’API, cela pouvait exposer l’application à des
dénis de service, des contrefaçons de requête côté serveur et même à
des attaques conduisant à une exécution de code à distance.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.4-28+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets axis.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de axis,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/axis">\
https://security-tracker.debian.org/tracker/axis</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3622.data"
# $Id: $
