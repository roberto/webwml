#use wml::debian::translation-check translation="3a68be61840b52c58208213d3f1e50c1c0543ade" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité d’injection SQL a été découverte dans PostgreSQL, un
système de gestion de base de données relationnelle et objet.</p>

<p>Un script d’extension était vulnérable s’il utilisait @extowner@,
@extschema@ ou @extschema:...@ à l’intérieur d’une construction de citation
(guillemet dollar, '', ou "").</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 11.21-0+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets postgresql-11.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de postgresql-11,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/postgresql-11">\
https://security-tracker.debian.org/tracker/postgresql-11</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3600.data"
# $Id: $
