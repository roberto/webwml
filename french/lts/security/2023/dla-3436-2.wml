#use wml::debian::translation-check translation="c1300955627c208f0f75955a4e875a3bdf3f9ef2" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de régression pour LTS</define-tag>
<define-tag moreinfo>
<p>sssd 1.16.3-3.2+deb10u1 (<a href="./dla-3436">DLA 3436-1</a>) a cassé le
chemin de mise à niveau à partir de la version 1.16.3-3.2.</p>

<p>Il était possible de mettre à niveau sssd-common vers la
version 1.16.3-3.2+deb10u1 tout en laissant libsss-certmap0 à la
version 1.16.3-3.2. La mauvaise correspondance casse SSSD car le correctif
pour le <a href="https://security-tracker.debian.org/tracker/CVE-2022-4254">CVE-2022-4254</a>
introduisait de nouveaux symboles qui étaient utilisés dans
<code>sssd_pam</code> de sssd-common.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.16.3-3.2+deb10u2. Cette version diffère de la
version 1.16.3-3.2+deb10u1 seulement dans les métadonnées de paquet.
(Passer à la version minimale pour libsss-certmap0 du champ Depends: de
sssd-common assure un chemin sûr de mise à niveau.)</p>

<p>Nous vous recommandons de mettre à jour vos paquets sssd.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de sssd,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/sssd">\
https://security-tracker.debian.org/tracker/sssd</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3436-2.data"
# $Id: $
