#use wml::debian::translation-check translation="1daa37a8f3f8c2016103c3c3f6df016b199e7b0b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Moment.js est une bibliothèque JavaScript de date pour analyser, valider,
manipuler et formater les dates. Un couple de vulnérabilités ont été signalées.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24785">CVE-2022-24785</a>

<p>Une vulnérabilité de traversée de répertoires impactait les utilisateurs de
npm (serveur) de Moment.js, particulièrement si une chaine de locale fournie
par l’utilisateur était directement utilisée pour changer la locale de moment.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-31129">CVE-2022-31129</a>

<p>Des versions affectées de moment ont été trouvées qui utilisaient un algorithme
inefficace d’analyse. Plus particulièrement, l’utilisation d’analyse
 chaine-vers-date dans moment (plus particulièrement l’analyse rfc2822, qui
était essayée par défaut) avait une complexité quadratique (N^2) pour des
entrées particulières. Les utilisateurs pouvaient remarquer un ralentissement
notable avec des entrées de plus de 10k caractères. Les utilisateurs qui
passaient des chaines sans vérification de longueur au constructeur de moment
étaient vulnérables à des attaques par (Re)DoS.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.24.0+ds-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets node-moment.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de node-moment,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/node-moment">\
https://security-tracker.debian.org/tracker/node-moment</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3295.data"
# $Id: $
