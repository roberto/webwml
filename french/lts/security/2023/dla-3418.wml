#use wml::debian::translation-check translation="da97c62a4740e4df6454f14349218e92319d20a0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>NVIDIA a publié une mise à jour de sécurité pour la branche de pilotes Linux
<q>NVIDIA GPU Display Driver R390</q>. Cette mise à jour corrige des problèmes
qui pouvaient conduire à un déni de service, une augmentation des privilèges,
une divulgation d'informations, une manipulation de données ou un comportement
indéfini.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-34670">CVE-2022-34670</a>

<p><q>NVIDIA GPU Display Driver</q> pour Linux contenait une vulnérabilité
dans le gestionnaire de la couche de mode du noyau, où un utilisateur quelconque
pouvait provoquer des erreurs de coupure lors de la conversion d’une primitive
en une plus petite provoquant des pertes de données, ce qui pouvait conduire à
un déni de service ou une divulgation d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-34674">CVE-2022-34674</a>

<p><q>NVIDIA GPU Display Driver</q> pour Linux contenait une vulnérabilité
dans le gestionnaire de la couche de mode du noyau, où un assistant de fonction
mappait plus de pages physiques que nécessaires, ce qui pouvait conduire à un
comportement indéfini ou à une fuite d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-34675">CVE-2022-34675</a>

<p><q>NVIDIA GPU Display Driver</q> pour Linux contenait une vulnérabilité
dans le gestionnaire de la couche de mode du noyau, où il ne vérifiait pas la
valeur renvoyée d’un déréférencement de pointeur NULL, ce qui pouvait conduire
à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-34677">CVE-2022-34677</a>

<p><q>NVIDIA GPU Display Driver</q> pour Linux contenait une vulnérabilité
dans le gestionnaire de la couche de mode du noyau, où un utilisateur
quelconque pouvait provoquer une troncature d’entier, ce qui pouvait conduire
à un déni de service ou à une manipulation de données.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-34680">CVE-2022-34680</a>

<p><q>NVIDIA GPU Display Driver</q> pour Linux contenait une vulnérabilité
dans le gestionnaire de la couche de mode du noyau, où une troncature d’entier
pouvait conduire à une lecture hors limites, ce qui pouvait conduire à un déni
de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42257">CVE-2022-42257</a>

<p><q>NVIDIA GPU Display Driver</q> pour Linux contenait une vulnérabilité
dans la couche de mode du noyau (nvidia.ko), où un dépassement d'entier pouvait
conduire à une divulgation d'informations, une manipulation de données ou un
déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42258">CVE-2022-42258</a>

<p><q>NVIDIA GPU Display Driver</q> pour Linux contenait une vulnérabilité
dans la couche de mode du noyau (nvidia.ko), où un dépassement d'entier pouvait
conduire à une divulgation d'informations, une manipulation de données ou un
déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42259">CVE-2022-42259</a>

<p><q>NVIDIA GPU Display Driver</q> pour Linux contenait une vulnérabilité
dans la couche de mode du noyau (nvidia.ko), où un dépassement d'entier pouvait
conduire à un déni de service.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 390.157-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets nvidia-graphics-drivers-legacy-390xx.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de nvidia-graphics-drivers-legacy-390xx,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/nvidia-graphics-drivers-legacy-390xx">\
https://security-tracker.debian.org/tracker/nvidia-graphics-drivers-legacy-390xx</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3418.data"
# $Id: $
