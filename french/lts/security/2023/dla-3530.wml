#use wml::debian::translation-check translation="f7e0a6954177b5523cfd0472fc7cfcc0c717dddb" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été découvertes dans OpenSSL, une boîte à outils SSL
(Secure Socket Layer).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3446">CVE-2023-3446</a>

<p>, <a href="https://security-tracker.debian.org/tracker/CVE-2023-3817">CVE-2023-3817</a>.</p>

<p>Des clés DH excessivement longues ou des vérifications de paramètre pouvaient
provoquer des délais importants dans les applications utilisant DH_check(),
DH_check_ex() ou des fonctions EVP_PKEY_param_check(), conduisant éventuellement
à des attaques par déni de service quand les clés ou les paramètres étaient
obtenus de sources non fiables.</p>


<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.1.1n-0+deb10u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openssl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openssl,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openssl">\
https://security-tracker.debian.org/tracker/openssl</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3530.data"
# $Id: $
