#use wml::debian::translation-check translation="eda2c69cecd4ba6e3d37c74e436ed40931995e95" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité a été découverte dans l’environnement d’exécution Java
OpenJDK, qui pouvait aboutir à un déni de service.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 11.0.21+9-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openjdk-11.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openjdk-11,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openjdk-11">\
https://security-tracker.debian.org/tracker/openjdk-11</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3636.data"
# $Id: $
