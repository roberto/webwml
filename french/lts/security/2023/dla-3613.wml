#use wml::debian::translation-check translation="e0429e6b6f63a92bdef8d201ce8bbacade3c417d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux problèmes de sécurité ont été découverts dans Curl, un outil en ligne de
commande et une bibliothèque côté client de transfert par URL d'utilisation
facile.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28321">CVE-2023-28321</a>

<p>Hiroki Kurosawa a trouvé que curl pouvait ne pas faire correspondre des noms
d’hôte avec des jokers en utilisant sa propre fonction de correspondance de
noms.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38546">CVE-2023-38546</a>

<p>Il a été découvert que dans certaines circonstances libcurl était vulnérable
à une injection de cookie.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 7.64.0-4+deb10u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets curl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de curl,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/curl">\
https://security-tracker.debian.org/tracker/curl</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3613.data"
# $Id: $
