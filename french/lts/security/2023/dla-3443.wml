#use wml::debian::translation-check translation="ab74025f93fe40f3bcbc149b2dd8e48224c4a11c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été corrigées dans l’analyseur de trafic réseau
Wireshark.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2856">CVE-2023-2856</a>

<p>Plantage de l’analyseur de fichier TCPIPtrace VMS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2858">CVE-2023-2858</a>
 <p>Plantage de l’analyseur de fichier NetScaler.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2879">CVE-2023-2879</a>

<p>Boucle infinie GDSDB.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2952">CVE-2023-2952</a>

<p>Boucle infinie du dissecteur XRA.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.6.20-0+deb10u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wireshark.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de wireshark,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/wireshark">\
https://security-tracker.debian.org/tracker/wireshark</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3443.data"
# $Id: $
