#use wml::debian::translation-check translation="ece8c2dd8256165f3951ba2871890e3d9537e6c4" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Dernière version 10.3.39 d’entretien mineur de MariaDB incluant un correctif
pour la vulnérabilité de sécurité suivante :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-47015">CVE-2022-47015</a>
<p>moteur de stockage Spider vulnérable à un déni de service.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1:10.3.39-0+deb10u1.</p>

<p>De plus, la modification de l’IPA libmariadb non rétrocompatible a été
annulée (clôture du bogue n° 1031773).</p>

<p>Nous vous recommandons de mettre à jour vos paquets mariadb-10.3.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de mariadb-10.3,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/mariadb-10.3">\
https://security-tracker.debian.org/tracker/mariadb-10.3</a>.</p>

<p>Note! According to <a href="https://mariadb.org/about/#maintenance-policy">https://mariadb.org/about/#maintenance-policy</a> cette was the last minor maintenance release pour MariaDB 10.3 series.</p>


<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3444.data"
# $Id: $
