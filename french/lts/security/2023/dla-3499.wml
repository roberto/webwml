#use wml::debian::translation-check translation="4a9df1743fd26a446411e8234b4b48f857a2e559" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Des vulnérabilités de redirection ouverte ont été trouvées dans
libapache2-mod-auth-openidc, une implémentation OpenID Connect Relying Party
pour Apache, qui pouvaient conduire à une divulgation d'informations à l’aide
d’attaques par hameçonnage.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39191">CVE-2021-39191</a>

<p>La fonctionnalité SSO init de tierce partie de <code>mod_auth_openidc</code>
a été signalée comme vulnérable à une attaque de redirection ouverte en fournissant
un URL contrefait dans le paramètre <code>target_link_uri</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23527">CVE-2022-23527</a>

<p>Lors de la fourniture d’un paramètre de déconnexion à l’URI de redirection,
<code>mod_auth_openidc</code> échouait à vérifier correctement les URL
commençant avec <q><code>/&bsol;t</code></q>, conduisant à une redirection
ouverte.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.3.10.2-1+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libapache2-mod-auth-openidc.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libapache2-mod-auth-openidc,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libapache2-mod-auth-openidc">\
https://security-tracker.debian.org/tracker/libapache2-mod-auth-openidc</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3499.data"
# $Id: $
