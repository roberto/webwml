#use wml::debian::translation-check translation="7aae399732656ed5232b9762b78c943d967ba012" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>libxpm est une bibliothèque de traitement du format d’image X PixMap
(fichiers xpm). Les fichiers xpm sont une extension du format monochrome
X BitMap spécifié dans le protocole X et sont couramment utilisés dans les
applications X traditionnelles.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4883">CVE-2022-4883</a>

<p>Lors du traitement de fichiers avec extension .Z ou .gz, la bibliothèque
appelle des programmes externes pour la compression et la décompression de
fichier, s’appuyant sur la variable d’environnement PATH pour trouver ces
programmes. Cela pouvait permettre à un utilisateur malveillant d’exécuter
d’autres programmes en manipulant la variable d’environnement PATH.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-44617">CVE-2022-44617</a>

<p>Lors du traitement d’un fichier avec une largeur de zéro et une très grande
hauteur, quelques fonctions d’analyse sont appelées de manière répétée et
pouvaient conduire à une boucle infinie, aboutissant à un déni de service dans
les applications liées à la bibliothèque.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-46285">CVE-2022-46285</a>

<p>Lors de l’analyse d’un fichier avec un commentaire non clos, une condition
de fin de fichier n’était pas détectée, conduisant à une boucle infinie et
aboutissant à un déni de service dans les applications liées à la bibliothèque.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1:3.5.12-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libxpm.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libxpm,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libxpm">\
https://security-tracker.debian.org/tracker/libxpm</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3459.data"
# $Id: $
