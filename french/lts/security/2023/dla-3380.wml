#use wml::debian::translation-check translation="4c3f0cacbcaf0bf5fbf4a7fd6473c1aa599747b1" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Les paquets firmware-nonfree ont été mis à jour pour inclure des
microprogrammes pouvant être nécessaires pour certains pilotes dans Linux 5.10,
disponibles pour Debian LTS sous forme de noyau rétroporté.</p>

<p>Certains des fichiers mis à jour corrigent des vulnérabilités de sécurité
qui pouvaient permettre une élévation des privilèges, un déni de service ou une
divulgation d’informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24586">CVE-2020-24586</a>

<p>(INTEL-SA-00473)</p>

<p>La norme 802.11 qui étaye <q>Wi-Fi Protected Access</q> (WPA, WPA2 et WPA3)
et <q>Wired Equivalent Privacy</q> (WEP) ne requiert pas que les fragments reçus
soient nettoyés de la mémoire après la (re)connexion à un réseau. Dans certaines
circonstances, quand un autre appareil envoyait des trames fragmentées chiffrées
en utilisant WEP, CCMP ou GCMP, cela pouvait être détourné pour injecter des
paquets arbitraires dans le réseau ou exfiltrer des données d’utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24587">CVE-2020-24587</a>

 <p>(INTEL-SA-00473)</p>

<p>La norme 802.11 qui étaye <q>Wi-Fi Protected Access</q> (WPA, WPA2 et WPA3)
et <q>Wired Equivalent Privacy</q> (WEP) ne requiert pas que tous les fragments
d’une trame soient chiffrés avec la même clé. Un ennemi pouvait détourner cela
pour déchiffrer des fragments choisis quand un autre appareil envoyait des
trames fragmentées et la clé de chiffrement WEP, CCMP ou GCMP était
périodiquement renouvelée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24588">CVE-2020-24588</a>

 <p>(INTEL-SA-00473)</p>

<p>La norme 802.11 qui étaye <q>Wi-Fi Protected Access</q> (WPA, WPA2 et WPA3)
et <q>Wired Equivalent Privacy</q> (WEP) ne requiert pas que le drapeau A-MSDU
dans le champ d’en-tête QoS en texte simple soit authentifié. Dans des appareils
ne gérant pas la réception des trames A-MSDU non SSP (ce qui est obligatoire car
faisant partie de 802.11n), un ennemi peut détourner cela pour injecter des
paquets réseau arbitraires.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23168">CVE-2021-23168</a>

 <p>(INTEL-SA-00621)</p>

<p>Une lecture hors limites pour quelques produits Wi-Fi PROSet/Wireless et
Killer™ pouvait permettre à un utilisateur non authentifié d’éventuellement
activer un déni de service à l'aide d'un accès adjacent.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23223">CVE-2021-23223</a>

<p>(INTEL-SA-00621)</p>

<p>Un initialisation incorrecte pour quelques produits Wi-Fi PROSet/Wireless et
Killer™ pouvait permettre à un utilisateur non authentifié d’éventuellement
activer un déni de service à l'aide d'un accès local.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37409">CVE-2021-37409</a>

<p>(INTEL-SA-00621)</p>

<p>Un contrôle incorrect d’accès pour quelques produits Wi-Fi PROSet/Wireless et
Killer™ pouvait permettre à un utilisateur non authentifié d’éventuellement
activer un déni de service à l'aide d'un accès local.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44545">CVE-2021-44545</a>

<p>(INTEL-SA-00621)</p>

<p>Une validation incorrecte d’entrée pour quelques produits Wi-Fi PROSet/Wireless
et Killer™ pouvait permettre à un utilisateur non authentifié d’éventuellement
activer un déni de service à l'aide d'un accès adjacent.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21181">CVE-2022-21181</a>

<p>(INTEL-SA-00621)</p>

<p>Une validation incorrecte d’entrée pour quelques produits Wi-Fi PROSet/Wireless
et Killer™ pouvait permettre à un utilisateur non authentifié d’éventuellement
activer un déni de service à l'aide d'un accès local.</p>

<p>Les annonces suivantes sont aussi corrigées par cette publication, mais
nécessitent de mettre à jour le noyau Linux pour charger les microprogrammes
mis à jour.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12362">CVE-2020-12362</a>

<p>(INTEL-SA-00438)</p>

<p>Un dépassement d'entier dans le microprogramme pour certains pilotes
graphiques d’Intel pour Windows *, avant la version 26.20.100.7212 et avant le
noyau Linux version 5.5, pouvait permettre à un utilisateur privilégié d’élever
ses privilèges à l’aide d’un accès local.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12363">CVE-2020-12363</a>

<p>(INTEL-SA-00438)</p>

<p>Une validation incorrecte d’entrée dans certains pilotes graphiques d’Intel
pour Windows *, avant la version 26.20.100.7212 et avant le noyau Linux
version 5.5, pouvait permettre à un utilisateur privilégié d’activer un déni de
service à l’aide d’un accès local.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12364">CVE-2020-12364</a>

<p>(INTEL-SA-00438)</p>

<p>Un déréférencement de pointeur NULL dans certains pilotes graphiques d’Intel
pour Windows *, avant la version 26.20.100.7212 et avant le noyau Linux
version 5.5, pouvait permettre à un utilisateur privilégié d’activer un déni de
service à l’aide d’un accès local.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 20190114+really20220913-0+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets firmware-nonfree.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de firmware-nonfree,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/firmware-nonfree">\
https://security-tracker.debian.org/tracker/firmware-nonfree</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3380.data"
# $Id: $
