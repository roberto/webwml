#use wml::debian::translation-check translation="dee770116ec413a1b2c1eabdff602c68ce861021" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>ruby-sidekiq, un traitement simple et efficace en arrière-plan pour Ruby,
avait deux vulnérabilités.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30151">CVE-2021-30151</a>

<p>Sidekiq permettait un script XSS à l’aide du nom de la file dans la fonction
live-poll quand Internet Explorer était utilisé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23837">CVE-2022-23837</a>

<p>Dans api.rb dans Sidekiq, il n’y avait pas de limite sur le nombre de jours
lors d’une requête de statistiques pour le graphe. Cela surchargeait le système
en affectant l’interface utilisateur web et la rendait non accessible aux
utilisateurs.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 5.2.3+dfsg-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-sidekiq.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ruby-sidekiq,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ruby-sidekiq">\
https://security-tracker.debian.org/tracker/ruby-sidekiq</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3360.data"
# $Id: $
