#use wml::debian::translation-check translation="95ba90be1b9d9452dceac8df83016626652bc259" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Intel® a publié l’annonce INTEL-SA-00766 concernant les vulnérabilités
potentielles de sécurité dans quelques produits Wi-Fi PROSet/Wireless et Killer™
qui pouvaient permettre une élévation de privilèges ou un déni de service.
L’annonce complète est disponible sur [1]</p>

<p>[1] <a href="https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00766.html">https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00766.html</a>.</p>

<p>Cette mise à jour du paquet firmware-nonfree inclut les fichiers de
micrologiciels suivants :
</br>– Intel Bluetooth série AX2xx :
</br>ibt-0041-0041.sfi
</br>ibt-19-0-0.sfi
</br>ibt-19-0-1.sfi
</br>ibt-19-0-4.sfi
</br>ibt-19-16-4.sfi
</br>ibt-19-240-1.sfi
</br>ibt-19-240-4.sfi
</br>ibt-19-32-0.sfi
</br>ibt-19-32-1.sfi
</br>ibt-19-32-4.sfi
</br>ibt-20-0-3.sfi
</br>ibt-20-1-3.sfi
</br>ibt-20-1-4.sfi
</br>– Intel Wireless séries 22000 :
</br>iwlwifi-Qu-b0-hr-b0-77.ucode
</br>iwlwifi-Qu-b0-jf-b0-77.ucode
</br>iwlwifi-Qu-c0-hr-b0-77.ucode
</br>iwlwifi-Qu-c0-jf-b0-77.ucode
</br>iwlwifi-QuZ-a0-hr-b0-77.ucode
</br>iwlwifi-cc-a0-77.ucode</p>

<p>Les fichiers des micrologiciels mis à jour peuvent nécessiter un noyau mis à jour
pour fonctionner. Il est recommandé de désérialiser si le noyau incorpore le
fichier de micrologiciels mis à jour et de prendre les mesures adéquates si
nécessaires.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-27635">CVE-2022-27635</a>

<p>Un contrôle d’accès incorrect pour quelques logiciels Wi-Fi PROSet/Wireless
d’Intel(R) WiFi et Killer(TM) pouvait permettre à un utilisateur privilégié
d’éventuellement permettre une élévation de privilèges à l’aide d’un accès
local.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-36351">CVE-2022-36351</a>

<p>Un contrôle d’accès incorrect pour quelques logiciels Wi-Fi PROSet/Wireless
d’Intel(R) WiFi et Killer(TM) pouvait permettre à un utilisateur non authentifié
d’éventuellement provoquer un déni de service à l'aide d'un accès adjacent.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-38076">CVE-2022-38076</a>

<p>Une validation d’entrée incorrecte pour quelques logiciels Wi-Fi
PROSet/Wireless d’Intel(R) WiFi et Killer(TM) pouvait permettre à un utilisateur
authentifié d’éventuellement permettre une élévation de privilèges à l’aide d’un
accès local.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-40964">CVE-2022-40964</a>

<p>Un contrôle d’accès incorrect pour quelques logiciels Wi-Fi PROSet/Wireless
d’Intel(R) WiFi et Killer(TM) pouvait permettre à un utilisateur privilégié
d’éventuellement permettre une élévation de privilèges à l’aide d’un accès
local.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-46329">CVE-2022-46329</a>

<p>Un défaut du mécanisme de protection pour quelques logiciels Wi-Fi
PROSet/Wireless d’Intel(R) WiFi et Killer(TM) pouvait permettre à un utilisateur
privilégié d’éventuellement permettre une élévation de privilèges à l’aide d’un
accès local.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 20190114+really20220913-0+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets firmware-nonfree.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de firmware-nonfree,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/firmware-nonfree">\
https://security-tracker.debian.org/tracker/firmware-nonfree</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3596.data"
# $Id: $
