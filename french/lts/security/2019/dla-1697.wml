#use wml::debian::translation-check translation="c4a1c2bdc93f45db86669b5a8ce1e0f17a23954d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux problèmes ont été découverts dans bind9, le serveur de noms de domaine
d’Internet.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6465">CVE-2019-6465</a>

<p>Transferts de zone pour les DLZ exécutés bien que non autorisés par les
ACL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5745">CVE-2018-5745</a>

<p>Empêchement d’assertion, et par conséquent terminaison délibérée de named
quand une clef de confiance d’ancre est remplacée par une clef utilisant un
algorithme non pris en charge.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes de sécurité ont été corrigés dans
la version 1:9.9.5.dfsg-9+deb8u17.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bind9.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1697.data"
# $Id: $
