#use wml::debian::translation-check translation="0489e1b952f47155cfd52286f4b52319011dbc06" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans php5, un langage de script
embarqué dans du HTML et côté serveur.</p>


<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9637">CVE-2019-9637</a>

<p>rename() à travers le périphérique peut permettre un accès non désiré lors
du traitement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9638">CVE-2019-9638</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-9639">CVE-2019-9639</a>

<p>Lecture non initialisée dans exif_process_IFD_in_MAKERNOTE.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9640">CVE-2019-9640</a>

<p>Lecture non valable dans exif_process_SOFn.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9641">CVE-2019-9641</a>

<p>Lecture non valable dans exif_process_IFD_in_TIFF.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9022">CVE-2019-9022</a>

<p>Un problème lors de l’analyse de réponses DNS permet à un serveur de DNS hostile
d’abuser de memcpy, ce qui conduit à une opération de lecture après un tampon
alloué.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 5.6.40+dfsg-0+deb8u2.</p>
<p>Nous vous recommandons de mettre à jour vos paquets php5.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1741.data"
# $Id: $
