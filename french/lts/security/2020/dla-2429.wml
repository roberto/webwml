#use wml::debian::translation-check translation="14745a0412e79a9104fb5a5c3db444fee2a8c3d2" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités on été signalées à l’encontre de wordpress :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28032">CVE-2020-28032</a>

<p>WordPress avant la version 4.7.19 gère incorrectement les requêtes de
désérialisation dans wp-includes/Requests/Utility/FilteredIterator.php.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28033">CVE-2020-28033</a>

<p>WordPress avant la version 4.7.19 gère incorrectement les encapsulations des
sites désactivés d’un réseau multisite, comme le montre l’autorisation d’un
pourriel encapsulé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28034">CVE-2020-28034</a>

<p>WordPress avant la version 4.7.19 permet un script intersite (XSS) associé
avec des variables globales.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28035">CVE-2020-28035</a>

<p>WordPress avant la version 4.7.19 permet à des attaquants d’élever leurs
privilèges à l’aide du protocole XML-RPC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28036">CVE-2020-28036</a>

<p>wp-includes/class-wp-xmlrpc-server.php dans WordPress avant la version 4.7.19
permet à des attaquants d’élever leurs privilèges en utilisant le protocole
XML-RPC pour commenter une publication.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28037">CVE-2020-28037</a>

<p>is_blog_installed dans wp-includes/functions.php dans WordPress avant la
version 4.7.19 détermine improprement si WordPress est déjà installé. Cela
pourrait permettre à un attaquant de réaliser une nouvelle installation,
conduisant à une exécution de code à distance (ainsi qu’à un déni de service
pour l’ancienne installation).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28038">CVE-2020-28038</a>

<p>WordPress avant la version 4.7.19 permet le stockage d’XSS à l’aide
d’identifiants (slugs) de publication.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28039">CVE-2020-28039</a>

<p>is_protected_meta dans wp-includes/meta.php dans WordPress avant la
version 4.7.19 permet une suppression de fichier arbitraire car il ne détermine
pas correctement si une clé méta est considérée comme protégée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28040">CVE-2020-28040</a>

<p>WordPress avant la version 4.7.19 permet des attaques CSRF changeant
l’image d’arrière-plan de thème.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 4.7.19+dfsg-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wordpress.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de wordpress, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/wordpress">https://security-tracker.debian.org/tracker/wordpress</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2429.data"
# $Id: $
