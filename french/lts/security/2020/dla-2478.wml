#use wml::debian::translation-check translation="0a71c5c7fa8f6a933e3c3c3afed61d77ca3b413e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le système de base de
données PostgreSQL.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25694">CVE-2020-25694</a>

<p>Peter Eisentraut a trouvé que des reconnexions vers la base de données
pourraient abandonner des options de la connexion originelle, telles que le
chiffrement. Cela pourrait conduire à une divulgation d'informations ou à une
attaque d’homme du milieu.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25695">CVE-2020-25695</a>

<p>Etienne Stalmans a signalé qu’un utilisateur, avec permission de créer des
objets non temporaires dans un schéma, peut exécuter des fonctions arbitraires
SQL en tant que superutilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25696">CVE-2020-25696</a>

<p>Nick Cleaton a trouvé que la commande \gset modifiait des variables qui
contrôlaient le comportement de psql. Cela pourrait aboutir à un serveur
compromis ou malveillant exécutant du code arbitraire dans la session de
l’utilisateur.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 9.6.20-0+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets postgresql-9.6.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de postgresql-9.6, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/postgresql-9.6">https://security-tracker.debian.org/tracker/postgresql-9.6</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2478.data"
# $Id: $
