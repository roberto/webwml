#use wml::debian::translation-check translation="5e003aa9ca25237cbd3fea020bd3e46950398d84" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Le gem JSON jusqu’à la version 2.2.0 pour Ruby, comme utilisé dans Ruby 2.1,
possède une vulnérabilité de création non sûre d’objet. Cela ressemble beaucoup
à <a href="https://security-tracker.debian.org/tracker/CVE-2013-0269">CVE-2013-0269</a>,
mais ne repose pas sur un comportement laxiste du ramasse-miettes au sein de Ruby.
Particulièrement, l’utilisation de méthodes d’analyse JSON peut conduire à la
création d’objet malveillant dans l’interpréteur, avec des effets négatifs
variables selon l’application.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 2.1.5-2+deb8u10.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby2.1.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2192.data"
# $Id: $
