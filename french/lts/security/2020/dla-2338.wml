#use wml::debian::translation-check translation="ce4cd86d5bf126f0056ff5fc640643aa295c3dc2" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs fuites de mémoire ont été découvertes dans proftpd-dfsg, un démon
FTP polyvalent d’hébergement virtuel, lorsque mod_facl ou mod_sftp sont utilisés. 
Elles pourraient conduire à un épuisement de mémoire et un déni de service.</p>

<p>La mise à jour rend les mises à niveau automatiques de proftpd-dfsg de
Debian 8 vers Debian 9 de nouveau possibles.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1.3.5e+r1.3.5b-4+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets proftpd-dfsg.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de proftpd-dfsg, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/proftpd-dfsg">https://security-tracker.debian.org/tracker/proftpd-dfsg</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2338.data"
# $Id: $
