#use wml::debian::translation-check translation="43410f66f58139428427baec12278be919a5d05f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans la bibliothèque Oniguruma
d’expressions bibliothèque rationnelles, surtout utilisée dans des chaînes
multi-octets (mbstring) PHP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13224">CVE-2019-13224</a>

<p>Une utilisation de mémoire après libération dans onig_new_deluxe() dans
regext.c permet à des attaquants de provoquer une divulgation éventuelle
d'informations, un déni de service, ou, éventuellement, une exécution de code
en fournissant des expressions rationnelles contrefaites. L’attaquant fournit
une paire d’expression rationnelle et de chaîne, avec un encodage multi-octet,
géré par onig_new_deluxe().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16163">CVE-2019-16163</a>

<p>Oniguruma permet un épuisement de pile dans regcomp.c à cause d’une récursion
dans regparse.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19012">CVE-2019-19012</a>

<p>Un dépassement d'entier dans la fonction search_in_range dans regexec.c dans
Onigurama conduit à une lecture hors limites, dans laquelle le décalage de cette
lecture est sous le contrôle de l’attaquant. Cela touche uniquement la version
compilée en 32 bits. Des attaquants distants peuvent provoquer un déni de
service ou une divulgation d'informations, ou, éventuellement, avoir un impact
non précisé, à l'aide d'une expression rationnelle contrefaite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19203">CVE-2019-19203</a>

<p>Un problème a été découvert dans Oniguruma. Dans la fonction
gb18030_mbc_enc_len dans le fichier gb18030.c, un pointeur UChar est
déréférencé sans vérifier s’il dépasse la chaîne mise en correspondance. Cela
conduit à une lecture hors limites de tampon de tas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19204">CVE-2019-19204</a>

<p>Un problème a été découvert dans Oniguruma. Dans la fonction
fetch_interval_quantifier (anciennement connue comme fetch_range_quantifier)
dans regparse.c, PFETCH est appelé sans vérification de PEND. Cela conduit à une
lecture hors limites de tampon de tas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19246">CVE-2019-19246</a>

<p>Oniguruma possède une lecture hors limites de tampon de tas dans
str_lower_case_match dans regexec.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26159">CVE-2020-26159</a>

<p>Dans Oniguruma, un attaquant capable de fournir une expression rationnelle
pour la compilation peut outrepasser un tampon d’un octet dans
concat_opt_exact_str dans src/regcomp.c</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 6.1.3-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libonig.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libonig, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libonig">https://security-tracker.debian.org/tracker/libonig</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2431.data"
# $Id: $
