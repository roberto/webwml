#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Quelques vulnérabilités de sécurité ont été trouvées dans Mercurial qui
permettent à des utilisateurs authentifiés de déclencher l’exécution de code
arbitraire et l’accès à des données non autorisé dans certaine configuration de
serveur. Des correctifs et des dépôts malformés peuvent conduire à des plantages
et à l’exécution de code arbitraire dans les clients.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9462">CVE-2017-9462</a>

<p>Dans Mercurial avant 4.1.3, « hg serve --stdio » permet à les utilisateurs
authentifiés distants de lancer le débogueur de Python, et par conséquent,
exécuter du code arbitraire, en utilisant --debugger comme nom de dépôt.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17458">CVE-2017-17458</a>

<p>Dans Mercurial avant 4.4.1, il est possible qu’un répertoire malformé pour
l’occasion peut causer que des sous-dépôts exécutent du code arbitraire sous
forme de script .git/hooks/post-update vérifié dans le dépôt. L’utilisation
typique de Mercurial empêche la construction de tels dépôts, mais ils peuvent
être créés par programmation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000132">CVE-2018-1000132</a>

<p>Mercurial, version 4.5 et précédentes, contient une vulnérabilité de
contrôle d’accès incorrect (CWE-285) dans le serveur de protocole, qui pourrait
aboutir à un accès à des données non autorisé. Cette attaque semble être
exploitable à l’aide d’une connectivité réseau. Cette vulnérabilité semble
avoir été corrigée dans la version 4.5.1.</p>

<li>OVE-20180430-0001

<p>mpatch : prudence lors de l’analyse de données binaires modifiées</p></li>

<li>OVE-20180430-0002

<p>mpatch : protection contre une sous-alimentation dans mpatch_apply</p></li>

<li>OVE-20180430-0004

<p>mpatch : assurance qu’un élément start n’est pas après la fin d’orig</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.1.2-2+deb8u5.</p>
<p>Nous vous recommandons de mettre à jour vos paquets mercurial.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1414.data"
# $Id: $
