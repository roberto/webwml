#use wml::debian::translation-check translation="67bf8c09e902baf452131a8e0dddd38bd19d7d2b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Un problème a été découvert dans wpa, un ensemble d’outils pour prendre
en charge WPA et WPA2 (IEEE 802.11i). Une absence de validation des données
peut aboutir à un écrasement de tampon, pouvant conduire à un déni de service
du processus wpa_supplicant ou, éventuellement, à l’exécution de code
arbitraire.</p>

<p>Sur demande, avec ce téléversement, la prise en charge de
WPA-EAP-SUITE-B(-192) a été intégrée.</p>


<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 2:2.4-1+deb9u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wpa.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de wpa, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/wpa">\
https://security-tracker.debian.org/tracker/wpa</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2572.data"
# $Id: $
