#use wml::debian::translation-check translation="6d6f95108073448c4496d55adc37bf30c2b207fb" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Jonathan Schlue a découvert une vulnérabilité dans Salt, un puissant
gestionnaire d'exécution à distance. Un utilisateur qui a le contrôle de la
source et des URL source_hash peut obtenir un accès complet au système de
fichiers en tant que superutilisateur sur un client de salt (« minion »).</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
2016.11.2+ds-1+deb9u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets salt.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de salt, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/salt">\
https://security-tracker.debian.org/tracker/salt</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2823.data"
# $Id: $
