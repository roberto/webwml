#use wml::debian::translation-check translation="6547e2ef597e2fb74539c6e0f44f578d5eceb2b9" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une fuite d’identifiant d'authentification
distante dans lynx, le navigateur web en mode texte.</p>
<p>Le paquet gère désormais correctement les sous-composants d’authentification
dans les URI (par exemple, <tt>https://user:pass@example.com</tt>) pour éviter
que des attaquants distants découvrent les identifiants en clair
dans les données de connexion SSL.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38165">CVE-2021-38165</a>

<p>Lynx jusqu’à la version 2.8.9 gère incorrectement le sous-composant userinfo
d’un URI. Cela permet à des attaquants distants de découvrir les identifiants
en clair car ils peuvent apparaître dans les données SNI.</p></li>

</ul>

<p>Pour Debian 9 « Stretch », ce problème a été corrigé dans
la version 2.8.9dev11-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets lynx.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2736.data"
# $Id: $
