#use wml::debian::translation-check translation="5a92f5ba5c86bcac9b588a3e7656db8f48163007" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Tiff3 est sujet à plusieurs problèmes pouvant aboutir au moins à des dénis de
service d’applications utilisant libtiff4. Des fichiers TIFF contrefaits
peuvent être fournis pour déclencher des appels abort() à l’aide d’assertions
défaillantes, de dépassements de tampon (à la fois dans les modes lecture et
écriture).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8781">CVE-2015-8781</a>

<p>La fonction tif_luv.c dans libtiff permet à des attaquants de provoquer un
déni de service (écriture hors limites) à l'aide d'un nombre non valable
d’échantillons par pixel dans une image TIFF compressée LogL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8782">CVE-2015-8782</a>

<p>La fonction tif_luv.c dans libtiff permet à des attaquants de provoquer un
déni de service (écriture hors limites) à l'aide d'une image TIFF contrefaite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8783">CVE-2015-8783</a>

<p>La fonction tif_luv.c dans libtiff permet à des attaquants de provoquer un
déni de service (lectures hors limites) à l'aide d'une image TIFF contrefaite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8784">CVE-2015-8784</a>

<p>La fonction NeXTDecode dans tif_next.c dans LibTIFF permet à des attaquants
distants de provoquer un déni de service (écriture hors limites) à l'aide d'une
image TIFF contrefaite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9533">CVE-2016-9533</a>

<p>La fonction tif_pixarlog.c dans libtiff 4.0.6 est sujette à une vulnérabilité
d’écriture hors limites dans les tampons alloués de tas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9534">CVE-2016-9534</a>

<p>La fonction tif_write.c dans libtiff 4.0.6 a un problème dans le chemin de
code d’erreur de TIFFFlushData1() qui ne réinitialisait pas les membres
tif_rawcc et tif_rawcp. </p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9535">CVE-2016-9535</a>

<p>Les fonctions tif_predict.h et tif_predict.c dans libtiff 4.0.6 possèdent des
assertions pouvant conduire à l’échec d’assertions dans le mode de débogage, ou
des dépassements de tampon dans le mode release, lors du traitement avec des
tailles inhabituelles de tuile, comme YCbCr avec le sous-échantillonnage.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 3.9.6-11+deb7u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tiff3.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-880.data"
# $Id: $
