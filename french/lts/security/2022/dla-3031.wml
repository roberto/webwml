#use wml::debian::translation-check translation="4525b07077115b88d5f669adae1c34303d30bbb7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il y avait une potentielle attaque par épuisement de ressources dans
modsecurity-apache, un module Apache qui inspecte les requêtes HTTP dans le
but de prévenir des attaques typiques à l'encontre des applications web
telles que les attaques par script intersite et les injections de code SQL.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42717">CVE-2021-42717</a>

<p>ModSecurity 3.x jusqu'à la version 3.0.5 ne gérait pas correctement les
objets JSON excessivement imbriqués. Des objets JSON contrefaits avec une
imbrication de dizaines de milliers de niveaux pouvaient avoir pour
conséquence que le serveur web était incapable de servir des requêtes
légitimes. Même des requêtes HTTP modérément grandes (par exemple, 300 Ko)
pouvaient occuper un des processus <q>worker</q> limité de NGINX pendant
plusieurs minutes et consommer presque tout le processeur disponible sur la
machine. Modsecurity 2 est également vulnérable : les versions de 2.8.0 à
2.9.4 sont affectées.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
2.9.1-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets modsecurity-apache.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3031.data"
# $Id: $
