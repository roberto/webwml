#use wml::debian::translation-check translation="2b7ba7e5ee0d864ef9f02c5e32ce4f8f29ec2dff" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans PHP, un langage de
script généraliste au source libre couramment utilisé, qui pouvaient aboutir à
un déni de service, à une divulgation d'informations, à un traitement non
sécurisé de cookie ou éventuellement à une exécution de code arbitraire.</p>

<p>Pour Debian 10 « Buster », ces problèmes ont été corrigés dans
la version 7.3.31-1~deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets php7.3.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de php7.3,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/php7.3">\
https://security-tracker.debian.org/tracker/php7.3</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3243.data"
# $Id: $
