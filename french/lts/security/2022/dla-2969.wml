#use wml::debian::translation-check translation="d3400c2bf7b1f899a27697c9457ace29e2d3be23" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités sont corrigées dans Asterisk, une boîte à
outils au code source ouvert pour autocommutateur téléphonique (PBX).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13161">CVE-2019-13161</a>

<p>Un attaquant était capable de planter Asterisk lors du traitement d’une
réponse SDP à une ré-invite T.38 sortante.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18610">CVE-2019-18610</a>

<p>Les utilisateurs distants d’AMI (Asterisk Manager Interface)
authentifiés sans autorisation du système pouvaient exécuter des commandes
système arbitraires.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18790">CVE-2019-18790</a>

<p>Vulnérabilité de détournement d’appel SIP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18976">CVE-2019-18976</a>

<p>Un plantage peut survenir si Asterisk reçoit une ré-invite d'envoi d'un
fax T 38 et a un port 0 et pas de ligne c= dans le SDP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28242">CVE-2020-28242</a>

<p>Si Asterisk reçoit une demande d'authentification après une invite
sortante, il entre dans une boucle infinie d'envois d'invite menant à une
consommation excessive de mémoire provoquant l'arrêt ou le redémarrage
de l'application.</p></li>
</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1:13.14.1~dfsg-2+deb9u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets asterisk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de asterisk, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/asterisk">\
https://security-tracker.debian.org/tracker/asterisk</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2969.data"
# $Id: $
