#use wml::debian::translation-check translation="d21dc7a4d0b9d3a2ca38023562681f6a3b843af7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été découverts dans Epiphany, le navigateur web
de GNOME, permettant des attaques par script intersite par des sites web
malveillants ou une corruption de mémoire et le plantage de l'application
par une page avec un titre très long.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans la
version 3.32.1.2-3~deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets epiphany-browser.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de epiphany-browser,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/epiphany-browser">\
https://security-tracker.debian.org/tracker/epiphany-browser</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3074.data"
# $Id: $
