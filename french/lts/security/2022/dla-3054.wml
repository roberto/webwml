#use wml::debian::translation-check translation="14bd3f4a76657de0c42a8818f1ee08abf7f94d2d" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

Plusieurs vulnérabilités de sécurité ont été découvertes dans sleuthkit,
une collection d'outils pour l’analyse légale de données de volume et de
système de fichiers.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13755">CVE-2017-13755</a>

<p>L'ouverture d'une image ISO 9660 contrefaite déclenche une lecture hors
limites dans iso9660_proc_dir() dans tsk/fs/iso9660_dent.c de libtskfs.a,
comme démontré par fls.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13756">CVE-2017-13756</a>

<p>L'ouverture d'une image disque contrefaite déclenche une récursion
infinie dans dos_load_ext_table() dans tsk/vs/dos.c de libtskvs.a, comme
démontré par mmls.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13760">CVE-2017-13760</a>

<p>fls bloque sur une image exfat corrompue dans tsk_img_read() dans
tsk/img/img_io.c de libtskimg.a.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19497">CVE-2018-19497</a>

<p>Dans Sleuth Kit (TSK) jusqu'à la version 4.6.4, hfs_cat_traverse dans
tsk/fs/hfs.c ne détermine pas correctement quand une longueur de clé est
trop importante. Cela permet à des attaquants de provoquer un déni de
service (SEGV sur une adresse inconnue avec accès en lecture à la mémoire
dans un appel tsk_getu16 dans hfs_dir_open_meta_cb de tsk/fs/hfs_dent.c).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10232">CVE-2020-10232</a>

<p>Évite un dépassement de tampon de pile dans yaffsfs_istat par
l'augmentation de la taille du tampon à celle requise par
tsk_fs_time_to_str.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1010065">CVE-2019-1010065</a>

<p>Les versions de Sleuth Kit 4.6.0 et antérieures sont affectées par un
dépassement d'entier. L'impact est un plantage dans tsk/fs/hfs_dent.c:237
déclenché par l'ouverture d'une image disque contrefaite. Le composant
concerné est l'outil fls utilisé sur les images HFS. Le bogue se trouve
dans le fichier tsk/fs/hfs.c dans la fonction hfs_cat_traverse() (lignes
952 et 1062). Le vecteur de l'attaque est l'ouverture par la victime d'une
image de système de fichiers NFS contrefaite.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 4.4.0-5+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sleuthkit.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de sleuthkit, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/sleuthkit">\
https://security-tracker.debian.org/tracker/sleuthkit</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3054.data"
# $Id: $
