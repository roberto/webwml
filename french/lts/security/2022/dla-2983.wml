#use wml::debian::translation-check translation="ae61afb684d6abc7ab29ae9261a02c0941731e58" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans abcm2ps : un programme
de traduction de fichier de description de musique du format ABC vers le
format PostScript.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10753">CVE-2018-10753</a>

<p>Un dépassement de tampon de pile dans la fonction delayed_output de
music.c permet à des attaquants distants de provoquer un déni de service
(plantage d'application) ou d'avoir éventuellement un autre impact non
précisé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10771">CVE-2018-10771</a>

<p>Un dépassement de tampon de pile dans la fonction get_key de parse.c
permet à des attaquants distants de provoquer un déni de service (plantage
d'application) ou d'avoir éventuellement un autre impact non précisé</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1010069">CVE-2019-1010069</a>

<p>Un contrôle d'accès incorrect permet à des attaquants de provoquer un
déni de service à l'aide d'un fichier contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32434">CVE-2021-32434</a>

<p>Un dépassement de tableau quand il y a une durée fausse dans un
recouvrement de voix.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32435">CVE-2021-32435</a>

<p>Un dépassement de tampon de pile dans la fonction get_key de parse.c
permet à des attaquants distants de provoquer un déni de service (DoS) par
des moyens non précisés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32436">CVE-2021-32436</a>

<p>Une lecture hors limites dans la fonction write_title() de subs.c permet
à des attaquants distants de provoquer un déni de service par des moyens
non précisés.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 7.8.9-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets abcm2ps.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de abcm2ps, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/abcm2ps">\
https://security-tracker.debian.org/tracker/abcm2ps</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2983.data"
# $Id: $
