#use wml::debian::translation-check translation="7051c004fc95f256461cd1e95a97fc3e255cf6dc" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Les vulnérabilités suivantes ont été découvertes dans rails, un
cadriciel MVC basé sur Ruby et destiné au développement d’applications web.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21831">CVE-2022-21831</a>

<p>Une vulnérabilité d'injection de code existe dans Active Storage pouvait
permettre à un attaquant d'exécuter du code au moyen des arguments de
image_processing.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-22577">CVE-2022-22577</a>

<p>Une vulnérabilité de script intersite dans Action Pack pouvait permettre
à un attaquant de contourner la CSP pour les réponses qui ne ressemblent
pas au HTML.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23633">CVE-2022-23633</a>

<p>Action Pack est un cadriciel pour le traitement et la réponse à des
requêtes web. Dans certaines circonstances, le corps des réponses n'est pas
fermé. Dans le cas où une réponse <strong>n'est pas</strong>* notifiée d'un
<q>close</q>, <q>ActionDispatch::Executor</q> ne sait pas qu'il doit
réinitialiser l'état local du fil d'exécution de la requête suivante. Cela
peut conduire à la divulgation de données aux requêtes suivantes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-27777">CVE-2022-27777</a>

<p>Une vulnérabilité de script intersite dans des <q>tag helpers</q>
d'Action View qui permettait à un attaquant d'injecter des contenus s'il
est capable de contrôler une entrée dans des attributs spécifiques.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32224">CVE-2022-32224</a>

<p>Quand des colonnes sérialisées qui utilisent YAML (par défaut) sont
désérialisées, Rails utilise YAML.unsafe_load pour convertir les données
YAML en objets Ruby. Si un attaquant peut manipuler des données dans la
base de données (avec des moyens tels qu'une injection SQL), il peut alors
être possible pour l'attaquant d’obtenir des privilèges supérieurs pour une
exécution de code à distance (RCE).</p></li>

</ul>

<p>Pour Debian 10 Buster, ces problèmes ont été corrigés dans la version
2:5.2.2.1+dfsg-1+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets rails.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de rails, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/rails">\
https://security-tracker.debian.org/tracker/rails</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3093.data"
# $Id: $
