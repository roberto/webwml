#
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: (null)\n"
"PO-Revision-Date: 2019-12-15 01:55+0100\n"
"Last-Translator: Andreas Rönnquist <andreas@ronnquist.net>\n"
"Language-Team: Swedish <debian-l10n-swedish@lists.debian.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"

#: ../../english/template/debian/votebar.wml:13
msgid "Date"
msgstr "Datum"

#: ../../english/template/debian/votebar.wml:16
msgid "Time Line"
msgstr "Tidslinje"

#: ../../english/template/debian/votebar.wml:19
msgid "Summary"
msgstr "Sammanfattning"

#: ../../english/template/debian/votebar.wml:22
msgid "Nominations"
msgstr "Nomineringar"

#: ../../english/template/debian/votebar.wml:25
msgid "Withdrawals"
msgstr "Upphävanden"

#: ../../english/template/debian/votebar.wml:28
msgid "Debate"
msgstr "Debatt"

#: ../../english/template/debian/votebar.wml:31
msgid "Platforms"
msgstr "Plattformar"

#: ../../english/template/debian/votebar.wml:34
msgid "Proposer"
msgstr "Föreslaget av"

#: ../../english/template/debian/votebar.wml:37
msgid "Proposal A Proposer"
msgstr "Förslag A föreslaget av"

#: ../../english/template/debian/votebar.wml:40
msgid "Proposal B Proposer"
msgstr "Förslag B föreslaget av"

#: ../../english/template/debian/votebar.wml:43
msgid "Proposal C Proposer"
msgstr "Förslag C föreslaget av"

#: ../../english/template/debian/votebar.wml:46
msgid "Proposal D Proposer"
msgstr "Förslag D föreslaget av"

#: ../../english/template/debian/votebar.wml:49
msgid "Proposal E Proposer"
msgstr "Förslag E föreslaget av"

#: ../../english/template/debian/votebar.wml:52
msgid "Proposal F Proposer"
msgstr "Förslag F föreslaget av"

#: ../../english/template/debian/votebar.wml:55
msgid "Proposal G Proposer"
msgstr "Förslag G föreslaget av"

#: ../../english/template/debian/votebar.wml:58
msgid "Proposal H Proposer"
msgstr "Förslag H föreslaget av"

#: ../../english/template/debian/votebar.wml:61
msgid "Seconds"
msgstr "Sekunderingar"

#: ../../english/template/debian/votebar.wml:64
msgid "Proposal A Seconds"
msgstr "Sekunderingar av förslag A"

#: ../../english/template/debian/votebar.wml:67
msgid "Proposal B Seconds"
msgstr "Sekunderingar av förslag B"

#: ../../english/template/debian/votebar.wml:70
msgid "Proposal C Seconds"
msgstr "Sekunderingar av förslag C"

#: ../../english/template/debian/votebar.wml:73
msgid "Proposal D Seconds"
msgstr "Sekunderingar av förslag D"

#: ../../english/template/debian/votebar.wml:76
msgid "Proposal E Seconds"
msgstr "Sekunderingar av förslag E"

#: ../../english/template/debian/votebar.wml:79
msgid "Proposal F Seconds"
msgstr "Sekunderingar av förslag F"

#: ../../english/template/debian/votebar.wml:82
msgid "Proposal G Seconds"
msgstr "Sekunderingar av förslag G"

#: ../../english/template/debian/votebar.wml:85
msgid "Proposal H Seconds"
msgstr "Sekunderingar av förslag H"

#: ../../english/template/debian/votebar.wml:88
msgid "Opposition"
msgstr "Opponenter"

#: ../../english/template/debian/votebar.wml:91
msgid "Text"
msgstr "Text"

#: ../../english/template/debian/votebar.wml:94
msgid "Proposal A"
msgstr "Förslag A"

#: ../../english/template/debian/votebar.wml:97
msgid "Proposal B"
msgstr "Förslag B"

#: ../../english/template/debian/votebar.wml:100
msgid "Proposal C"
msgstr "Förslag C"

#: ../../english/template/debian/votebar.wml:103
msgid "Proposal D"
msgstr "Förslag D"

#: ../../english/template/debian/votebar.wml:106
msgid "Proposal E"
msgstr "Förslag E"

#: ../../english/template/debian/votebar.wml:109
msgid "Proposal F"
msgstr "Förslag F"

#: ../../english/template/debian/votebar.wml:112
msgid "Proposal G"
msgstr "Förslag G"

#: ../../english/template/debian/votebar.wml:115
msgid "Proposal H"
msgstr "Förslag H"

#: ../../english/template/debian/votebar.wml:118
msgid "Choices"
msgstr "Alternativ"

#: ../../english/template/debian/votebar.wml:121
msgid "Amendment Proposer"
msgstr "Ändringar föreslagna av"

#: ../../english/template/debian/votebar.wml:124
msgid "Amendment Seconds"
msgstr "Sekunderingar av ändringar"

#: ../../english/template/debian/votebar.wml:127
msgid "Amendment Text"
msgstr "Text för ändringar"

#: ../../english/template/debian/votebar.wml:130
msgid "Amendment Proposer A"
msgstr "Ändring A föreslagen av"

#: ../../english/template/debian/votebar.wml:133
msgid "Amendment Seconds A"
msgstr "Ändring A sekunderas av"

#: ../../english/template/debian/votebar.wml:136
msgid "Amendment Text A"
msgstr "Text för ändring A"

#: ../../english/template/debian/votebar.wml:139
msgid "Amendment Proposer B"
msgstr "Ändring B föreslagen av"

#: ../../english/template/debian/votebar.wml:142
msgid "Amendment Seconds B"
msgstr "Ändring B sekunderas av"

#: ../../english/template/debian/votebar.wml:145
msgid "Amendment Text B"
msgstr "Text för ändring B"

#: ../../english/template/debian/votebar.wml:148
msgid "Amendment Proposer C"
msgstr "Ändring C föreslagen av"

#: ../../english/template/debian/votebar.wml:151
msgid "Amendment Seconds C"
msgstr "Ändring C sekunderas av"

#: ../../english/template/debian/votebar.wml:154
msgid "Amendment Text C"
msgstr "Text för ändring C"

#: ../../english/template/debian/votebar.wml:157
msgid "Amendments"
msgstr "Ändringar"

#: ../../english/template/debian/votebar.wml:160
msgid "Proceedings"
msgstr "Protokoll"

#: ../../english/template/debian/votebar.wml:163
msgid "Majority Requirement"
msgstr "Majoritetskrav"

#: ../../english/template/debian/votebar.wml:166
msgid "Data and Statistics"
msgstr "Data och statistik"

#: ../../english/template/debian/votebar.wml:169
msgid "Quorum"
msgstr "Beslutsmässigt antal"

#: ../../english/template/debian/votebar.wml:172
msgid "Minimum Discussion"
msgstr "Minsta diskussion"

#: ../../english/template/debian/votebar.wml:175
msgid "Ballot"
msgstr "Röstsedel"

#: ../../english/template/debian/votebar.wml:178
msgid "Forum"
msgstr "Forum"

#: ../../english/template/debian/votebar.wml:181
msgid "Outcome"
msgstr "Resultat"

#: ../../english/template/debian/votebar.wml:185
msgid "Waiting&nbsp;for&nbsp;Sponsors"
msgstr "Väntar&nbsp;på&nbsp;sponsor"

#: ../../english/template/debian/votebar.wml:188
msgid "In&nbsp;Discussion"
msgstr "Under&nbsp;diskussion"

#: ../../english/template/debian/votebar.wml:191
msgid "Voting&nbsp;Open"
msgstr "Röstning&nbsp;pågår"

#: ../../english/template/debian/votebar.wml:194
msgid "Decided"
msgstr "Beslutade"

#: ../../english/template/debian/votebar.wml:197
msgid "Withdrawn"
msgstr "Tillbakadragna"

#: ../../english/template/debian/votebar.wml:200
msgid "Other"
msgstr "Annat"

#: ../../english/template/debian/votebar.wml:204
msgid "Home&nbsp;Vote&nbsp;Page"
msgstr "Hem&nbsp;-&nbsp;Röstningssida"

#: ../../english/template/debian/votebar.wml:207
msgid "How&nbsp;To"
msgstr "Hur&nbsp;man..."

#: ../../english/template/debian/votebar.wml:210
msgid "Submit&nbsp;a&nbsp;Proposal"
msgstr "Avger&nbsp;förslag"

#: ../../english/template/debian/votebar.wml:213
msgid "Amend&nbsp;a&nbsp;Proposal"
msgstr "Ändrar&nbsp;förslag"

#: ../../english/template/debian/votebar.wml:216
msgid "Follow&nbsp;a&nbsp;Proposal"
msgstr "Följer&nbsp;förslag"

#: ../../english/template/debian/votebar.wml:219
msgid "Read&nbsp;a&nbsp;Result"
msgstr "Läser&nbsp;resultat"

#: ../../english/template/debian/votebar.wml:222
msgid "Vote"
msgstr "Röstar"
