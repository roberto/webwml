#use wml::debian::template title="Debian-Installer errata"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="9a1725c7a7c2a16470f0814224c0b78cecb2e2fe" mindelta="1" maxdelta="1"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"

<h1>Kända problem i <humanversion /></h1>

<p>
Detta är en lista på kända problem i utgåvan <humanversion /> av
Debian Installer. Om du inte ser ditt problem i listan här, vänligen sänd in en
<a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">installationsrapport</a>
som beskriver problemet.
</p>

<dl class="gloss">

   <dt>Tema som används i installeraren</dt>
   <dd>Det finns inget Bookwork-utseende ännu, och installeraren använder
      fortfarande utseendet från Bullseye.
      <br/>
      <b>Status:</b> Fullständigt rättat i Bookworm RC 1.
   </dd>

   <dt>Fastprogramvara (firmware) krävs för vissa ljudkort</dt>
   <dd>Det verkar finnas ett antal ljudkort som kräver att en
   fastprogramvara (firmware) laddas för att leverera ljud 
   <a href="https://bugs.debian.org/992699">#992699</a>).
   <br/>
   <b>Status:</b> Rättat i Bookworm Alpha 1.
   </dd>

   <dt>Krypterad LVM kan misslyckas på system med lite minne</dt>
   <dd>Det är möjligt att system med lite minne (exempelvis 1 GB) råkar ut
   för misslyckande att sätta upp krypterad LVM: cryptsetup kan trigga
   out-of-memory-dödaren vid formattering av LUKS-partitionen
   (<a href="https://bugs.debian.org/1028250">#1028250</a>,
   <a href="https://gitlab.com/cryptsetup/cryptsetup/-/issues/802">cryptsetup uppströms</a>).
   <br />
   <b>Status:</b> Rättat i Bookworm RC 2.</dd>


   <dt>Trasig “Guidad - använd hela disken och ställ in LVM”-partitionering i UEFI-läge</dt>
   <dd>Användning av denna typ av guidad partitionering resulterar i en
   "Tvinga UEFI-installation?"-prompt (även om det inte finns andra
   operativsystem), med standardalternativet "Nej"
   (<a href="https://bugs.debian.org/1033913">#1033913</a>).
   Att välja standardalternativet är mycket troligt att resultera i
   ett icke-startbart system om Secure Boot är aktiverat. Att växla till
   "Ja" bör undvika detta problem.
   <br />
   <b>Status:</b> Rättat i Bookworm RC 2.</dd>


</dl>
