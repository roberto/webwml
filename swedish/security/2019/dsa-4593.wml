#use wml::debian::translation-check translation="4b462ea192fc355c557625a4edd8b02668ca91dd" mindelta="1"
<define-tag description>säkerhetsuppdatering</define-tag>
<define-tag moreinfo>
<p>Det har upptäckts att freeimage, ett grafikbibliotek, påverkades av
följande två säkerhetsproblem:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12211">CVE-2019-12211</a>

    <p>Heapbuffertspill orsakat av ogiltiga memcpy i PluginTIFF. Denna
    brist kan utnyttjas av fjärrangripare för att trigga överbelastning
    eller andra ospecificerade påverkan genom skapad TIFF-data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12213">CVE-2019-12213</a>

    <p>Stackförbrukning orsakas av oönskad rekursion i PluginTIFF. Denna
    brist kan utnyttjas av fjärrangripare för att trigga överbelastning
    via skapad TIFF-data.</p></li>

</ul>

<p>För den gamla stabila utgåvan (Stretch) har dessa problem rättats
i version 3.17.0+ds1-5+deb9u1.</p>

<p>För den stabila utgåvan (Buster) har dessa problem rättats i
version 3.18.0+ds2-1+deb10u1.</p>

<p>Vi rekommenderar att ni uppgraderar era freeimage-paket.</p>

<p>För detaljerad säkerhetsstatus om freeimage vänligen se
dess säkerhetsspårare på
<a href="https://security-tracker.debian.org/tracker/freeimage">\
https://security-tracker.debian.org/tracker/freeimage</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4593.data"
