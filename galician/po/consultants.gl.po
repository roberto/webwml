# Jorge Barreiro <yortx.barry@gmail.com>, 2012, 2014.
# Parodper <parodper@gmail.com>, 2022
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2022-05-13 09:42+0200\n"
"Last-Translator: Parodper <parodper@gmail.com>\n"
"Language-Team: Galician <debian-l10n-galician@lists.debian.org>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Poedit 3.0.1\n"

#: ../../english/consultants/consultant.defs:6
msgid "Name:"
msgstr "Nome:"

#: ../../english/consultants/consultant.defs:9
msgid "Company:"
msgstr "Empresa:"

#: ../../english/consultants/consultant.defs:12
msgid "Address:"
msgstr "Enderezo:"

#: ../../english/consultants/consultant.defs:15
msgid "Contact:"
msgstr "Contacto:"

#: ../../english/consultants/consultant.defs:19
msgid "Phone:"
msgstr "Teléfono:"

#: ../../english/consultants/consultant.defs:22
msgid "Fax:"
msgstr "Fax:"

#: ../../english/consultants/consultant.defs:25
msgid "URL:"
msgstr "Páxina web:"

#: ../../english/consultants/consultant.defs:29
msgid "or"
msgstr "ou"

#: ../../english/consultants/consultant.defs:34
msgid "Email:"
msgstr "Correo electrónico:"

#: ../../english/consultants/consultant.defs:52
msgid "Rates:"
msgstr "Puntuacións:"

#: ../../english/consultants/consultant.defs:55
msgid "Additional Information"
msgstr "Información adicional"

#: ../../english/consultants/consultant.defs:58
msgid "Willing to Relocate"
msgstr "Dispostos a desprazarse"

#: ../../english/consultants/consultant.defs:61
msgid ""
"<total_consultant> Debian consultants listed in <total_country> countries "
"worldwide."
msgstr ""
"Hai listados <total_consultant> asesores técnicos de Debian en "
"<total_country> países de todo o mundo."

#: ../../english/template/debian/consultant.wml:6
msgid "List of Consultants"
msgstr "Lista de asesores técnicos"

#: ../../english/template/debian/consultant.wml:9
msgid "Back to the <a href=\"./\">Debian consultants page</a>."
msgstr "Volver á <a href=\"./\">páxina de asesores técnicos de Debian</a>."
