#use wml::debian::template title="Debian &ldquo;stretch&rdquo; release-informatie"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="9f9207921c4ea799d5d3daa8a0f4b0faedf2b073"

<p>Debian <current_release_stretch> werd uitgebracht op
<a href="$(HOME)/News/<current_release_newsurl_stretch/>"><current_release_date_stretch></a>.
<ifneq "9.0" "<current_release>"
  "Debian 9.0 werd oorspronkelijk uitgebracht op <:=spokendate('2017-06-17'):>."
/>
De release bevatte verschillende ingrijpende wijzigingen, beschreven in ons
<a href="$(HOME)/News/2017/20170617">persbericht</a> en in de
<a href="releasenotes">notities bij de release</a>.</p>

<p><strong>Debian 9 werd vervangen door
<a href="../buster/">Debian 10 (<q>buster</q>)</a>.
Er worden geen beveiligingsupdates meer uitgebracht sinds <:=spokendate('2020-07-06'):>.
</strong></p>

<p><strong>Stretch heeft ook genoten van langetermijnondersteuning (Long Term Support - LTS) tot
eind juni 2022. De LTS was beperkt tot i386, amd64, armel, armhf en arm64.
Alle andere architecturen werden niet langer ondersteund in stretch.
Raadpleeg voor meer informatie de <a
href="https://wiki.debian.org/LTS">sectie over LTS op de Wiki van Debian</a>.
</strong></p>

<p>Raadpleeg de installatie-informatie-pagina en de
Installatiehandleiding over het verkrijgen en installeren
van Debian. Zie de instructies in
de <a href="releasenotes">notities bij de release</a> om van een oudere Debian release
op te waarderen.</p>


# Wat volgt activeren wanneer de LTS-periode begint.
<p>Ondersteunde architecturen tijdens de langetermijnondersteuning:</p>

<ul>
<li><a href="../../ports/amd64/">64-bits pc (amd64)</a>
<li><a href="../../ports/i386/">32-bits pc (i386)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/armhf/">Hard Float ABI ARM (armhf)</a>
<li><a href="../../ports/arm64/">64-bits ARM (AArch64)</a>
</ul>

<p>Ondersteunde computerarchitecturen bij de initiële release van buster:</p>
<ul>
<li><a href="../../ports/amd64/">64-bits pc (amd64)</a>
<li><a href="../../ports/i386/">32-bits pc (i386)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/armhf/">Hard Float ABI ARM (armhf)</a>
<li><a href="../../ports/mipsel/">MIPS (little endian)</a>
<li><a href="../../ports/mips/">MIPS (big endian)</a>
<li><a href="../../ports/s390x/">IBM System z</a>
<li><a href="../../ports/arm64/">64-bits ARM (AArch64)</a>
<li><a href="../../ports/ppc64el/">POWER Processors</a>
<li><a href="../../ports/mips64el/">64-bits MIPS (little endian)</a>
</ul>


<p>In tegenstelling tot wat we zouden wensen, kunnen er enkele problemen bestaan
in de release, ondanks dat deze <em>stabiel</em> wordt genoemd. We hebben
<a href="errata">een overzicht van de belangrijkste bekende problemen</a> gemaakt
en u kunt ons altijd andere problemen rapporteren.</p>
