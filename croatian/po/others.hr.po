msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 16:56+0200\n"
"Last-Translator: Josip Rodin\n"
"Language-Team: Croatian\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/devel/join/nm-steps.inc:7
msgid "New Members Corner"
msgstr ""

#: ../../english/devel/join/nm-steps.inc:10
msgid "Step 1"
msgstr ""

#: ../../english/devel/join/nm-steps.inc:11
msgid "Step 2"
msgstr ""

#: ../../english/devel/join/nm-steps.inc:12
msgid "Step 3"
msgstr ""

#: ../../english/devel/join/nm-steps.inc:13
msgid "Step 4"
msgstr ""

#: ../../english/devel/join/nm-steps.inc:14
msgid "Step 5"
msgstr ""

#: ../../english/devel/join/nm-steps.inc:15
msgid "Step 6"
msgstr ""

#: ../../english/devel/join/nm-steps.inc:16
msgid "Step 7"
msgstr ""

#: ../../english/devel/join/nm-steps.inc:19
msgid "Applicants' checklist"
msgstr ""

#: ../../english/devel/website/tc.data:11
msgid ""
"See <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</a> "
"(only available in French) for more information."
msgstr ""
"Pogledajte <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/"
"french/</a> (dostupno samo na francuskom) za više informacija."

#: ../../english/devel/website/tc.data:12
#: ../../english/devel/website/tc.data:14
#: ../../english/devel/website/tc.data:15
#: ../../english/devel/website/tc.data:16
#: ../../english/events/merchandise.def:145
#, fuzzy
msgid "More information"
msgstr "Dodatne informacije"

#: ../../english/devel/website/tc.data:13
msgid ""
"See <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> (only available in Spanish) for more information."
msgstr ""
"Pogledajte <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/"
"spanish/</a> (dostupno samo na španjolskom) za više informacija."

#: ../../english/distrib/pre-installed.defs:18
#, fuzzy
msgid "Phone"
msgstr "Telefon:"

#: ../../english/distrib/pre-installed.defs:19
#, fuzzy
msgid "Fax"
msgstr "Telefaks:"

#: ../../english/distrib/pre-installed.defs:21
#, fuzzy
msgid "Address"
msgstr "Adresa:"

#: ../../english/events/merchandise.def:13
msgid "Products"
msgstr "Proizvodi"

#: ../../english/events/merchandise.def:16
msgid "T-shirts"
msgstr "Majice"

#: ../../english/events/merchandise.def:19
msgid "hats"
msgstr "kape"

#: ../../english/events/merchandise.def:22
msgid "stickers"
msgstr "naljepnice"

#: ../../english/events/merchandise.def:25
msgid "mugs"
msgstr "šalice"

#: ../../english/events/merchandise.def:28
msgid "other clothing"
msgstr "ostala odjeća"

#: ../../english/events/merchandise.def:31
msgid "polo shirts"
msgstr "polo majice"

#: ../../english/events/merchandise.def:34
msgid "frisbees"
msgstr "frizbiji"

#: ../../english/events/merchandise.def:37
msgid "mouse pads"
msgstr "podloge za miša"

#: ../../english/events/merchandise.def:40
msgid "badges"
msgstr "bedževi"

#: ../../english/events/merchandise.def:43
msgid "basketball goals"
msgstr "koševi za košarku"

#: ../../english/events/merchandise.def:47
msgid "earrings"
msgstr "naušnice"

#: ../../english/events/merchandise.def:50
msgid "suitcases"
msgstr "torbe"

#: ../../english/events/merchandise.def:53
msgid "umbrellas"
msgstr ""

#: ../../english/events/merchandise.def:56
#, fuzzy
msgid "pillowcases"
msgstr "torbe"

#: ../../english/events/merchandise.def:59
msgid "keychains"
msgstr ""

#: ../../english/events/merchandise.def:62
msgid "Swiss army knives"
msgstr ""

#: ../../english/events/merchandise.def:65
msgid "USB-Sticks"
msgstr ""

#: ../../english/events/merchandise.def:80
msgid "lanyards"
msgstr ""

#: ../../english/events/merchandise.def:83
msgid "others"
msgstr ""

#: ../../english/events/merchandise.def:90
msgid "Available languages:"
msgstr ""

#: ../../english/events/merchandise.def:107
msgid "International delivery:"
msgstr ""

#: ../../english/events/merchandise.def:118
msgid "within Europe"
msgstr ""

#: ../../english/events/merchandise.def:122
msgid "Original country:"
msgstr ""

#: ../../english/events/merchandise.def:187
msgid "Donates money to Debian"
msgstr ""

#: ../../english/events/merchandise.def:192
msgid "Money is used to organize local free software events"
msgstr ""

#: ../../english/logos/index.data:6
msgid "With&nbsp;``Debian''"
msgstr "Uz&nbsp;tekst&nbsp;&#x201E;Debian&#x201D;"

#: ../../english/logos/index.data:9
msgid "Without&nbsp;``Debian''"
msgstr "Bez&nbsp;teksta&nbsp;&#x201E;Debian&#x201D;"

#: ../../english/logos/index.data:12
msgid "Encapsulated PostScript"
msgstr "Encapsulated PostScript"

#: ../../english/logos/index.data:18
msgid "[Powered by Debian]"
msgstr "[Powered by Debian]"

#: ../../english/logos/index.data:21
msgid "[Powered by Debian GNU/Linux]"
msgstr "[Powered by Debian GNU/Linux]"

#: ../../english/logos/index.data:24
msgid "[Debian powered]"
msgstr "[Debian powered]"

#: ../../english/logos/index.data:27
msgid "[Debian] (mini button)"
msgstr "[Debian] (malo dugme)"

#: ../../english/mirror/submit.inc:7
msgid "same as the above"
msgstr ""

#: ../../english/women/profiles/profiles.def:24
msgid "How long have you been using Debian?"
msgstr ""

#: ../../english/women/profiles/profiles.def:27
msgid "Are you a Debian Developer?"
msgstr ""

#: ../../english/women/profiles/profiles.def:30
msgid "What areas of Debian are you involved in?"
msgstr ""

#: ../../english/women/profiles/profiles.def:33
msgid "What got you interested in working with Debian?"
msgstr ""

#: ../../english/women/profiles/profiles.def:36
msgid ""
"Do you have any tips for women interested in getting more involved with "
"Debian?"
msgstr ""

#: ../../english/women/profiles/profiles.def:39
msgid ""
"Are you involved with any other women in technology group? Which one(s)?"
msgstr ""

#: ../../english/women/profiles/profiles.def:42
msgid "A bit more about you..."
msgstr ""

#~ msgid ""
#~ "<total_consultant> Debian consultants listed in <total_country> countries "
#~ "worldwide."
#~ msgstr ""
#~ "<: if (<total_consultant> % 10 == 0 or <total_consultant> % 10 > 4 or "
#~ "(<total_consultant> > 10 and <total_consultant> < 20)) { print \"Navedeno "
#~ "je <total_consultant> Debian konzultanata\"; } elsif (<total_consultant> "
#~ "% 10 == 1) { print \"Naveden je <total_consultant> Debian konzultant\"; } "
#~ "else { print \"Navedena su <total_consultant> Debian konzultanta\"; } "
#~ "print \" iz <total_country> \" . ((<total_country> % 10 == 0 or "
#~ "<total_country> % 10 > 4 or (<total_country> > 10 and <total_country> < "
#~ "20)) ? \"zemalja\" : \"zemlje\") . \" širom svijeta.\" . \"\\n\"; :>"

#~ msgid "<void id=\"d-i\" />Unknown"
#~ msgstr "<void id=\"d-i\" />Nepoznat"

#~ msgid "??"
#~ msgstr "??"

#~ msgid "ALL"
#~ msgstr "SVE"

#~ msgid "Architecture:"
#~ msgstr "Arhitektura:"

#~ msgid "BAD"
#~ msgstr "NE"

#~ msgid "BAD?"
#~ msgstr "NE?"

#~ msgid "Booting"
#~ msgstr "Podiže se"

#~ msgid "Building"
#~ msgstr "Radi se"

#~ msgid "Clear"
#~ msgstr "Isprazni"

#~ msgid "Company:"
#~ msgstr "Poduzeće:"

#~ msgid "Debian GNU/Linux: Your next Linux distribution."
#~ msgstr "Debian GNU/Linux: Your next Linux distribution."

#~ msgid "Debian: The Perfect OS"
#~ msgstr "Debian: The biggest is still the best."

#~ msgid "Debian: The biggest is still the best."
#~ msgstr "Debian: The biggest is still the best."

#~ msgid "Debian: apt-get into it."
#~ msgstr "Debian: apt-get into it."

#~ msgid "Debian: the potato has landed. announcing version 2.2"
#~ msgstr "Debian: the potato has landed. announcing version 2.2"

#~ msgid "Download"
#~ msgstr "Prijenos"

#~ msgid "Email:"
#~ msgstr "E-mail:"

#~ msgid "Mailing List Subscription"
#~ msgstr "Pretplata na mailing liste"

#~ msgid "Mailing List Unsubscription"
#~ msgstr "Otkazivanje pretplate na mailing liste"

#~ msgid "Moderated:"
#~ msgstr "Moderirana:"

#~ msgid "Name:"
#~ msgstr "Ime:"

#~ msgid "Newer Debian banners"
#~ msgstr "Noviji Debian natpisi"

#~ msgid "No description given"
#~ msgstr "Opis nije dan"

#~ msgid "No images"
#~ msgstr "Nema snimke"

#~ msgid "No kernel"
#~ msgstr "Nema kernel"

#~ msgid "Not yet"
#~ msgstr "Još ne"

#~ msgid ""
#~ "Note that most Debian mailing lists are public forums. Any mails sent to "
#~ "them will be published in public mailing list archives and indexed by "
#~ "search engines. You should only subscribe to Debian mailing lists using "
#~ "an e-mail address that you do not mind being made public."
#~ msgstr ""
#~ "Primijetite da su većina Debian mailing lista javni forumi. Svi e-mailovi "
#~ "koji se pošalju na njih će se objaviti na javnoj arhivi mailing lista i "
#~ "indeksirat će ih internetski pretraživači. Na Debianove mailing liste se "
#~ "trebate pretplatiti samo koristeći e-mail adresu za koju vam nije bitno "
#~ "hoće li se objaviti."

#~ msgid "OK"
#~ msgstr "DA"

#~ msgid "OK?"
#~ msgstr "DA?"

#~ msgid "Old banner ads"
#~ msgstr "Stari natpisi/oglasi"

#~ msgid "Older Debian banners"
#~ msgstr "Stariji Debian natpisi"

#~ msgid ""
#~ "Only messages signed by a Debian developer will be accepted by this list."
#~ msgstr ""
#~ "Samo poruke koje potpiše Debian razvijatelj će biti propuštene na ovu "
#~ "listu."

#~ msgid "Package"
#~ msgstr "Paket"

#~ msgid ""
#~ "Please respect the <a href=\"./#ads\">Debian mailing list advertising "
#~ "policy</a>."
#~ msgstr ""
#~ "Molimo poštujte <a href=\"./#ads\">načela oglašavanja na Debian mailing "
#~ "listama</a>."

#~ msgid "Please select which lists you want to subscribe to:"
#~ msgstr "Molimo odaberite liste na koje se želite pretplatiti:"

#~ msgid "Please select which lists you want to unsubscribe from:"
#~ msgstr "Molimo odaberite liste za koje želite otkazati pretplatu:"

#~ msgid "Posting messages allowed only to subscribers."
#~ msgstr "Slanje poruka dozvoljeno samo pretplatnicima."

#~ msgid "Rates:"
#~ msgstr "Cijene:"

#~ msgid ""
#~ "See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
#~ "how to subscribe using e-mail. An <a href=\"unsubscribe\">unsubscription "
#~ "web form</a> is also available, for unsubscribing from mailing lists. "
#~ msgstr ""
#~ "Pogledajte stranicu <a href=\"./#subunsub\">mailing lista</a> za "
#~ "informacije o tome kako se pretplatiti koristeći e-mail. Dostupan je i <a "
#~ "href=\"unsubscribe\">web formular za otkazivanje pretplate</a>, kako "
#~ "biste prekinuli pretplatu s mailing lista. "

#~ msgid ""
#~ "See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
#~ "how to unsubscribe using e-mail. An <a href=\"subscribe\">subscription "
#~ "web form</a> is also available, for subscribing to mailing lists. "
#~ msgstr ""
#~ "Pogledajte stranicu <a href=\"./#subunsub\">mailing lista</a> za "
#~ "informacije o tome kako otkazati pretplatu koristeći e-mail. Dostupan je "
#~ "i <a href=\"subscribe\">web formular za pretplatu</a>, kako biste se "
#~ "pretplatili na mailing liste. "

#~ msgid "Specifications:"
#~ msgstr "Specifikacije:"

#~ msgid "Status"
#~ msgstr "Stanje"

#~ msgid "Subscribe"
#~ msgstr "Pretplata"

#~ msgid "Subscription:"
#~ msgstr "Pretplata:"

#~ msgid "URL"
#~ msgstr "URL"

#~ msgid "URL:"
#~ msgstr "URL:"

#~ msgid "Unavailable"
#~ msgstr "Nedostupan"

#~ msgid "Unknown"
#~ msgstr "Nepoznato"

#~ msgid "Unsubscribe"
#~ msgstr "Otkaži pretplatu"

#~ msgid "Version"
#~ msgstr "Verzija"

#~ msgid "Wanted:"
#~ msgstr "Traži se:"

#~ msgid "Where:"
#~ msgstr "Gdje:"

#~ msgid "Who:"
#~ msgstr "Tko:"

#~ msgid "Willing to Relocate"
#~ msgstr "Spremni putovati"

#~ msgid "Working"
#~ msgstr "Radeći"

#~ msgid "Your E-Mail address:"
#~ msgstr "Vaša e-mail adresa:"

#~ msgid "animated&nbsp;GIF&nbsp;banner"
#~ msgstr "animirani&nbsp;GIF&nbsp;natpis"

#~ msgid "closed"
#~ msgstr "zatvorena"

#~ msgid "horizontal&nbsp;banner"
#~ msgstr "vodoravni&nbsp;natpis"

#~ msgid "is a read-only, digestified version."
#~ msgstr "je digest inačica koju samo možete čitati."

#~ msgid "open"
#~ msgstr "otvorena"

#~ msgid "or"
#~ msgstr "ili"

#~ msgid "p<get-var page />"
#~ msgstr "str. <get-var page />"

#~ msgid "sarge"
#~ msgstr "sarge"

#~ msgid "sarge (broken)"
#~ msgstr "sarge (pokvaren)"

#~ msgid "vertical&nbsp;banner"
#~ msgstr "okomiti&nbsp;natpis"
