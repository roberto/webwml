#use wml::debian::template title="Debian-Installer 正誤表"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="9a1725c7a7c2a16470f0814224c0b78cecb2e2fe" mindelta="1" maxdelta="1"

<h1><humanversion /> の正誤表</h1>

<p>これは Debian Installer の <humanversion />
リリースでの既知の問題のリストです。ここに挙げていない問題を見つけた場合、問題を記述した<a
href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">\
インストールレポート</a>を送ってください。</p>

<dl class="gloss">
     <dt>インストーラーで使っているテーマ</dt>
     <dd>まだBookworm向けのアートワークがないので、インストーラーはBullseyeのテーマを使い続けています。
     <br />
     <b>現状:</b> Bookworm RC 1で修正済み</dd>

     <dt>ファームウェアが必要なサウンドカードがある</dt>
     <dd>音を鳴らせるようにするためにファームウェアのロードが必要なサウンドカードがあります。
     (<a href="https://bugs.debian.org/992699">#992699</a>)
     <br />
     <b>現状:</b> Bookworm Alpha 1で修正済み</dd>

     <dt>搭載メモリの少ないシステムで暗号化LVMのセットアップに失敗する可能性がある</dt>
     <dd>搭載メモリが少ないシステム(例: 1 GB)で暗号化LVMをセットアップするときに遭遇する可能性があります:
     cryptsetup がLUKSパーティションを初期化中にOOMキラーによって失敗する可能性があります。
     (<a href="https://bugs.debian.org/1028250">#1028250</a>,
     <a href="https://gitlab.com/cryptsetup/cryptsetup/-/issues/802">cryptsetup upstream</a>)
     <br />
     <b>現状:</b> Bookworm RC 2で修正済み</dd>

     <dt>UEFIモードでパーティションを "ガイド - ディスク全体を使い、暗号化LVMをセットアップする" で作成できない</dt>
     <dd>ガイドにしたがって、パーティションを作成しようとすると、"強制的にUEFIをインストールしますか?"というプロンプトがでます。
     (他にオペレーティングシステムインストールしていなくても) 既定では "いいえ"を選択するようになっています。
     (<a href="https://bugs.debian.org/1033913">#1033913</a>)
     既定の選択のままにすると、セキュアブートが有効な場合に起動しないインストール結果となります。
     問題を回避するためには、"はい"を選択すべきです。
     <br />
     <b>現状:</b> Bookworm RC 2で修正済み</dd>

<!-- leaving this in for possible future use...
     <dt>multi-arch イメージのブートメニュー</dt>

     <dd>multi-arch イメージのブートメニューにはリグレッションがあります:
	<q>Rescue</q> メニューが 32 ビットフレーバにありません
       (<a href="https://bugs.debian.org/793118">#793118</a>)。
       <br />
       <b>現状:</b> Stretch Alpha 2 で修正されています。
       </dd>

     <dt>暗号化したインストールに失敗する</dt>

     <dd>partman-crypto のリグレッションにより、preseeding
	で設定が保存されないため暗号化したインストールができません
       (<a href="https://bugs.debian.org/793643">#793643</a>)。
       <br />
       <b>現状:</b> Stretch Alpha 3 で修正されています。
       </dd>
-->

<!-- things should be better starting with Jessie Beta 2...
	<dt>GNU/kFreeBSD サポート</dt>

	<dd>GNU/kFreeBSD 用インストールイメージで様々な重要なバグに苦しむ
	(<a href="https://bugs.debian.org/757985"><a href="https://bugs.debian.org/757985">#757985</a></a>、
	<a href="https://bugs.debian.org/757986"><a href="https://bugs.debian.org/757986">#757986</a></a>、
	<a href="https://bugs.debian.org/757987"><a href="https://bugs.debian.org/757987">#757987</a></a>、
	<a href="https://bugs.debian.org/757988"><a href="https://bugs.debian.org/757988">#757988</a></a>)。
	移植担当者は支援の手を活用してインストーラを使える状態に戻せるはずです!
	</dd>
-->

<!-- kind of obsoleted by the first "important change" mentioned in the 20140813 announce...
	<dt>インストールしたシステムのアクセシビリティ</dt>
	<dd>インストールの過程でアクセシビリティ技術を利用した場合でも
	インストールしたシステムでそれが自動的に有効化されないことがあります。
	</dd>
-->

<!-- leaving this in for possible future use...
	<dt>i386 で、CD#1 だけを使用してデスクトップをインストールすることができない</dt>
	<dd>1 枚目の CD 容量には制限があるため、要求される GNOME
	デスクトップ用パッケージが CD#1 に収まりません。他のパッケージソース
	(例えば 2 枚目の CD やネットワークミラー) を追加するか DVD を使ってください。
	<br />
	<b>現状:</b> 1 枚目の CD にこれ以上パッケージを収録することは期待できません。
	</dd>
-->
 
<!-- ditto...
	<dt>amd64で UEFI ブートする場合に問題が起きる可能性</dt>
	<dd>amd64 システムで Debian Installer を UEFI
	モードでブートした場合の問題が何件か報告されています。
	<code>grub-efi</code> を使うと確実なブートができないシステムや、
	最初のインストールスプラッシュ画面の表示時に画像が乱れるシステムがあるようです。
	<br />
	こういった問題に遭ってしまった場合、
	バグ報告を提出して症状およびハードウェアについてできる限り詳細な情報をください。
	そうすることが、チームがこのバグを修正するのに役立つはずです。
	とりあえずの回避方法としては、UEFI を使わずに<q>レガシー BIOS</q>
	か<q>フォールバックモード</q>を使ってください。
	<br />
	<b>現状:</b> Wheezy ポイントリリースではもっとバグの修正ができるかもしれません。
	</dd>
-->

<!-- ditto...
	<dt>i386: more than 32 mb of memory is needed to install</dt>
	<dd>
	The minimum amount of memory needed to successfully install on i386
	is 48 mb, instead of the previous 32 mb. We hope to reduce the
	requirements back to 32 mb later. Memory requirements may have
	also changed for other architectures.
	</dd>
-->

</dl>
