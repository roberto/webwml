#use wml::debian::template title="L'angolo norvegese di Debian"
#use wml::debian::translation-check translation="7ff1e58fbad65b9683e41dcd4c9d35fe7c22ecef" maintainer="Giuseppe Sacco"

    <p>In questa pagina troverete informazioni sugli utenti Debian
    norvegesi. Se avete qualcosa che ritenete inerente vi invitiamo a
    scrivere a uno dei <a href="#translators">traduttori</a> norvegesi.</p>

    <h2>Mailing list</h2>

    <p>Debian non ha attualmente nessuna mailing list ufficiale in
    norvegese. Se l'interesse nasce siamo in grado di crearne una o più, per
    gli utenti e gli sviluppatori.</p>

    <h2>Collegamenti</h2>

    <p>Alcuni riferimenti di interesse per gli utenti Debian norvegesi:</p>

    <ul>

    <!-- li><a href="http://www.linux.no/">Linux Norway</a><br>
	<em>"Un'organizzazione di volontariato che si propone di
        diffondere informazioni sul sistema operativo Linux in Norvegia."</em>
	Ospita, tra le altre cose, il 
	<a href="http://www.tnldp.org/">progetto di documentazione Linux
	in norvegese</a>, e le 
	<a href="http://www.linux.no/gullinux.html">pagine gialle Linux
	</a>.</li>

    <li><a href="http://nuug.no/">User Group UNIX norvegese</a></li>
    </li -->

    <li><a href="https://nuug.no/">Gruppi di utenti UNIX norvegesi</a>. Visitare
        la pagina web su <a href="https://wiki.nuug.no/likesinnede-oversikt">organizzazioni
        simili</a> che elenca molti, se non tutti, gruppi di utenti in Norvegia.</li>

    </ul>

    <h2>Contributori norvegesi del progetto Debian</h2>
    
    <p>Gli sviluppatore Debian norvegesi attualmente attivi:</p>

    <ul>
      <li>Lars Bahner &lt;<email bahner@debian.org>&gt;</li>
      <li>Morten Werner Forsbring &lt;<email werner@debian.org>&gt;</li>
      <li>Ove Kåven &lt;<email ovek@debian.org>&gt;</li>
      <li>Petter Reinholdtsen &lt;<email pere@debian.org>&gt;</li>
      <li>Ruben Undheim &lt;<email rubund@debian.org>&gt;</li>
      <li>Steinar H. Gunderson &lt;<email sesse@debian.org>&gt;</li>
      <li>Stein Magnus Jodal &lt;<email jodal@debian.org>&gt;</li>
      <li>Stig Sandbeck Mathisen &lt;<email ssm@debian.org>&gt;</li>
      <li>Tollef Fog Heen &lt;<email tfheen@debian.org>&gt;</li>
      <li>Øystein Gisnås &lt;<email shaka@debian.org>&gt;</li>
    </ul>

    <p>L'elenco non viene aggiornato di frequente per cui è possibile
    controllare il
    <a href="https://db.debian.org/">database degli sviluppatori
    Debian</a> selezionando la Norvegia come paese.</p>

    <h3>Ex sviluppatori Debian norvegesi:</h3>

    <ul>
      <li>Morten Hustvei</li>
      <li>Ole J. Tetlie</li>
      <li>Peter Krefting</li>
      <li>Tom Cato Amundsen</li>
      <li>Tore Anderson</li>
      <li>Tor Slettnes</li>
    </ul>

    <a id="translators"></a>
    <h3>Traduttori:</h3>

    <p>Le pagine web di Debian sono attualmente tradotte da:</p>

    <ul>
      <li>Hans F. Nordhaug &lt;<email hansfn@gmail.com>&gt;</li>
    </ul>

    <p>
      <!-- Debian currently does not have anyone that translates its web pages
      into Norwegian. -->
      Se sei interessato a tradurre, inizia leggendo le
      <a href="$(DEVEL)/website/translating">informazioni per i traduttori</a>.
    </p>

    <p>
      Le pagine web di Debian sono state tradotte in precedenza da:
    </p>

    <ul>
      <li>Cato Auestad</li>
      <li>Stig Sandbeck Mathisen</li>
      <li>Tollef Fog Heen</li>
      <li>Tor Slettnes</li>
    </ul>

    <p>Se volete dare una mano, o se siete a conoscenza di qualcuno
    che sia coinvolto in un modo o nell'altro nel progetto Debian, si prega di
    contattarci.</p>

