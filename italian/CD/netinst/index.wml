#use wml::debian::cdimage title="Installazione via rete da un CD minimale" BARETITLE=true
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="c64bdf15d82617c46942470ac0846ed7ec8b0f74" maintainer="Luca Monducci"

<p>Un CD per l'<q>installazione via rete</q> o <q>netinst</q> è un
semplice CD che permette di installare l'intero sistema operativo.
Questo CD contiene la minima quantità di software che permette di
installare il sistema di base e scaricare i pacchetti rimanenti da
Internet.</p>

<p><strong>Cos'è meglio: il CD-ROM minimale autoavviante o i CD
completi?</strong>
Dipende, in molti casi l'immagine del CD minimale è la scelta migliore
soprattutto perché verranno scaricati solo i pacchetti che si sceglie di
installare sulla propria macchina, risparmiando tempo e banda. D'altro
canto, i CD completi sono più comodi quando si fanno installazioni su
più macchine o su macchine non dotate di una connessione Internet.</p>

<p><strong>Quali tipi di connessione alla rete sono supportati durante
l'installazione?</strong>
È ovvio che per l'installazione via rete è necessaria la disponibilit&agrave;
di un collegamento a Internet. Sono supportate diverse modalità di
collegamento tra le quali dial-up PPP analogico, Ethernet e WLAN
(con qualche limitazione); purtroppo ISDN non è supportato,
siamo spiacenti!</p>

<p>Sono disponibili per il download le seguenti immagini avviabili
di CD minimali:</p>

<ul>
  <li>Immagini ufficiali <q>netinst</q> per il rilascio <q>stable</q>,
  <a href="#netinst-stable">vedere sotto</a>.</li>

  <li>Immagini per il rilascio <q>testing</q> vedere la
  <a href="$(DEVEL)/debian-installer/">pagina dell'installatore
  Debian</a>.</li>
</ul>


<h2 id="netinst-stable">Immagini ufficiali <q>netinst</q> per il
rilascio <q>stable</q></h2>

<p>Questa immagine contiene
l'installatore e una ridotta selezione di pacchetti che permettono
l'installazione di un sistema (molto) minimale.</p>

<div class="line">
<div class="item col50">
<p><strong>Immagine del CD netinst</strong></p>
  <stable-netinst-images />
</div>
<div class="item col50 lastcol">
<p><strong>
Immagine del CD netinst (via <a href="$(HOME)/CD/torrent-cd">bittorrent</a>)
</strong></p>
  <stable-netinst-torrent />
</div>
<div class="clear"></div>
</div>

<p>Le informazioni su questi file e su come usarli sono contenute
nelle <a href="../faq/">FAQ</a>.</p>

<p>Dopo aver scaricato le immagini, si consiglia di leggere le
<a href="$(HOME)/releases/stable/installmanual">istruzioni dettagliate
di installazione</a>.</p>
