<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An implementation flaw was discovered in mat, the metadata anonymisation
toolkit. The implementation of PDF support lacks support to anonymize
the metadata in embedded images. As there is no easy fix for this flaw,
it was decided that PDF support will be removed altogether from mat for
the time being.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.3.2-1+deb7u1.</p>

<p>We recommend that you upgrade your mat packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-650.data"
# $Id: $
