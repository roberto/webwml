<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9122">CVE-2017-9122</a>

    <p>The quicktime_read_moov function in moov.c in libquicktime 1.2.4 allows
    remote attackers to cause a denial of service (infinite loop and CPU
    consumption) via a crafted mp4 file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9123">CVE-2017-9123</a>

    <p>The lqt_frame_duration function in lqt_quicktime.c in libquicktime
    1.2.4 allows remote attackers to cause a denial of service (invalid
    memory read and application crash) via a crafted mp4 file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9124">CVE-2017-9124</a>

    <p>The quicktime_match_32 function in util.c in libquicktime 1.2.4 allows
    remote attackers to cause a denial of service (NULL pointer dereference
    and application crash) via a crafted mp4 file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9125">CVE-2017-9125</a>

    <p>The lqt_frame_duration function in lqt_quicktime.c in libquicktime
    1.2.4 allows remote attackers to cause a denial of service (heap-based
    buffer over-read) via a crafted mp4 file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9126">CVE-2017-9126</a>

    <p>The quicktime_read_dref_table function in dref.c in libquicktime 1.2.4
    allows remote attackers to cause a denial of service (heap-based buffer
    overflow and application crash) via a crafted mp4 file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9127">CVE-2017-9127</a>

    <p>The quicktime_user_atoms_read_atom function in useratoms.c in
    libquicktime 1.2.4 allows remote attackers to cause a denial of service
    (heap-based buffer overflow and application crash) via a crafted mp4
    file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9128">CVE-2017-9128</a>

    <p>The quicktime_video_width function in lqt_quicktime.c in libquicktime
    1.2.4 allows remote attackers to cause a denial of service (heap-based
    buffer over-read and application crash) via a crafted mp4 file.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2:1.2.4-3+deb7u2.</p>

<p>We recommend that you upgrade your libquicktime packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1042.data"
# $Id: $
