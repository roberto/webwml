<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8817">CVE-2017-8817</a>

     <p>Fuzzing by the OSS-Fuzz project led to the discovery of a read out of
     bounds flaw in the FTP wildcard function in libcurl. A malicious
     server could redirect a libcurl-based client to an URL using a
     wildcard pattern, triggering the out-of-bound read.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
7.26.0-1+wheezy23.</p>

<p>We recommend that you upgrade your curl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1195.data"
# $Id: $
