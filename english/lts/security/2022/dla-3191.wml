<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Multiple issues were discovered in Django, a Python-based web
development framework:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45452">CVE-2021-45452</a>

    <p>Storage.save allowed directory traversal if
   crafted filenames were passed directlyto it.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-22818">CVE-2022-22818</a>

    <p>The {% debug %} template tag did not properly
   encode the current context. This may lead to a cross-site
   scripting (XSS) vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23833">CVE-2022-23833</a>

    <p>The HTTP MultiPartParser had a issue whereby certain inputs to multipart
    forms could result in an infinite loop when parsing uploaded
    files.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, these problems have been fixed in version
1:1.11.29-1+deb10u4.</p>

<p>We recommend that you upgrade your python-django packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3191.data"
# $Id: $
