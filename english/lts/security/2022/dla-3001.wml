<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that the package com.google.code.gson:gson
before 2.8.9 is vulnerable to Deserialization of Untrusted Data
via the writeReplace() method in internal classes, which may
lead to DoS attacks.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.4-1+deb9u1.</p>

<p>We recommend that you upgrade your libgoogle-gson-java packages.</p>

<p>For the detailed security status of libgoogle-gson-java please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libgoogle-gson-java">https://security-tracker.debian.org/tracker/libgoogle-gson-java</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3001.data"
# $Id: $
