<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in abcm2ps: program which
translates ABC music description files to PostScript.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10753">CVE-2018-10753</a>

    <p>Stack-based buffer overflow in the delayed_output function in music.c
    allows remote attackers to cause a denial of service (application crash) or
    possibly have unspecified other impact.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10771">CVE-2018-10771</a>

    <p>Stack-based buffer overflow in the get_key function in parse.c allows remote
    attackers to cause a denial of service (application crash) or possibly have
    unspecified other impact.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1010069">CVE-2019-1010069</a>

    <p>Incorrect access control allows attackers to cause a denial of service via a
    crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32434">CVE-2021-32434</a>

    <p>Array overflow when wrong duration in voice overlay.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32435">CVE-2021-32435</a>

    <p>Stack-based buffer overflow in the function get_key in parse.c allows remote
    attackers to cause a senial of service (DoS) via unspecified vectors.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32436">CVE-2021-32436</a>

    <p>Out-of-bounds read in the function write_title() in subs.c allows remote
    attackers to cause a denial of service via unspecified vectors.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
7.8.9-1+deb9u1.</p>

<p>We recommend that you upgrade your abcm2ps packages.</p>

<p>For the detailed security status of abcm2ps please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/abcm2ps">https://security-tracker.debian.org/tracker/abcm2ps</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2983.data"
# $Id: $
