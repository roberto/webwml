<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several integer overflows have been discovered in TurboJPEG, a JPEG image
library, which can lead to a denial of service (application crash) if someone
attempts to compress or decompress gigapixel images with the TurboJPEG API.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1:1.5.1-2+deb9u2.</p>

<p>We recommend that you upgrade your libjpeg-turbo packages.</p>

<p>For the detailed security status of libjpeg-turbo please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libjpeg-turbo">https://security-tracker.debian.org/tracker/libjpeg-turbo</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3037.data"
# $Id: $
