<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The last Java security update introduced a change that broke libbluray's
interactive BD-J support. This update adds compatibility with those Java
changes.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1:1.1.0-1+deb10u1.</p>

<p>We recommend that you upgrade your libbluray packages.</p>

<p>For the detailed security status of libbluray please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libbluray">https://security-tracker.debian.org/tracker/libbluray</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3159.data"
# $Id: $
