<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in the Tomcat
servlet and JSP engine.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43980">CVE-2021-43980</a>

    <p>The simplified implementation of blocking reads and writes introduced in
    Tomcat 10 and back-ported to Tomcat 9.0.47 onwards exposed a long standing
    (but extremely hard to trigger) concurrency bug that could cause client
    connections to share an Http11Processor instance resulting in responses, or
    part responses, to be received by the wrong client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23181">CVE-2022-23181</a>

    <p>The fix for bug <a href="https://security-tracker.debian.org/tracker/CVE-2020-9484">CVE-2020-9484</a> introduced a time of check, time of use
    vulnerability into Apache Tomcat that allowed a local attacker to perform
    actions with the privileges of the user that the Tomcat process is using.
    This issue is only exploitable when Tomcat is configured to persist
    sessions using the FileStore.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29885">CVE-2022-29885</a>

    <p>The documentation of Apache Tomcat for the EncryptInterceptor incorrectly
    stated it enabled Tomcat clustering to run over an untrusted network. This
    was not correct. While the EncryptInterceptor does provide confidentiality
    and integrity protection, it does not protect against all risks associated
    with running over any untrusted network, particularly DoS risks.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
9.0.31-1~deb10u7.</p>

<p>We recommend that you upgrade your tomcat9 packages.</p>

<p>For the detailed security status of tomcat9 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/tomcat9">https://security-tracker.debian.org/tracker/tomcat9</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3160.data"
# $Id: $
