<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that <a href="https://security-tracker.debian.org/tracker/CVE-2014-10401">CVE-2014-10401</a> was fixed incompletely in the
Perl5 Database Interface (DBI).  An attacker could trigger information
disclosure through a different vector.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-10401">CVE-2014-10401</a>

    <p>DBD::File drivers can open files from folders other than those
    specifically passed via the f_dir attribute.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-10402">CVE-2014-10402</a>

    <p>DBD::File drivers can open files from folders other than those
    specifically passed via the f_dir attribute in the data source
    name (DSN). NOTE: this issue exists because of an incomplete fix
    for <a href="https://security-tracker.debian.org/tracker/CVE-2014-10401">CVE-2014-10401</a>.</p></li>

</ul>

<p>For Debian 9 stretch, this problem has been fixed in version
1.636-1+deb9u2.</p>

<p>We recommend that you upgrade your libdbi-perl packages.</p>

<p>For the detailed security status of libdbi-perl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libdbi-perl">https://security-tracker.debian.org/tracker/libdbi-perl</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3035.data"
# $Id: $
