<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Untrusted search path in FileZilla before 3.41.0-rc1 allows an
attacker to gain privileges via a malicious <q>fzsftp</q> binary in
the user's home directory.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
3.24.0-1+deb9u1.</p>

<p>We recommend that you upgrade your filezilla packages.</p>

<p>For the detailed security status of filezilla please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/filezilla">https://security-tracker.debian.org/tracker/filezilla</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3026.data"
# $Id: $
