<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that Mako, a Python template library, was vulnerable to a
denial of service attack via crafted regular expressions.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.0.7+ds1-1+deb10u1.</p>

<p>We recommend that you upgrade your mako packages.</p>

<p>For the detailed security status of mako please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mako">https://security-tracker.debian.org/tracker/mako</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3116.data"
# $Id: $
