<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>KiCad is a suite of programs for the creation of printed circuit boards.
It includes a schematic editor, a PCB layout tool, support tools and a 3D
viewer to display a finished &amp; fully populated PCB.</p>

<p>Several buffer-overflows were discovered in the Gerber Viewer and excellon
file parser, that could lead to code execution when opening a
maliciously-crafted file.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23803">CVE-2022-23803</a>

    <p>A stack-based buffer overflow vulnerability exists in the Gerber Viewer
    gerber and excellon ReadXYCoord coordinate parsing functionality of KiCad
    EDA.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23804">CVE-2022-23804</a>

    <p>A stack-based buffer overflow vulnerability exists in the Gerber Viewer
    gerber and excellon ReadIJCoord coordinate parsing functionality of KiCad
    EDA.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23946">CVE-2022-23946</a>

    <p>A stack-based buffer overflow vulnerability exists in the Gerber Viewer
    gerber and excellon GCodeNumber parsing functionality of KiCad EDA.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23947">CVE-2022-23947</a>

    <p>A stack-based buffer overflow vulnerability exists in the Gerber Viewer
    gerber and excellon DCodeNumber parsing functionality of KiCad EDA.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
5.0.2+dfsg1-1+deb10u1.
These problems were previously dealt with in DLA-2998-1 for Debian 9 stretch,
but the buster update wasn't applied, at the time.</p>

<p>We recommend that you upgrade your kicad packages.</p>

<p>For the detailed security status of kicad please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/kicad">https://security-tracker.debian.org/tracker/kicad</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3078.data"
# $Id: $
