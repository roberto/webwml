<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The following vulnerabilities have been discovered in rails, a ruby
based MVC frame work for web development.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21831">CVE-2022-21831</a>

    <p>A code injection vulnerability exists in the Active Storage that
    could allow an attacker to execute code via image_processing
    arguments.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-22577">CVE-2022-22577</a>

    <p>An XSS Vulnerability in Action Pack that could allow an attacker
    to bypass CSP for non HTML like responses.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23633">CVE-2022-23633</a>

    <p>Action Pack is a framework for handling and responding to web
    requests. Under certain circumstances response bodies will not be
    closed. In the event a response is *not* notified of a `close`,
    `ActionDispatch::Executor` will not know to reset thread local
    state for the next request. This can lead to data being leaked to
    subsequent requests.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-27777">CVE-2022-27777</a>

    <p>A XSS Vulnerability in Action View tag helpers which would allow
    an attacker to inject content if able to control input into
    specific attributes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32224">CVE-2022-32224</a>

    <p>When serialized columns that use YAML (the default) are
    deserialized, Rails uses YAML.unsafe_load to convert the YAML data
    in to Ruby objects. If an attacker can manipulate data in the
    database (via means like SQL injection), then it may be possible
    for the attacker to escalate to an RCE.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2:5.2.2.1+dfsg-1+deb10u4.</p>

<p>We recommend that you upgrade your rails packages.</p>

<p>For the detailed security status of rails please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/rails">https://security-tracker.debian.org/tracker/rails</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3093.data"
# $Id: $
