<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Vulnerabilities have been discovered in pdfresurrect, a tool for
analyzing and manipulating revisions to PDF documents.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14934">CVE-2019-14934</a>

    <p>pdf_load_pages_kids in pdf.c doesn't validate a certain size value,
    which leads to a malloc failure and out-of-bounds write</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-20740">CVE-2020-20740</a>

    <p>lack of header validation checks causes heap-buffer-overflow in
    pdf_get_version()</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
0.12-6+deb9u1.</p>

<p>We recommend that you upgrade your pdfresurrect packages.</p>

<p>For the detailed security status of pdfresurrect please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/pdfresurrect">https://security-tracker.debian.org/tracker/pdfresurrect</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2475.data"
# $Id: $
