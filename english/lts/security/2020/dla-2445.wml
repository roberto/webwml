<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A heap-based buffer over-read has been found in libmaxminddb, an IP geolocation
database library. This could be exploited when the mmdblookup tool is used to
open a specially crafted database file.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.2.0-1+deb9u1.</p>

<p>We recommend that you upgrade your libmaxminddb packages.</p>

<p>For the detailed security status of libmaxminddb please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libmaxminddb">https://security-tracker.debian.org/tracker/libmaxminddb</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2445.data"
# $Id: $
