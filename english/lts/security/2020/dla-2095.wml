<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>storeBackup.pl in storeBackup through 3.5 relies on the /tmp/storeBackup.lock
pathname, which allows symlink attacks that possibly lead to privilege
escalation.</p>

<p>Local users can also create a plain file named /tmp/storeBackup.lock to block
use of storeBackup until an admin manually deletes that file.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
3.2.1-1+deb8u1.</p>

<p>We recommend that you upgrade your storebackup packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2095.data"
# $Id: $
