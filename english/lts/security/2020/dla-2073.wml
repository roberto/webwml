<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues have been found in transfig, a XFig figure files converter.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16140">CVE-2018-16140</a>

    <p>Buffer underwrite vulnerability in get_line()
    allows an attacker to write prior to the beginning of the
    buffer via a crafted .fig file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14275">CVE-2019-14275</a>

    <p>Stack-based buffer overflow in the calc_arrow
    function in bound.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19555">CVE-2019-19555</a>

    <p>Stack-based buffer overflow because of an
    incorrect sscanf.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:3.2.5.e-4+deb8u2.</p>

<p>We recommend that you upgrade your transfig packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2073.data"
# $Id: $
