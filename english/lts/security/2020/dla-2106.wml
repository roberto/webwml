<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A vulnerability was discovered in libgd2, the GD graphics library,
whereby an attacker can employ a specific function call sequence to
trigger a NULL pointer dereference, subsequently crash the application
using libgd2, and create a denial of service.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.1.0-5+deb8u14.</p>

<p>We recommend that you upgrade your libgd2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2106.data"
# $Id: $
