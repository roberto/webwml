<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in lrzip, a compression
program. Heap-based and stack buffer overflows, use-after-free and infinite
loops would allow attackers to cause a denial of service or possibly other
unspecified impact via a crafted file.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
0.631-1+deb9u1.</p>

<p>We recommend that you upgrade your lrzip packages.</p>

<p>For the detailed security status of lrzip please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/lrzip">https://security-tracker.debian.org/tracker/lrzip</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2725.data"
# $Id: $
