<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in QEMU, a fast processor
emulator.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20257">CVE-2021-20257</a>

    <p>net: e1000: infinite loop while processing transmit descriptors</p>

<p></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20255">CVE-2021-20255</a>

    <p>A stack overflow via an infinite recursion vulnerability was found in the
    eepro100 i8255x device emulator of QEMU. This issue occurs while processing
    controller commands due to a DMA reentry issue. This flaw allows a guest
    user or process to consume CPU cycles or crash the QEMU process on the
    host, resulting in a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20203">CVE-2021-20203</a>

    <p>An integer overflow issue was found in the vmxnet3 NIC emulator of the
    QEMU. It may occur if a guest was to supply invalid values for rx/tx queue
    size or other NIC parameters. A privileged guest user may use this flaw to
    crash the QEMU process on the host resulting in DoS scenario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3416">CVE-2021-3416</a>

    <p>A potential stack overflow via infinite loop issue was found in various NIC
    emulators of QEMU in versions up to and including 5.2.0. The issue occurs
    in loopback mode of a NIC wherein reentrant DMA checks get bypassed. A
    guest user/process may use this flaw to consume CPU cycles or crash the
    QEMU process on the host resulting in DoS scenario.</p>

<p></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3416">CVE-2021-3416</a>

    <p>The patch for <a href="https://security-tracker.debian.org/tracker/CVE-2020-17380">CVE-2020-17380</a>/<a href="https://security-tracker.debian.org/tracker/CVE-2020-25085">CVE-2020-25085</a> was found to be ineffective,
    thus making QEMU vulnerable to the out-of-bounds read/write access issues
    previously found in the SDHCI controller emulation code. This flaw allows a
    malicious privileged guest to crash the QEMU process on the host, resulting
    in a denial of service or potential code execution.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1:2.8+dfsg-6+deb9u14.</p>

<p>We recommend that you upgrade your qemu packages.</p>

<p>For the detailed security status of qemu please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/qemu">https://security-tracker.debian.org/tracker/qemu</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2623.data"
# $Id: $
