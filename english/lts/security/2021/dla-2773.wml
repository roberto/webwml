<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two issues have been found in curl, a command line tool and an easy-to-use
client-side library for transferring data with URL syntax.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22946">CVE-2021-22946</a>

      <p>Crafted answers from a server might force clients to not use TLS on
      connections though TLS was required and expected.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22947">CVE-2021-22947</a>

      <p>When using STARTTLS to initiate a TLS connection, the server might
      send multiple answers before the TLS upgrade and such the client
      would handle them as being trusted. This could be used by a
      MITM-attacker to inject fake response data.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
7.52.1-5+deb9u16.</p>

<p>We recommend that you upgrade your curl packages.</p>

<p>For the detailed security status of curl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/curl">https://security-tracker.debian.org/tracker/curl</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2773.data"
# $Id: $
