<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities were discovered in ansible, a configuration
management, deployment, and task execution system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7481">CVE-2017-7481</a>

    <p>Ansible fails to properly mark lookup-plugin results as unsafe. If an
    attacker could control the results of lookup() calls, they could inject
    Unicode strings to be parsed by the jinja2 templating system, resulting in
    code execution. By default, the jinja2 templating language is now marked as
    <q>unsafe</q> and is not evaluated.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10156">CVE-2019-10156</a>

    <p>A flaw was discovered in the way Ansible templating was implemented,
    causing the possibility of information disclosure through unexpected
    variable substitution. By taking advantage of unintended variable
    substitution the content of any variable may be disclosed.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14846">CVE-2019-14846</a>

    <p>Ansible was logging at the DEBUG level which lead to a disclosure of
    credentials if a plugin used a library that logged credentials at the DEBUG
    level. This flaw does not affect Ansible modules, as those are executed in
    a separate process.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14904">CVE-2019-14904</a>

    <p>A flaw was found in the solaris_zone module from the Ansible Community
    modules. When setting the name for the zone on the Solaris host, the zone
    name is checked by listing the process with the <q>ps</q> bare command on the
    remote machine. An attacker could take advantage of this flaw by crafting
    the name of the zone and executing arbitrary commands in the remote host.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2.2.1.0-2+deb9u2.</p>

<p>We recommend that you upgrade your ansible packages.</p>

<p>For the detailed security status of ansible please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/ansible">https://security-tracker.debian.org/tracker/ansible</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2535.data"
# $Id: $
