<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues were discovered in activemq, a message
broker built around Java Message Service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15709">CVE-2017-15709</a>

    <p>When using the OpenWire protocol in activemq, it was found that
    certain system details (such as the OS and kernel version) are
    exposed as plain text.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11775">CVE-2018-11775</a>

    <p>TLS hostname verification when using the Apache ActiveMQ Client
    was missing which could make the client vulnerable to a MITM
    attack between a Java application using the ActiveMQ client and
    the ActiveMQ server. This is now enabled by default.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0222">CVE-2019-0222</a>

    <p>Unmarshalling corrupt MQTT frame can lead to broker Out of Memory
    exception making it unresponsive</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-26117">CVE-2021-26117</a>

    <p>The optional ActiveMQ LDAP login module can be configured to use
    anonymous access to the LDAP server. The anonymous context is used
    to verify a valid users password in error, resulting in no check
    on the password.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
5.14.3-3+deb9u2.</p>

<p>We recommend that you upgrade your activemq packages.</p>

<p>For the detailed security status of activemq please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/activemq">https://security-tracker.debian.org/tracker/activemq</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2583.data"
# $Id: $
