<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3609">CVE-2021-3609</a>

    <p>Norbert Slusarek reported a race condition vulnerability in the CAN
    BCM networking protocol, allowing a local attacker to escalate
    privileges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21781">CVE-2021-21781</a>

    <p>"Lilith >_>" of Cisco Talos discovered that the Arm initialisation
    code does not fully initialise the <q>sigpage</q> that is mapped into
    user-space processes to support signal handling.  This could
    result in leaking sensitive information, particularly when the
    system is rebooted.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33909">CVE-2021-33909</a>

    <p>The Qualys Research Labs discovered a size_t-to-int conversion
    vulnerability in the Linux kernel's filesystem layer. An
    unprivileged local attacker able to create, mount, and then delete a
    deep directory structure whose total path length exceeds 1GB, can
    take advantage of this flaw for privilege escalation.</p>

    <p>Details can be found in the Qualys advisory at
    <a href="https://www.qualys.com/2021/07/20/cve-2021-33909/sequoia-local-privilege-escalation-linux.txt">https://www.qualys.com/2021/07/20/cve-2021-33909/sequoia-local-privilege-escalation-linux.txt</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-34693">CVE-2021-34693</a>

    <p>Norbert Slusarek discovered an information leak in the CAN BCM
    networking protocol. A local attacker can take advantage of this
    flaw to obtain sensitive information from kernel stack memory.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
4.9.272-2.  This additionally fixes a regression in the previous
update (<a href="https://bugs.debian.org/990072">#990072</a>) that affected LXC.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2713-2.data"
# $Id: $
