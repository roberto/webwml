<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several security issues have been corrected in multiple demuxers and
decoders of the libav multimedia library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-1872">CVE-2015-1872</a>

    <p>The ff_mjpeg_decode_sof function in libavcodec/mjpegdec.c did not
    validate the number of components in a JPEG-LS Start Of Frame
    segment, which allowed remote attackers to cause a denial of service
    (out-of-bounds array access) or possibly have unspecified other
    impact via crafted Motion JPEG data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14058">CVE-2017-14058</a>

    <p>The read_data function in libavformat/hls.c did not restrict reload
    attempts for an insufficient list, which allowed remote attackers to
    cause a denial of service (infinite loop).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000460">CVE-2017-1000460</a>

    <p>In get_last_needed_nal() (libavformat/h264.c) the return value of
    init_get_bits was ignored and get_ue_golomb(&amp;gb) was called on an
    uninitialized get_bits context, which caused a NULL deref exception.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6392">CVE-2018-6392</a>

    <p>The filter_slice function in libavfilter/vf_transpose.c allowed
    remote attackers to cause a denial of service (out-of-array access)
    via a crafted MP4 file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1999012">CVE-2018-1999012</a>

    <p>libav contained a CWE-835: Infinite loop vulnerability in pva format
    demuxer that could result in a vulnerability that allowed attackers to
    consume excessive amount of resources like CPU and RAM. This attack
    appeared to be exploitable via specially crafted PVA file had to be
    provided as input.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
6:11.12-1~deb8u6.</p>

<p>We recommend that you upgrade your libav packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1740.data"
# $Id: $
