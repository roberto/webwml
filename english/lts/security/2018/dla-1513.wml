<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities were discovered in OpenAFS, a
distributed file system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16947">CVE-2018-16947</a>

    <p>The backup tape controller process accepts incoming RPCs but does
    not require (or allow for) authentication of those RPCs. Handling
    those RPCs results in operations being performed with administrator
    credentials, including dumping/restoring volume contents and
    manipulating the backup database.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16948">CVE-2018-16948</a>

    <p>Several RPC server routines did not fully initialize their output
    variables before returning, leaking memory contents from both the
    stack and the heap. Because the OpenAFS cache manager functions as
    an Rx server for the AFSCB service, clients are also susceptible to
    information leakage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16949">CVE-2018-16949</a>

    <p>Several data types used as RPC input variables were implemented as
    unbounded array types, limited only by the inherent 32-bit length
    field to 4GB. An unauthenticated attacker could send, or claim to
    send, large input values and consume server resources waiting for
    those inputs, denying service to other valid connections.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.6.9-2+deb8u8.</p>

<p>We recommend that you upgrade your openafs packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1513.data"
# $Id: $
