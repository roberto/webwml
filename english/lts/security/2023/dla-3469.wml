<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Issues were found in lua5.3, a powerful, light-weight programming
language designed for extending applications, which may result in denial
of service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6706">CVE-2019-6706</a>

    <p>Fady Osman discovered a heap-user-after-free vulnerability in
    <code>lua_upvaluejoin()</code> in <code>lapi.c</code>, which might
    result in denial of service or potentially arbitary code execution
    upon calling <code>debug.upvaluejoin()</code> with specific
    arguments.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24370">CVE-2020-24370</a>

    <p>Yongheng Chen discovered a negation overflow and segmentation fault
    issue in <code>getlocal()</code> and <code>setlocal()</code>, as
    demonstrated by <code>getlocal(3,2^31)</code>.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
5.3.3-1.1+deb10u1.</p>

<p>We recommend that you upgrade your lua5.3 packages.</p>

<p>For the detailed security status of lua5.3 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/lua5.3">https://security-tracker.debian.org/tracker/lua5.3</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3469.data"
# $Id: $
