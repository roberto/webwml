<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential Regular Expression Denial of
Service (ReDoS) attack in node-cookiejar, a Node.js library for parsing and
manipulating HTTP cookies. An attack was possible via passing a large value to
the <code>Cookie.parse</code> function.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25901">CVE-2022-25901</a>

    <p>Versions of the package cookiejar before 2.1.4 are vulnerable to Regular
    Expression Denial of Service (ReDoS) via the Cookie.parse function, which
    uses an insecure regular expression.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
2.0.1-1+deb10u1.</p>

<p>We recommend that you upgrade your node-cookiejar packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3561.data"
# $Id: $
