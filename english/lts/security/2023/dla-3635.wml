<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An upper bound check issue in `dsaVerify` function has been discovered in
node-browserify-sign. This allows an attacker to construct signatures that
can be successfully verified by any public key, thus leading to a signature
forgery attack.</p>

<p>For Debian 10 buster, this problem has been fixed in version
4.0.4-2+deb10u1.</p>

<p>We recommend that you upgrade your node-browserify-sign packages.</p>

<p>For the detailed security status of node-browserify-sign please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/node-browserify-sign">https://security-tracker.debian.org/tracker/node-browserify-sign</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3635.data"
# $Id: $
