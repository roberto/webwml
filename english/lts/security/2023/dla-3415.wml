<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>t was discovered that there was a potential validation bypass in
<a href="https://djangoproject.com">Djang</a>, a popular Python-based web
development framework.</p>

<p>Uploading multiple files using one form field has never been supported by
<code>forms.FileField</code> or <code>forms.ImageField</code>, as only the last
uploaded file was ever validated. Unfortunately, the uploading multiple files
topic in the documentation suggested otherwise.</p>

<p>In order to avoid a vulnerability, <code>ClearableFileInput</code> and
<code>FileInput</code> form widgets now raise <code>ValueError</code> when the
multiple HTML attribute is set on them. To prevent the exception and keep the
old behavior, set <code>allow_multiple_selected</code> to
<code>True</code>.</p>

<p>For more details on using the new attribute and handling of multiple
files through a single field, please see
<a href="https://www.djangoproject.com/weblog/2023/may/03/security-releases/">upstream's website<a></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-31047">CVE-2023-31047</a>

    <p>Potential bypass of validation when uploading multiple files using one form field</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
1:1.11.29-1+deb10u8.</p>

<p>We recommend that you upgrade your python-django packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3415.data"
# $Id: $
