<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Kevin Backhouse discovered an out-of-bounds array access in Libcue, a
library for parsing CD metadata, which could result in the execution of
arbitrary code.</p>


<p>For Debian 10 buster, this problem has been fixed in version
2.2.1-2+deb10u1.</p>

<p>We recommend that you upgrade your libcue packages.</p>

<p>For the detailed security status of libcue please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libcue">https://security-tracker.debian.org/tracker/libcue</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3615.data"
# $Id: $
