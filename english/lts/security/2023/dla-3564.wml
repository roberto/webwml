<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential Man In the Middle (MITM)
vulnerability in e2guardian, a web content filtering engine.</p>

<p>Validation of SSL certificates was missing in e2guardian's own MITM
prevention engine. In standalone mode (ie. acting as a proxy or a transparent
proxy) with SSL MITM enabled, e2guardian did not validate hostnames in
certificates of the web servers that it connected to, and thus was itself
vulnerable to MITM attacks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44273">CVE-2021-44273</a>

    <p>e2guardian v5.4.x &lt;= v5.4.3r is affected by missing SSL certificate
    validation in the SSL MITM engine. In standalone mode (i.e., acting as a
    proxy or a transparent proxy), with SSL MITM enabled, e2guardian, if built
    with OpenSSL v1.1.x, did not validate hostnames in certificates of the web
    servers that it connected to, and thus was itself vulnerable to MITM
    attacks.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
5.3.1-1+deb10u1.</p>

<p>We recommend that you upgrade your e2guardian packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3564.data"
# $Id: $
