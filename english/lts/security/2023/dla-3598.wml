<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two buffer overflow vulnerabilities were found in libvpx, a multimedia
library for the VP8 and VP9 video codecs, which could result in the
execution of arbitrary code if a specially crafted VP8 or VP9 media
stream is processed.</p>

<p>For Debian 10 buster, these problems have been fixed in version
1.7.0-3+deb10u2.</p>

<p>We recommend that you upgrade your libvpx packages.</p>

<p>For the detailed security status of libvpx please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libvpx">https://security-tracker.debian.org/tracker/libvpx</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3598.data"
# $Id: $
