<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A logic error was discovered in the implementation of the <q>SafeSocks</q>
option of Tor, a connection-based low-latency anonymous communication
system, which did result in allowing unsafe SOCKS4 traffic to pass.</p>


<p>For Debian 10 buster, this problem has been fixed in version
0.3.5.16-1+deb10u1.</p>

<p>We recommend that you upgrade your tor packages.</p>

<p>For the detailed security status of tor please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/tor">https://security-tracker.debian.org/tracker/tor</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3286.data"
# $Id: $
