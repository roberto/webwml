<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in the Tomcat
servlet and JSP engine.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-24998">CVE-2023-24998</a>

    <p>Denial of service. Tomcat uses a packaged renamed copy of Apache Commons
    FileUpload to provide the file upload functionality defined in the Jakarta
    Servlet specification. Apache Tomcat was, therefore, also vulnerable to the
    Commons FileUpload vulnerability <a href="https://security-tracker.debian.org/tracker/CVE-2023-24998">CVE-2023-24998</a> as there was no limit to
    the number of request parts processed. This resulted in the possibility of
    an attacker triggering a DoS with a malicious upload or series of uploads.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-41080">CVE-2023-41080</a>

    <p>Open redirect. If the ROOT (default) web application is configured to use
    FORM authentication then it is possible that a specially crafted URL could
    be used to trigger a redirect to an URL of the attackers choice.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-42795">CVE-2023-42795</a>

    <p>Information Disclosure. When recycling various internal objects, including
    the request and the response, prior to re-use by the next request/response,
    an error could cause Tomcat to skip some parts of the recycling process
    leading to information leaking from the current request/response to the
    next.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-44487">CVE-2023-44487</a>

    <p>DoS caused by HTTP/2 frame overhead (Rapid Reset Attack)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-45648">CVE-2023-45648</a>

    <p>Request smuggling. Tomcat did not correctly parse HTTP trailer headers. A
    specially crafted, invalid trailer header could cause Tomcat to treat a
    single request as multiple requests leading to the possibility of request
    smuggling when behind a reverse proxy.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
9.0.31-1~deb10u9.</p>

<p>We recommend that you upgrade your tomcat9 packages.</p>

<p>For the detailed security status of tomcat9 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/tomcat9">https://security-tracker.debian.org/tracker/tomcat9</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3617.data"
# $Id: $
