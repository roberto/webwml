<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential local privilege escalation in
GruntJS, a multipurpose task runner and build system tool.</p>

<p><code>file.copy</code> operations in GruntJS were vulnerable to a TOCTOU
("time-of-check vs. time-of-use") race condition that could have led to
arbitrary file writes in GitHub repositories. This could have then led to local
privilege escalation if a lower-privileged user had write access to both source
and destination directories, as the lower-privileged user could have created a
symlink to the GruntJS user's <code>~/.bashrc</code> configuration file
(etc).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1537">CVE-2022-1537</a>

    <p>file.copy operations in GruntJS are vulnerable to a TOCTOU race
    condition leading to arbitrary file write in GitHub repository
    gruntjs/grunt prior to 1.5.3. This vulnerability is capable of arbitrary
    file writes which can lead to local privilege escalation to the GruntJS
    user if a lower-privileged user has write access to both source and
    destination directories as the lower-privileged user can create a symlink
    to the GruntJS user's .bashrc file or replace /etc/shadow file if the
    GruntJS user is root.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
1.0.1-8+deb10u2.</p>

<p>We recommend that you upgrade your grunt packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3383.data"
# $Id: $
