<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Jacob Champion discovered that libpq can leak memory contents after
GSSAPI transport encryption initiation fails.</p>

<p>A modified server, or an unauthenticated man-in-the-middle, can send a
not-zero-terminated error message during setup of GSSAPI (Kerberos)
transport encryption.  libpq will then copy that string, as well as
following bytes in application memory up to the next zero byte, to its
error report. Depending on what the calling application does with the
error report, this could result in disclosure of application memory
contents.  There is also a small probability of a crash due to reading
beyond the end of memory.  Fix by properly zero-terminating the server
message. (<a href="https://security-tracker.debian.org/tracker/CVE-2022-41862">CVE-2022-41862</a>)</p>


<p>For Debian 10 buster, this problem has been fixed in version
11.19-0+deb10u1.</p>

<p>We recommend that you upgrade your postgresql-11 packages.</p>

<p>For the detailed security status of postgresql-11 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/postgresql-11">https://security-tracker.debian.org/tracker/postgresql-11</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3316.data"
# $Id: $
