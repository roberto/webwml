<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Moment.js is a JavaScript date library for parsing, validating,
manipulating, and formatting dates. A couple of vulnerabilities
were reported as follows:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24785">CVE-2022-24785</a>

    <p>A path traversal vulnerability impacts npm (server) users of
    Moment.js, especially if a user-provided locale string is directly
    used to switch moment locale.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-31129">CVE-2022-31129</a>

    <p>Affected versions of moment were found to use an inefficient
    parsing algorithm. Specifically using string-to-date parsing in
    moment (more specifically rfc2822 parsing, which is tried by
    default) has quadratic (N^2) complexity on specific inputs. Users
    may notice a noticeable slowdown is observed with inputs above 10k
    characters. Users who pass user-provided strings without sanity
    length checks to moment constructor are vulnerable to (Re)DoS
    attacks.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2.24.0+ds-1+deb10u1.</p>

<p>We recommend that you upgrade your node-moment packages.</p>

<p>For the detailed security status of node-moment please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/node-moment">https://security-tracker.debian.org/tracker/node-moment</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3295.data"
# $Id: $
