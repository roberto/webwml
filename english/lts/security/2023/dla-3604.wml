<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple issues were discovered in qemu, a fast processor emulator.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24165">CVE-2020-24165</a>

    <p>A use-after-free race in the code generator could lead to the
    execution of arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0330">CVE-2023-0330</a>

    <p>A DMA-MMIO reentrancy problem in the lsi53c895a device may lead to
    memory orruption bugs, such as stack overflow or use-after-free.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3180">CVE-2023-3180</a>

    <p>The function virtio_crypto_sym_op_helper, part of the implementation
    of qemu's virtual crypto device, did not check that the values of
    <q>src_len</q> and <q>dst_len</q> are the same.  This could lead to a heap
    buffer overflow.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1:3.1+dfsg-8+deb10u11.</p>

<p>We recommend that you upgrade your qemu packages.</p>

<p>For the detailed security status of qemu please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/qemu">https://security-tracker.debian.org/tracker/qemu</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3604.data"
# $Id: $
