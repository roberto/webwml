<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This release fixes various issues in shim bootloader and updates it to
a supported version. Older versions of the shim may eventually be blocked
by Secure Boot, so it is strongly advised for Secure Boot enabled systems
to upgrade to this newer version to keep the system bootable.</p>

<p>Additionally, this update blocks old, insecure versions of GRUB. Thus an
update to a signed GRUB 2.06-3~deb10u3 package as released in DLA 3190-2
must be in place prior to updating the shim packages.</p>

<p>For Debian 10 buster, this problem has been fixed in version
15.7-1~deb10u1.</p>

<p>We recommend that you upgrade your shim packages.</p>

<p>For the detailed security status of shim please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/shim">https://security-tracker.debian.org/tracker/shim</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3312.data"
# $Id: $
