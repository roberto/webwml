<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple potential security vulnerabilities in some Intel® Processors
have been found which may allow information disclosure or may allow
escalation of privilege. Intel is releasing firmware updates to mitigate
this potential vulnerabilities.</p>

<p>Please pay attention that the fix for <a href="https://security-tracker.debian.org/tracker/CVE-2022-33196">CVE-2022-33196</a> might require a
firmware update.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21216">CVE-2022-21216</a>

<p>(INTEL-SA-00700)
    Insufficient granularity of access control in out-of-band
    management in some Intel(R) Atom and Intel Xeon Scalable Processors
    may allow a privileged user to potentially enable escalation of
    privilege via adjacent network access.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-33196">CVE-2022-33196</a>

<p>(INTEL-SA-00738)
    Incorrect default permissions in some memory controller
    configurations for some Intel(R) Xeon(R) Processors when using
    Intel(R) Software Guard Extensions which may allow a privileged user
    to potentially enable escalation of privilege via local access.</p>

    <p>This fix may require a firmware update to be effective on some
    processors.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-33972">CVE-2022-33972</a>

<p>(INTEL-SA-00730)
    Incorrect calculation in microcode keying mechanism for some 3rd
    Generation Intel(R) Xeon(R) Scalable Processors may allow a
    privileged user to potentially enable information disclosure via
    local acces</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-38090">CVE-2022-38090</a>

<p>(INTEL-SA-00767)
    Improper isolation of shared resources in some Intel(R) Processors
    when using Intel(R) Software Guard Extensions may allow a privileged
    user to potentially enable information disclosure via local access.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21233">CVE-2022-21233</a>

<p>(INTEL-SA-00657)
    Improper isolation of shared resources in some Intel(R) Processors
    may allow a privileged user to potentially enable information
    disclosure via local access.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
3.20230214.1~deb10u1.</p>

<p>We recommend that you upgrade your intel-microcode packages.</p>

<p>For the detailed security status of intel-microcode please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/intel-microcode">https://security-tracker.debian.org/tracker/intel-microcode</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3379.data"
# $Id: $
