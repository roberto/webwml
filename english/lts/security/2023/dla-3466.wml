<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Avahi a free zero-configuration networking (zeroconf) implementation,
including a system for multicast DNS/DNS-SD service discovery, was
affected by a Deny of Service. The event used to signal the termination of
the client connection on the avahi Unix socket is not correctly handled
in the client_work function, allowing a local attacker to trigger
an infinite loop.</p>

<p>For Debian 10 buster, this problem has been fixed in version
0.7-4+deb10u3.</p>

<p>We recommend that you upgrade your avahi packages.</p>

<p>For the detailed security status of avahi please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/avahi">https://security-tracker.debian.org/tracker/avahi</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3466.data"
# $Id: $
