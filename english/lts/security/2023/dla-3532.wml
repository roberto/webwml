<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that OpenSSH incorrectly handled loading certain
PKCS#11 providers. If a user forwarded their ssh-agent to an untrusted
system, a remote attacker could possibly use this issue to load
arbitrary libraries from the user’s system and execute arbitrary code.</p>

<p>In addition to the above security issue, this update also fixed
another bug - bad interaction between the ssh_config ConnectTimeout
and ConnectionAttempts directives - connection attempts after the
first attempt were ignoring the requested timeout. More details about
this can be found at <a href="https://bugzilla.mindrot.org/show_bug.cgi?id)18.">https://bugzilla.mindrot.org/show_bug.cgi?id)18.</a></p>

<p>For Debian 10 buster, this problem has been fixed in version
1:7.9p1-10+deb10u3.</p>

<p>We recommend that you upgrade your openssh packages.</p>

<p>For the detailed security status of openssh please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openssh">https://security-tracker.debian.org/tracker/openssh</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3532.data"
# $Id: $
