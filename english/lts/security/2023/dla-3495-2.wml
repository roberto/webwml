<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Ubuntu security team noted after extensive testing that DLA-3495-1
was incomplete as one PoC for <a href="https://security-tracker.debian.org/tracker/CVE-2022-2400">CVE-2022-2400</a> (particularly the
chroot escape) was still working on the patched version of
the package.</p>

<p>Further analysis of the upstream patch and  DLA-3495-1 version
helped to identify that the vulnerability was still present due to
DLA 3495-1 not including commit 7adf00f9, which added chroot checks
to one of the code path.</p>

<p>Special thanks to Camila Camargo de Matos of Ubuntu security team.</p>

<p>For Debian 10 buster, this problem has been fixed in version
0.6.2+dfsg-3+deb10u2.</p>

<p>We recommend that you upgrade your php-dompdf packages.</p>

<p>For the detailed security status of php-dompdf please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/php-dompdf">https://security-tracker.debian.org/tracker/php-dompdf</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3495-2.data"
# $Id: $
