<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were fixed in the Python3 interpreter.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-20107">CVE-2015-20107</a>

    <p>The mailcap module did not add escape characters into commands
    discovered in the system mailcap file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10735">CVE-2020-10735</a>

    <p>Prevent DoS with very large int.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3426">CVE-2021-3426</a>

    <p>Remove the pydoc getfile feature which could be abused to read
    arbitrary files on the disk.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3733">CVE-2021-3733</a>

    <p>Regular Expression Denial of Service in urllib's AbstractBasicAuthHandler class.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3737">CVE-2021-3737</a>

    <p>Infinite loop in the HTTP client code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4189">CVE-2021-4189</a>

    <p>Make ftplib not trust the PASV response.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-45061">CVE-2022-45061</a>

    <p>Quadratic time in the IDNA decoder.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
3.7.3-2+deb10u5.</p>

<p>We recommend that you upgrade your python3.7 packages.</p>

<p>For the detailed security status of python3.7 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python3.7">https://security-tracker.debian.org/tracker/python3.7</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3477.data"
# $Id: $
