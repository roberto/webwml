<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>LXC is a Linux Containers userspace tool set. Maher Azzouzi reported that the
lxc-user-nic command, included in LXC, allowed unprivileged users to infer
whether any file exists, even in protected directory trees.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1:3.1.0+really3.0.3-8+deb10u1.</p>

<p>We recommend that you upgrade your lxc packages.</p>

<p>For the detailed security status of lxc please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/lxc">https://security-tracker.debian.org/tracker/lxc</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3533.data"
# $Id: $
