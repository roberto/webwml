<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A specific flaw within the processing of recovery volumes exists in RAR,
an archive program for rar files. It allows remote attackers to execute
arbitrary code on affected installations. User interaction is required to
exploit this vulnerability. The target must visit a malicious page or open a
malicious rar file.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2:6.23-1~deb10u1.</p>

<p>We recommend that you upgrade your rar packages.</p>

<p>For the detailed security status of rar please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/rar">https://security-tracker.debian.org/tracker/rar</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3543.data"
# $Id: $
