#use wml::debian::template title="Debian &ldquo;etch&rdquo; Release Information"
#include "$(ENGLISHDIR)/releases/info"


<p>Debian GNU/Linux <current_release_etch> was
released on <a href="$(HOME)/News/<current_release_newsurl_etch/>"><current_release_date_etch></a>.
Debian 4.0 was initially released on <:=spokendate('2007-04-08'):>.
The release included many major changes, described in 
our <a href="$(HOME)/News/2007/20070408">press release</a> and 
the <a href="releasenotes">Release Notes</a>.</p>


<p><strong>Debian GNU/Linux 4.0 has been superseded by
<a href="../lenny/">Debian GNU/Linux 5.0 (<q>lenny</q>)</a>.
Security updates have been discontinued as of the end of February 2010.
</strong></p>


<p>To obtain and install Debian GNU/Linux, see
installation information page and the
Installation Guide. To upgrade from an older
Debian release, see the instructions in the
<a href="releasenotes">Release Notes</a>.</p>

<p>The following computer architectures were supported in this release:</p>

<ul>
<li><a href="../../ports/alpha/">Alpha</a>
<li><a href="../../ports/amd64/">64-bit PC (amd64)</a>
<li><a href="../../ports/arm/">ARM</a>
<li><a href="../../ports/hppa/">HP PA-RISC</a>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/ia64/">Intel Itanium IA-64</a>
<li><a href="../../ports/mips/">MIPS (big endian)</a>
<li><a href="../../ports/mipsel/">MIPS (little endian)</a>
<li><a href="../../ports/powerpc/">PowerPC</a>
<li><a href="../../ports/s390/">IBM S/390</a>
<li><a href="../../ports/sparc/">SPARC</a>
</ul>

<p>Contrary to our wishes, there may be some problems that exist in the
release, even though it is declared <em>stable</em>. We've made
<a href="errata">a list of the major known problems</a>, and you can always
report other issues to us.</p>
