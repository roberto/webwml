<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in the HTTP/2 code of Apache
Traffic Server, a reverse and forward proxy server, which could result
in denial of service.</p>

<p>The fixes are too intrusive to backport to the version in the oldstable
distribution (stretch). An upgrade to Debian stable (buster) is
recommended instead.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 8.0.2+ds-1+deb10u1.</p>

<p>We recommend that you upgrade your trafficserver packages.</p>

<p>For the detailed security status of trafficserver please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/trafficserver">\
https://security-tracker.debian.org/tracker/trafficserver</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4520.data"
# $Id: $
