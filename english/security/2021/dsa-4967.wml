<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Etienne Stalmans discovered that unsquashfs in squashfs-tools, the tools
to create and extract Squashfs filesystems, does not validate filenames
for traversal outside of the destination directory. An attacker can take
advantage of this flaw for writing to arbitrary files to the filesystem
if a malformed Squashfs image is processed.</p>

<p>For the oldstable distribution (buster), this problem has been fixed
in version 1:4.3-12+deb10u1.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 1:4.4-2+deb11u1.</p>

<p>We recommend that you upgrade your squashfs-tools packages.</p>

<p>For the detailed security status of squashfs-tools please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/squashfs-tools">https://security-tracker.debian.org/tracker/squashfs-tools</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4967.data"
# $Id: $
