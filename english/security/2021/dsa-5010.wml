<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Apache Santuario - XML Security for Java is vulnerable to an issue where the
<q>secureValidation</q> property is not passed correctly when creating a KeyInfo
from a KeyInfoReference element. This allows an attacker to abuse an XPath
Transform to extract any local .xml files in a RetrievalMethod element.</p>

<p>For the oldstable distribution (buster), this problem has been fixed
in version 2.0.10-2+deb10u1.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 2.0.10-2+deb11u1.</p>

<p>We recommend that you upgrade your libxml-security-java packages.</p>

<p>For the detailed security status of libxml-security-java please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libxml-security-java">\
https://security-tracker.debian.org/tracker/libxml-security-java</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-5010.data"
# $Id: $
