<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36311">CVE-2020-36311</a>

    <p>A flaw was discovered in the KVM subsystem for AMD CPUs, allowing an
    attacker to cause a denial of service by triggering destruction of a
    large SEV VM.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3609">CVE-2021-3609</a>

    <p>Norbert Slusarek reported a race condition vulnerability in the CAN
    BCM networking protocol, allowing a local attacker to escalate
    privileges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33909">CVE-2021-33909</a>

    <p>The Qualys Research Labs discovered a size_t-to-int conversion
    vulnerability in the Linux kernel's filesystem layer. An
    unprivileged local attacker able to create, mount, and then delete a
    deep directory structure whose total path length exceeds 1GB, can
    take advantage of this flaw for privilege escalation.</p>

    <p>Details can be found in the Qualys advisory at
    <a href="https://www.qualys.com/2021/07/20/cve-2021-33909/sequoia-local-privilege-escalation-linux.txt">https://www.qualys.com/2021/07/20/cve-2021-33909/sequoia-local-privilege-escalation-linux.txt</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-34693">CVE-2021-34693</a>

    <p>Norbert Slusarek discovered an information leak in the CAN BCM
    networking protocol. A local attacker can take advantage of this
    flaw to obtain sensitive information from kernel stack memory.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 4.19.194-3.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4941.data"
# $Id: $
