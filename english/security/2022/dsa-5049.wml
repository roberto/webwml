<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in Flatpak, an application
deployment framework for desktop apps.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43860">CVE-2021-43860</a>

    <p>Ryan Gonzalez discovered that Flatpak didn't properly validate
    that the permissions displayed to the user for an app at install
    time match the actual permissions granted to the app at
    runtime. Malicious apps could therefore grant themselves
    permissions without the consent of the user.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21682">CVE-2022-21682</a>

    <p>Flatpak didn't always prevent a malicious flatpak-builder user
    from writing to the local filesystem.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed
in version 1.10.7-0+deb11u1.</p>

<p>Please note that flatpak-builder also needed an update for
compatibility, and is now at version 1.0.12-1+deb11u1 in bullseye.</p>

<p>We recommend that you upgrade your flatpak and flatpak-builder
packages.</p>

<p>For the detailed security status of flatpak please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/flatpak">https://security-tracker.debian.org/tracker/flatpak</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5049.data"
# $Id: $
