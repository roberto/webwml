<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues were discovered in the Squid proxy caching
server:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28116">CVE-2021-28116</a>

    <p>Amos Jeffries discovered an information leak if WCCPv2 is enabled</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-46784">CVE-2021-46784</a>

    <p>Joshua Rogers discovered that an error in parsing Gopher server
    responses may result in denial of service</p></li>

</ul>

<p>For the oldstable distribution (buster), these problems have been fixed
in version 4.6-1+deb10u7.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 4.13-10+deb11u1.</p>

<p>We recommend that you upgrade your squid packages.</p>

<p>For the detailed security status of squid please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/squid">\
https://security-tracker.debian.org/tracker/squid</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5171.data"
# $Id: $
