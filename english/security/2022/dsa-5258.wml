<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in Squid, a fully featured web
proxy cache, which could result in exposure of sensitive information in
the cache manager (<a href="https://security-tracker.debian.org/tracker/CVE-2022-41317">CVE-2022-41317</a>),
or denial of service or information disclosure if Squid is configured to
negotiate authentication with the SSPI and SMB authentication helpers
(<a href="https://security-tracker.debian.org/tracker/CVE-2022-41318">CVE-2022-41318</a>).</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 4.13-10+deb11u2.</p>

<p>We recommend that you upgrade your squid packages.</p>

<p>For the detailed security status of squid please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/squid">\
https://security-tracker.debian.org/tracker/squid</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5258.data"
# $Id: $
