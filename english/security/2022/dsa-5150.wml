<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Peter Agten discovered that several modules for TCP syslog reception in
rsyslog, a system and kernel logging daemon, have buffer overflow flaws
when octet-counted framing is used, which could result in denial of
service or potentially the execution of arbitrary code.</p>

<p>For the oldstable distribution (buster), this problem has been fixed
in version 8.1901.0-1+deb10u2.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 8.2102.0-2+deb11u1.</p>

<p>We recommend that you upgrade your rsyslog packages.</p>

<p>For the detailed security status of rsyslog please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/rsyslog">\
https://security-tracker.debian.org/tracker/rsyslog</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5150.data"
# $Id: $
