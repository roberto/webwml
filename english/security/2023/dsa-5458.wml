<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the OpenJDK Java runtime,
which may result in bypass of sandbox restrictions, information
disclosure, reduced cryptographic strength of the AES implementation,
directory traversal or denial of service.</p>

<p>For the oldstable distribution (bullseye) additional build dependencies
need to be backported, a fixed package will be provided when these are
ready as 17.0.8+7-1~deb11u1.</p>

<p>For the stable distribution (bookworm), these problems have been fixed in
version 17.0.8+7-1~deb12u1.</p>

<p>We recommend that you upgrade your openjdk-17 packages.</p>

<p>For the detailed security status of openjdk-17 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openjdk-17">\
https://security-tracker.debian.org/tracker/openjdk-17</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5458.data"
# $Id: $
