<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in the Tomcat
servlet and JSP engine.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42252">CVE-2022-42252</a>

    <p>Apache Tomcat was configured to ignore invalid HTTP headers via setting
    rejectIllegalHeader to false. Tomcat did not reject a request containing an
    invalid Content-Length header making a request smuggling attack possible if
    Tomcat was located behind a reverse proxy that also failed to reject the
    request with the invalid header.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-45143">CVE-2022-45143</a>

    <p>The JsonErrorReportValve in Apache Tomcat did not escape the type, message
    or description values. In some circumstances these are constructed from
    user provided data and it was therefore possible for users to supply values
    that invalidated or manipulated the JSON output.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28708">CVE-2023-28708</a>

    <p>When using the RemoteIpFilter with requests received from a reverse proxy
    via HTTP that include the X-Forwarded-Proto header set to https, session
    cookies created by Apache Tomcat did not include the secure attribute. This
    could result in the user agent transmitting the session cookie over an
    insecure channel.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 9.0.43-2~deb11u6.</p>

<p>We recommend that you upgrade your tomcat9 packages.</p>

<p>For the detailed security status of tomcat9 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/tomcat9">\
https://security-tracker.debian.org/tracker/tomcat9</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5381.data"
# $Id: $
