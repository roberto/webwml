<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple issues were found in Git, a distributed revision control system.
An attacker may trigger remote code execution, cause local users into
executing arbitrary commands, leak information from the local filesystem,
and bypass restricted shell.</p>

<p>This update includes two changes of behavior that may affect certain setup:
  - It stops when directory traversal changes ownership from the current
    user while looking for a top-level git directory, a user could make an
    exception by using the new safe.directory configuration.
  - The default of protocol.file.allow has been changed from &quot;always&quot; to
    &quot;user&quot;.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 1:2.30.2-1+deb11u1.</p>

<p>We recommend that you upgrade your git packages.</p>

<p>For the detailed security status of git please refer to
its security tracker page at:
<a  rel="nofollow" href="https://security-tracker.debian.org/tracker/git">https://security-tracker.debian.org/tracker/git</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5332.data"
# $Id: $
