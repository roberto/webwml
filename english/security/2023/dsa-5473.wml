<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that authenticated API users of Orthanc, a DICOM server
for medical imaging, could overwrite arbitrary files and in some setups
execute arbitrary code.</p>

<p>This update backports the option RestApiWriteToFileSystemEnabled,
setting it to <q>true</q> in /etc/orthanc/orthanc.json restores the previous
behaviour.</p>

<p>For the oldstable distribution (bullseye), this problem has been fixed
in version 1.9.2+really1.9.1+dfsg-1+deb11u1.</p>

<p>For the stable distribution (bookworm), this problem has been fixed in
version 1.10.1+dfsg-2+deb12u1.</p>

<p>We recommend that you upgrade your orthanc packages.</p>

<p>For the detailed security status of orthanc please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/orthanc">https://security-tracker.debian.org/tracker/orthanc</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5473.data"
# $Id: $
