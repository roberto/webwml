<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in Request Tracker, an
extensible trouble-ticket tracking system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-41259">CVE-2023-41259</a>

    <p>Tom Wolters reported that Request Tracker is vulnerable to accepting
    unvalidated RT email headers in incoming email and the mail-gateway
    REST interface.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-41260">CVE-2023-41260</a>

    <p>Tom Wolters reported that Request Tracker is vulnerable to
    information leakage via response messages returned from requests
    sent via the mail-gateway REST interface.</p></li>

</ul>

<p>For the oldstable distribution (bullseye), these problems have been fixed
in version 4.4.4+dfsg-2+deb11u3.</p>

<p>For the stable distribution (bookworm), these problems have been fixed in
version 4.4.6+dfsg-1.1+deb12u1.</p>

<p>We recommend that you upgrade your request-tracker4 packages.</p>

<p>For the detailed security status of request-tracker4 please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/request-tracker4">\
https://security-tracker.debian.org/tracker/request-tracker4</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5542.data"
# $Id: $
