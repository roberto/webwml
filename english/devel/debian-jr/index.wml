#use wml::debian::template title="Debian Jr. Project"

<h2>Debian for children</h2>

<p>
This is a <a href="https://www.debian.org/blends/">Debian Pure Blend</a>
(in short Blend). Our goal is to make Debian an OS that children
of all ages will want to use. Our initial focus will be on producing something
for children up to age 8. Once we have accomplished this, our next target age
range is 7 to 12.  By the time children reach their teens, they should be
comfortable with using Debian without any special modifications.
</p>

<h3>Email List</h3>
<p>
Debian has set up an email list for this group.  You can 
<a href="https://lists.debian.org/debian-jr/">
subscribe to it or read the mail archives</a>.
</p>

<h3>Chat</h3>
<p>
We have a realtime discussion channel, #debian-jr on irc.debian.org
and a XMPP multiuserchat <a href="xmpp:debian-jr@conference.debian.org?join"> 
debian-jr@conference.debian.org</a>.
</p>

<h3>News</h3>
<p>
We have a <a href="https://debian-jr-team.pages.debian.net/blog/">Team Blog</a> for 
project news and updates.
</p>

<h3>Community Site</h3>
<p>
We have a <a href="https://wiki.debian.org/Teams/DebianJr">Debian Jr.
community site</a> where developers and users alike are encouraged to contribute to
the Debian Jr. project.
</p>

<h3>What can I do to help?</h3>
<p>
We would be interested to hear what you think Debian Jr. could be
doing, particularly if you'd like to help make it happen.
</p>

<h3>Installation</h3>
<p>
The <a href="https://packages.debian.org/junior-doc">junior-doc</a> package
contains the Quick Guide. 
</p>

<h3>Packages in Debian Jr.</h3>

<p>
Debian Pure Blends are creating <a
href="https://blends.debian.org/junior/tasks/">overviews over the
packages</a> in which are interesting for the target user group.
</p>


<h3>Debian Jr. Goals</h3>

<h4>Making Debian desirable to children</h4>

<p>
The primary goal of the Debian Jr. project is to make Debian an OS our
children <i>want</i> to run.  This involves some sensitivity to the
needs of children as expressed by the children themselves.  As parents,
developers, older siblings, sys admins, we need to keep our ears and
eyes open and discover what it is that makes computers desirable to
children.  Without this focus, we can easily get sidetracked trying to
achieve abstract goals like "user friendliness", "simplicity", "low
maintenance", or "robustness" that, while they are certainly laudable
goals for Debian as a whole, are too broad for addressing the specific
needs and wants of children.
</p>

<h4>Applications</h4>

<p>
Naturally, children have different needs and wants from adults in the
applications they choose to run.  Some of these will be games, while
others will be word processors, text editors, "paint" programs, and the
like.  The goal is to identify the highest quality applications
available within Debian that are suitable for children, add to that
number by packaging ones not yet in Debian, and ensure that the chosen
applications are kept in a well-maintained state.  One implementation
goal is to provide meta packages to make installing groups of "child
friendly" applications easier for the sys admin.  Another is to improve
our packages in ways that particularly matter for children, which
could be as simple as filling in holes in the documentation, or could be
more complex, involving work with the upstream authors.
</p>

<h4>Childproofing and account management</h4>

<p>
The idea here is not to necessarily to implement tough security measures. 
That is beyond our mandate.  The goal is simply to provide
sys admins with documentation and tools for setting up their
systems so that their naturally curious child users will not "break" their
accounts, soak up all system resources, or otherwise do things that
require constant sys admin intervention.  This is a more of a concern for
child users than adults, as they tend to explore and deliberately push the
system to the limits just to see what happens.  The messes that result can
be at once both amusing and frustrating.  This goal is about keeping your
sanity (and sense of humor) as a child's sys admin.
</p>

<h4>Learning to use the computer</h4>

<p>
The "Childproofing" goal needs to be balanced with the goal of <i>allowing</i>
children to try things (and yes, break things) and find solutions to their
problems.  Learning to use the keyboard, GUI, shell, and computer
languages are all things that parents and children alike need some tips to
help get them headed in the right direction. 
</p>

<h4>User interface</h4>

<p>
Discover and implement both GUI and text-based interfaces that work well
for and are attractive to children.  The idea is not to re-invent the user
interface, but to add value to existing tools and packages (window
managers, menu system and so forth) by providing some convenient
pre-selected configurations that we find work best for children.
</p>

<h4>Family guidance</h4>

<p>
Give parents (and in some cases, older siblings) the tools to help their
children (or siblings) learn about computers and to put reasonable
limits on their access, guiding them towards independent use of the
computer as they mature.  For example, many parents will be concerned
about regulating Internet use to protect their children until they reach
a suitable age to deal with mature content.  The important thing to
remember is that the parents will chose what they think is best for
their children.  The Debian Jr. group does not make this judgement, but
is there to help provide the tools and documentation to help the parents
with these decisions.  That being said, I think this goal should focus
more on the "guidance" aspect than restriction, as the former is a
positive activity while the latter is negative.
</p>

<h4>Children's System</h4>

<p>
While our first goal as children's sys admins will probably be to set up
the children with
accounts on our own systems and populate it with applications that they
enjoy, there comes a time when we contemplate getting them their own
system.  The most ambitious realization of this goal might be a Debian
software equivalent of the "toy" computers currently on the market: 
brightly colored, decal-covered systems pre-loaded with software
appealing to children of a specific age range.  It
is important to keep in perspective that this would still be a Debian
system, not some fork from the Debian distribution.  It is a perfectly
achievable goal through the Debian package management system (via meta
packages, for example) and should not require a fork in development to
produce a special Debian "children's edition". 
</p>

<h4>Internationalization</h4>

<p>
While English may be the "universal" language, not all children speak it
as their mother tongue.  And while internationalization should be a goal
of Debian itself, language problems are amplified when dealing with children.
Children will not want to use Debian if support for their language is not
there, and will find it more comfortable to use other OS's where language
support is better.  Therefore, we do need to keep this in mind.
</p>
